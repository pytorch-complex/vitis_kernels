#!/bin/bash

# Load Virtual Environment
. /usr/local/share/applications/venv/bin/activate

# Load Virtual Environment Variables
. /etc/environment.d/venv.sh

# Load Xilinx HLS Environment Variables
source /tools/Xilinx/Vitis_HLS/2023.2/settings64.sh  # XILINX_VITIS, XILINX_VIVADO, XILINX_HLS
source /opt/xilinx/xrt/setup.sh # XILINX_XRT

# Define ci Variables
export USER=$(logname)
export XCL_PLATFORM="${XCL_PLATFORM:-xilinx_u200_gen3x16_xdma_2_202110_1}"
export XCL_OPT_INDEX="${XCL_OPT_INDEX:-4}"  # 1: buffered 2: dataflow 3: distributed 4: vec
export XCL_CU="${XCL_CU:-1}" 
export XCL_EMULATION_MODE="${XCL_EMULATION_MODE:-sw_emu}" # sw_emu | hw_emu | hw
export XCL_DTYPE="${XCL_DTYPE:-Float}"
export XCL_CATEGORY="${XCL_CATEGORY:-BinaryArithmeticKernel}"
export Torch_DIR="${SITE_PACKAGES}/torch/share/cmake/Torch"
export PYTORCH_DIR="${SITE_PACKAGES}/torch"
export SRCDIR="/home/$(logname)/repos/vitis_kernels"
export TOPDIR="/tmp/math-build-${XCL_PLATFORM}-${XCL_DTYPE}-${XCL_CATEGORY}-${XCL_OPT_INDEX}-${XCL_CU}-${XCL_EMULATION_MODE}"
export J=1
export LOG_DIR="${TOPDIR}"
export INFO_FILE="${LOG_DIR}/info.log"
export DEBUG_FILE="${LOG_DIR}/debug.log"

ECHO_RESULT () {
  if [ $? -eq 0 ]; then
    echo " ✅ " | tee -a ${INFO_FILE}
  else
    echo " ❌ " | tee -a ${INFO_FILE}
  fi
}

# Reset the Top Directory
rm -rf ${TOPDIR} ; mkdir -p ${TOPDIR} ; cd ${TOPDIR}
git clone git@gitlab.com:pytorch-complex/vitis_kernels.git ${TOPDIR}
git checkout embedded_part

# Reset the log files
printf '' > ${INFO_FILE} > ${DEBUG_FILE}

# Tail the info logfile as a background process so the contents of the
# info logfile are output to stdout.
tail -f ${INFO_FILE} &

# Set an EXIT trap to ensure your background process is
# cleaned-up when the script exits
trap "pkill -P $$" EXIT

# Redirect both stdout and stderr to write to the debug logfile
exec 1>>${DEBUG_FILE} 2>>${DEBUG_FILE}

# Write to info only. Remember to always append
echo "$(date) - Starting ${TOPDIR} Build" | tee -a ${INFO_FILE}
echo "$(date) - Environment Variables: "
for var in $(env); do
  echo "$var" | tee -a ${INFO_FILE}
done

printf "$(date) - download" | tee -a ${INFO_FILE}
mkdir -p ${TOPDIR}/third_party/Vitis_Libraries
cp -rf ${SRCDIR}/third_party/Vitis_Libraries ${TOPDIR}/third_party
ECHO_RESULT

printf "$(date) - patch" | tee -a ${INFO_FILE}
python configure.py -d ${XCL_DTYPE} -c ${XCL_CATEGORY}
ECHO_RESULT

printf "$(date) - clean" | tee -a ${INFO_FILE}
python setup.py clean
ECHO_RESULT

printf "$(date) - build" | tee -a ${INFO_FILE}
python setup.py install
ECHO_RESULT

printf "$(date) - install" | tee -a ${INFO_FILE}
# install ${TOPDIR}/images/sdcard.img ${IMG_FILE}
ECHO_RESULT

if [ ${XCL_EMULATION_MODE} != "hw" ]; then
printf "$(date) - test" | tee -a ${INFO_FILE}
EMCONFIG_PATH=/usr/local/share/applications/venv/bin \
    pytest --capture=sys --rootdir=${TOPDIR}/test --junitxml=${TOPDIR}/test/test_${XCL_DTYPE}_${XCL_CATEGORY}.xml --capture=sys ${TOPDIR}/test/test_torch.py::TestTensor::test_${XCL_CATEGORY}
ECHO_RESULT
fi

echo "$(date) - Publishing Build Artifacts:" | tee -a ${INFO_FILE}
# echo "BIN=${IMG_FILE}" | tee -a ${INFO_FILE}

if [ ${XCL_EMULATION_MODE} == "hw" ]; then
    rm -rf ${TOPDIR}
fi
URI="https://hooks.slack.com/services/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"
curl -X POST -H 'Content-type: application/json' --data "{\"text\": \"$(cat ${INFO_FILE})\"}" ${URI}
sync

