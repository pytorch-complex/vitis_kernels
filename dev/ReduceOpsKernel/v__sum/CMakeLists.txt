cmake_minimum_required(VERSION 3.10.0)

# Include
set(ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../..)
include(${ROOT_DIR}/root.cmake)

# Top-Level Macros
## Find package only when it is not a subproject
macro(find_package)
    if(NOT ${ARGV0} IN_LIST subprojects)
        _find_package(${ARGV})
    endif()
endmacro()

# Variables
set(USE_LIBTORCH 1)

# List Subprojects
set(XCL_OBJ_FILENAMES "")
set(XCL_OBJ_FILENAMES ${XCL_OBJ_FILENAMES} ${CMAKE_CURRENT_BINARY_DIR}/kernel/vf4sum/vf4sum.xo)
set(subprojects platform kernel link_ host xrt)
add_subdirectory(platform)
add_subdirectory(kernel)
add_subdirectory(link_)
add_subdirectory(host)
add_subdirectory(xrt)
