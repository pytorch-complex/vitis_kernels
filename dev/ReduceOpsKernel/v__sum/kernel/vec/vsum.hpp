#include <ATen/fpga/kernel/SharedReduceOps.h>

namespace at { namespace vec {

namespace r1 {

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class SumConfig {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 0; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class SumConfig<double, PAR_LOG_SIZE, double, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 3; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class SumConfig<float, PAR_LOG_SIZE, float, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 2; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
struct SumInit {
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> value() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void sum_combine(ReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a, const ReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &b) {
  a.value = a.value + b.value;
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void sum_project(ReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a) {
  return;
}

template <typename acc_t,
          typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class SumOps : ReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>{
   public:
    using super_t = ReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    static void reduce(acc_t &a,
                       const ReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::reduce(&sum_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
    }
    static void combine(acc_t &r, acc_t &a,
                        const ReduceData<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::combine(&sum_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
        sum_combine(r, a);
    }
    static void project(acc_t &a) {
        super_t::project(&sum_project<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a);
    }
};

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
void vsum(ap_uint<SIZE_BITS * RN_PAR_SIZE>* result,
          const ap_uint<PAR_SIZE_BITS>* a,
          size_t stride,
          size_t offset,
          size_t num_iters) {
#ifndef __SYNTHESIS__
    assert(stride % PAR_SIZE == 0);
#endif
    using config_t = SumConfig<T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    using r_init_t = SumInit<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using b_init_t = SumInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using c_init_t = SumInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using in_t = Vec<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using buf_t = ReduceData<ACC_T, config_t::buf_log_size(), config_t::rn_log_size()>;
    using acc_t = ReduceData<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using out_t = Vec<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using i_out_t = Vec<size_t, config_t::rn_log_size(), config_t::rn_log_size()>;
    using r_t = SumOps<acc_t, T, config_t::par_log_size(), ACC_T, config_t::rn_log_size()>;
    using c_t = SumOps<acc_t, ACC_T, config_t::buf_log_size(), ACC_T, config_t::rn_log_size()>;
    using p_t = SumOps<acc_t, ACC_T, config_t::rn_log_size(), ACC_T, config_t::rn_log_size()>;

    #pragma HLS DATAFLOW
    hls::stream<in_t> a_stream;
    hls::stream<acc_t> reduce_stream;
    hls::stream<buf_t> buffer_stream;
    hls::stream<acc_t> combine_stream;
    hls::stream<out_t> result_stream;
    #pragma HLS data_pack variable = a_stream
    #pragma HLS stream variable = reduce_stream depth = 2
    #pragma HLS data_pack variable = reduce_stream
    #pragma HLS stream variable = buffer_stream depth = 2
    #pragma HLS data_pack variable = buffer_stream
    #pragma HLS data_pack variable = combine_stream
    #pragma HLS data_pack variable = result_stream

    read_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(a_stream, a, stride, offset);
    reduce_dataflow<config_t, acc_t, in_t, r_init_t, r_t, T, ACC_T>(reduce_stream, a_stream, stride, num_iters);
    buffer_dataflow<config_t, buf_t, acc_t, b_init_t, ACC_T, ACC_T>(buffer_stream, reduce_stream, stride, num_iters);
    combine_dataflow<config_t, acc_t, buf_t, c_init_t, c_t, ACC_T, ACC_T>(combine_stream, buffer_stream, stride, num_iters);
    project_dataflow<config_t, out_t, i_out_t, acc_t, p_t, ACC_T, ACC_T>(result_stream, combine_stream, 1);
    write_dataflow<ACC_T, RN_LOG_SIZE>(result, result_stream, 1, offset);
}

} // namespace r1

namespace r2 {

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class SumConfig {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 0; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class SumConfig<double, PAR_LOG_SIZE, double, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 3; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class SumConfig<float, PAR_LOG_SIZE, float, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 2; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
struct SumInit {
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> value() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void sum_combine(ReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a, const ReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &b) {
  a.value = a.value + b.value;
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void sum_project(ReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a) {
  return;
}

template <typename acc_t,
          typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class SumOps : ReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>{
   public:
    using super_t = ReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    static void reduce(acc_t &a,
                       const ReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::reduce(&sum_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
    }
    static void combine(acc_t &r, acc_t &a,
                        const ReduceData<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::combine(&sum_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
        sum_combine(r, a);
    }
    static void project(acc_t &a) {
        super_t::project(&sum_project<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a);
    }
};

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
void vsum(ap_uint<SIZE_BITS * RN_PAR_SIZE>* result,
          const ap_uint<PAR_SIZE_BITS>* a,
          size_t stride,
          size_t offset,
          size_t num_iters) {
#ifndef __SYNTHESIS__
    assert(stride % PAR_SIZE == 0);
#endif
    using config_t = SumConfig<T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    using r_init_t = SumInit<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using b_init_t = SumInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using c_init_t = SumInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using in_t = Vec<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using buf_t = ReduceData<ACC_T, config_t::buf_log_size(), config_t::rn_log_size()>;
    using acc_t = ReduceData<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using out_t = Vec<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using i_out_t = Vec<size_t, config_t::rn_log_size(), config_t::rn_log_size()>;
    using r_t = SumOps<acc_t, T, config_t::par_log_size(), ACC_T, config_t::rn_log_size()>;
    using c_t = SumOps<acc_t, ACC_T, config_t::buf_log_size(), ACC_T, config_t::rn_log_size()>;
    using p_t = SumOps<acc_t, ACC_T, config_t::rn_log_size(), ACC_T, config_t::rn_log_size()>;

    #pragma HLS DATAFLOW
    hls::stream<in_t> a_stream;
    hls::stream<acc_t> reduce_stream;
    hls::stream<buf_t> buffer_stream;
    hls::stream<acc_t> combine_stream;
    hls::stream<out_t> result_stream;
    #pragma HLS data_pack variable = a_stream
    #pragma HLS stream variable = reduce_stream depth = 2
    #pragma HLS data_pack variable = reduce_stream
    #pragma HLS stream variable = buffer_stream depth = 2
    #pragma HLS data_pack variable = buffer_stream
    #pragma HLS data_pack variable = combine_stream
    #pragma HLS data_pack variable = result_stream

    read_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(a_stream, a, stride, offset);
    reduce_dataflow<config_t, acc_t, in_t, r_init_t, r_t, T, ACC_T>(reduce_stream, a_stream, stride, num_iters);
    buffer_dataflow<config_t, buf_t, acc_t, b_init_t, ACC_T, ACC_T>(buffer_stream, reduce_stream, stride, num_iters);
    combine_dataflow<config_t, acc_t, buf_t, c_init_t, c_t, ACC_T, ACC_T>(combine_stream, buffer_stream, stride, num_iters);
    project_dataflow<config_t, out_t, i_out_t, acc_t, p_t, ACC_T, ACC_T>(result_stream, combine_stream, 1);
    write_dataflow<ACC_T, RN_LOG_SIZE>(result, result_stream, 1, offset);
}

} // namespace r2

} } // namespace at::vec
