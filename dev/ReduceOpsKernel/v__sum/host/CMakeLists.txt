cmake_minimum_required(VERSION 3.10.0)
project(host VERSION 1.0)

# Include
set(ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../../..)
include(${ROOT_DIR}/root.cmake)

# Packages
find_package(OpenCL REQUIRED)
find_package(Threads REQUIRED)
find_package(Torch REQUIRED)

# Variables
set(OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/..)
set(ATEN_DIR ${ROOT_DIR}/aten/src)
set(SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/${XCL_OPT})
set(THREADS_PREFER_PTHREAD_FLAG ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${TORCH_CXX_FLAGS}")

# Compile
add_executable(host
	${SRC_DIR}/host.cpp)
set_target_properties(host PROPERTIES
	PUBLIC_HEADER "${headers}"
	RUNTIME_OUTPUT_DIRECTORY ${OUTPUT_DIR}
	OUTPUT_NAME test)
target_include_directories(host PUBLIC
	${XILINX_XRT}/include
	${XILINX_VIVADO}/include
	${ROOT_DIR}
	${ATEN_DIR})
target_compile_options(host PUBLIC -Wall -O0 -g)

# Link
target_link_libraries(host "${TORCH_LIBRARIES}")
target_link_libraries(host OpenCL::OpenCL)
target_link_libraries(host Threads::Threads)
target_link_libraries(host rt)
target_link_libraries(host stdc++)

