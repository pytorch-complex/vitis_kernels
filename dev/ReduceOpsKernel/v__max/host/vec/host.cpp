#include <ATen/native/TensorIterator.h>
#include <torch/torch.h>

#include <c10/fpga/FPGAFunctions.h>
#include <ATen/host/kernel/dtype.h>

namespace at { namespace native { namespace {

bool allclose(std::string kernel, Tensor &expected, Tensor &expected_index, Tensor &actual, Tensor &actual_index){
  bool match = torch::allclose(expected, actual) && torch::allclose(expected_index, expected_index);
  std::string sub_kernel = kernel.replace(1, 2, toID(expected.scalar_type()));
  std::cout << "TEST " << sub_kernel << ": " << (match ? "PASSED" : "FAILED") << std::endl;

  std::cout << "SW: " << expected << " : " <<  "HW: " << actual << std::endl;
  std::cout << "SW_INDEX: " << expected_index << " : " <<  "HW_INDEX: " << actual_index << std::endl;
  return match;
}

template <typename DTYPE>
void run_kernel(std::string kernel, TensorIterator iter){
  cl_int err;
  size_t n = iter.numel();
  size_t vector_size_bytes = iter.tensor(0).element_size()*n;
  size_t size_bytes = iter.tensor(0).element_size();
  cl::Context context = fpgaGetContext();
  cl::Program program = fpgaGetProgram();
  cl::CommandQueue q = fpgaGetQ();
  std::string sub_kernel = kernel.replace(1, 2, toID(iter.dtype()));

  // Launch Kernel
  OCL_CHECK(err, cl::Kernel k(program, sub_kernel.c_str(), &err));

  // Allocate Global Memory Buffers
  OCL_CHECK(err, cl::Buffer result_buffer(context, OUTPUT_BUFFER, size_bytes, iter.tensor(0).data_ptr(), &err));
  OCL_CHECK(err, cl::Buffer index_buffer(context, OUTPUT_BUFFER, size_bytes, iter.tensor(1).data_ptr(), &err));
  OCL_CHECK(err, cl::Buffer a_buffer(context, INPUT_BUFFER, vector_size_bytes, iter.tensor(2).data_ptr(), &err));
  size_t stride = XCL_TEST_SIZE/XCL_CU;
  size_t offset = 0;

  // Set Kernel Arguments
  OCL_CHECK(err, err = k.setArg(0, result_buffer));
  OCL_CHECK(err, err = k.setArg(1, index_buffer));
  OCL_CHECK(err, err = k.setArg(2, a_buffer));

  // Enqueue Input Memory Transfer
  std::vector<cl::Event> events{};
  cl::Event write_event;
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects({a_buffer}, 0/* 0 means from host*/, nullptr, &write_event));
  events.push_back(write_event);

  // Enqueue Kernels Tasks for Execution
  std::vector<cl::Event> task_events;
  OCL_CHECK(err, err = k.setArg(3, stride));
  for(size_t cu = 0; cu < XCL_CU; cu++) {
    cl::Event task_event;
    offset = cu*stride;
    OCL_CHECK(err, err = k.setArg(4, offset));
    OCL_CHECK(err, err = q.enqueueTask(k, &events, &task_event));
    task_events.push_back(task_event);
  }
  copy(begin(task_events), end(task_events), std::back_inserter(events));

  // Enqueue Output Memory Transfer
  std::vector<cl::Event> read_events{};
  cl::Event read_event;
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects({result_buffer, index_buffer}, CL_MIGRATE_MEM_OBJECT_HOST, &events, &read_event));
  events.push_back(read_event);

  q.finish();

  // Release Kernel
  k.~Kernel();
  k = cl::Kernel();
}

bool test_vmax() {
  std::list<ScalarType> dtype_list = {ScalarType::Float};
  bool match = true;
  std::string kernel = "v__max";
  std::list<ScalarType>::iterator dtype_iter = dtype_list.begin();
  std::cout<<XCL_TEST_SIZE<<std::endl;
  for (; dtype_iter != dtype_list.end(); ++dtype_iter){
    TensorOptions options = TensorOptions(*dtype_iter);
    Tensor a = torch::rand(XCL_TEST_SIZE, options);
    Scalar alpha = std::rand() % int(XCL_TEST_SIZE);
    auto max_tuple = torch::max(a, 0);
    Tensor sw_result = std::get<0>(max_tuple);
    Tensor sw_index = std::get<1>(max_tuple);
    Tensor hw_result = torch::zeros_like(sw_result);
    Tensor hw_index = torch::zeros_like(sw_index).to(options);
    auto iter = TensorIterator();
    iter.add_output(hw_result);
    iter.add_output(hw_index);
    iter.add_input(a);
    iter.build();

    AT_DISPATCH_FLOATING_TYPES(iter.dtype(), kernel, [&](){
      using vec_t = at::native::ztype<scalar_t>::vec_t;
      std::vector<vec_t> scalar_vec = {vec_t(alpha.to<scalar_t>())};
      run_kernel<vec_t>(kernel, iter);
      Tensor hw_result_out = iter.output(0)[0];
      Tensor hw_index_out = iter.output(1)[0];
      match &= allclose(kernel, sw_result, sw_index, hw_result_out, hw_index_out);
    });
  }
  return match;
}

}}}  // namespace at::native::<anonymous>


int main() {
  std::string bitname = "/home/jenkins/repos/vitis_kernels/aten/src/ATen/fpga/binary_container_1.xclbin";
  fpgaSetDevice(0);
  fpgaSetProgram(bitname);
  bool result = at::native::test_vmax() ? EXIT_SUCCESS : EXIT_FAILURE;
  fpgaReleaseProgram();
  fpgaReleaseDevice();
  return result;
}
