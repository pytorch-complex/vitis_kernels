#include <ATen/fpga/kernel/SharedArgReduceOps.h>

namespace at { namespace vec {

namespace r1 {

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class MaxConfig {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 0; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class MaxConfig<double, PAR_LOG_SIZE, double, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 3; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class MaxConfig<float, PAR_LOG_SIZE, float, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 2; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
struct MaxInit {
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> value() {
      return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(std::numeric_limits<T>::lowest());
  }
  static constexpr Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE> index() {
      return Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE>(size_t(0));
  }
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void max_combine(ArgReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a,
                 const ArgReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &b) {
  for (size_t i = 0; i < RN_PAR_SIZE; i++) {
    #pragma HLS UNROLL
    if (b.value[i] > a.value[i]) {
      a.value[i] = b.value[i];
      a.index[i] = b.index[i];
    }
  }
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void max_project(ArgReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a) {
  for (size_t i = 0; i < RN_PAR_SIZE; i++) {
    #pragma HLS UNROLL
    a.value[i] = a.value[i];
    a.index[i] = a.index[i];
  }
}

template <typename acc_t,
          typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class MaxOps : ArgReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>{
   public:
    using super_t = ArgReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    static void reduce(acc_t &a,
                       const ArgReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::reduce(&max_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
    }
    static void combine(acc_t &r, acc_t &a,
                        const ArgReduceData<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::combine(&max_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
        max_combine(r, a);
    }
    static void project(acc_t &a) {
        super_t::project(&max_project<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a);
    }
};

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
void vmax(ap_uint<SIZE_BITS>* result,
           ap_uint<SIZE_BITS>* i_result,
           const ap_uint<PAR_SIZE_BITS>* a,
           size_t stride,
           size_t offset,
           size_t num_iters) {
#ifndef __SYNTHESIS__
    assert(stride % PAR_SIZE == 0);
#endif
    using config_t = MaxConfig<T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    using r_init_t = MaxInit<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using b_init_t = MaxInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using c_init_t = MaxInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using in_t = Vec<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using buf_t = ArgReduceData<ACC_T, config_t::buf_log_size(), config_t::rn_log_size()>;
    using acc_t = ArgReduceData<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using out_t = Vec<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using i_out_t = Vec<size_t, config_t::rn_log_size(), config_t::rn_log_size()>;
    using r_t = MaxOps<acc_t, T, config_t::par_log_size(), ACC_T, config_t::rn_log_size()>;
    using c_t = MaxOps<acc_t, ACC_T, config_t::buf_log_size(), ACC_T, config_t::rn_log_size()>;
    using p_t = MaxOps<acc_t, ACC_T, config_t::rn_log_size(), ACC_T, config_t::rn_log_size()>;

    #pragma HLS DATAFLOW
    hls::stream<in_t> a_stream;
    hls::stream<acc_t> reduce_stream;
    hls::stream<buf_t> buffer_stream;
    hls::stream<acc_t> combine_stream;
    hls::stream<out_t> result_stream;
    hls::stream<i_out_t> i_result_stream;
    hls::stream<out_t> i_copy_result_stream;
    #pragma HLS data_pack variable = a_stream
    #pragma HLS stream variable = reduce_stream depth = 2
    #pragma HLS data_pack variable = reduce_stream
    #pragma HLS stream variable = buffer_stream depth = 2
    #pragma HLS data_pack variable = buffer_stream
    #pragma HLS data_pack variable = combine_stream
    #pragma HLS data_pack variable = result_stream
    #pragma HLS data_pack variable = i_result_stream
    #pragma HLS data_pack variable = i_copy_result_stream

    read_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(a_stream, a, stride, offset);
    arg_reduce_dataflow<config_t, acc_t, in_t, r_init_t, r_t, T, ACC_T>(reduce_stream, a_stream, stride, num_iters);
    arg_buffer_dataflow<config_t, buf_t, acc_t, b_init_t, ACC_T, ACC_T>(buffer_stream, reduce_stream, stride, num_iters);
    arg_combine_dataflow<config_t, acc_t, buf_t, c_init_t, c_t, ACC_T, ACC_T>(combine_stream, buffer_stream, stride, num_iters);
    arg_project_dataflow<config_t, out_t, i_out_t, acc_t, p_t, ACC_T, ACC_T>(result_stream, i_result_stream, combine_stream, 1);
    copy_dataflow<ACC_T, size_t, RN_LOG_SIZE>(i_copy_result_stream, i_result_stream, 1);
    write_dataflow<ACC_T, RN_LOG_SIZE>(result, result_stream, 1, offset);
    write_dataflow<ACC_T, RN_LOG_SIZE>(i_result, i_copy_result_stream, 1, offset);
}

} // namespace r1

} } // namespace at::vec
