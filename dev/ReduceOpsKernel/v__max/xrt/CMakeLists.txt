cmake_minimum_required(VERSION 3.10.0)
project(xrt NONE)

# Include
set(ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../../..)
include(${ROOT_DIR}/root.cmake)
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/xrt.ini.in ${CMAKE_CURRENT_SOURCE_DIR}/xrt.ini)

# Variables
set(XRT_SRCNAME ${CMAKE_CURRENT_SOURCE_DIR}/xrt.ini)
set(XRT_DESTNAME ${CMAKE_CURRENT_BINARY_DIR}/../xrt.ini)

# Compile
add_custom_command(OUTPUT ${XRT_DESTNAME}
	COMMAND cp ${XRT_SRCNAME} ${XRT_DESTNAME} || true
	COMMENT "Configuring Runtime:"
)

add_custom_target(xrt ALL
	DEPENDS ${XRT_DESTNAME}
)
