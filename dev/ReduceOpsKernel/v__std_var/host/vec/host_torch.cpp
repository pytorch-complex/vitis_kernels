#include <ATen/host/kernel/Loops.h>

namespace at { namespace native { namespace {

bool allclose(std::string kernel, Tensor& expected, Tensor& actual){
  bool match = torch::allclose(expected, actual);
  std::string sub_kernel = kernel.replace(1, 1, 1, toID(expected.scalar_type()));
  std::cout << "TEST " << sub_kernel << ": " << (match ? "PASSED" : "FAILED") << std::endl;
  return match;
}

bool test_vstd_var() {
  std::list<ScalarType> dtype_list = {ScalarType::Float};
  bool match = true;
  std::string kernel = "v_std_var";
  std::list<ScalarType>::iterator dtype_iter = dtype_list.begin();
  std::cout<<XCL_TEST_SIZE<<std::endl;
  for (; dtype_iter != dtype_list.end(); ++dtype_iter){
    TensorOptions options = TensorOptions(*dtype_iter);
    Tensor a = torch::rand(XCL_TEST_SIZE, options);
    Tensor b = torch::rand(XCL_TEST_SIZE, options);
    Scalar alpha_scalar = std::rand() % int(XCL_TEST_SIZE);
    Tensor sw_result = a + alpha_scalar * b;
    Tensor hw_result = torch::zeros(XCL_TEST_SIZE, options);
    auto iter = TensorIterator();
    iter.add_output(hw_result);
    iter.add_input(a);
    iter.add_input(b);
    iter.build();
    AT_DISPATCH_FLOATING_TYPES(iter.dtype(), kernel, [&](){
      using vec_t = at::native::ztype<scalar_t>::vec_t;
      std::vector<vec_t> scalar_vec = {vec_t(alpha_scalar.to<scalar_t>())};
      fpga_kernel(kernel, iter, scalar_vec);
      Tensor hw_result_out = iter.output(0);
      match &= allclose(kernel, sw_result, hw_result_out);
    });
  }
  return match;
}

}}}  // namespace at::native::<anonymous>

int main() {
  std::string bitname = "/home/jenkins/repos/vitis_kernels/aten/src/ATen/fpga/binary_container_1.xclbin";
  fpgaSetDevice(0);
  fpgaSetProgram(bitname);
  bool result = at::native::test_vstd_var() ? EXIT_SUCCESS : EXIT_FAILURE;
  fpgaReleaseProgram();
  fpgaReleaseDevice();
  return result;
}

