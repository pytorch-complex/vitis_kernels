#include <ATen/native/TensorIterator.h>
#include <torch/torch.h>

#include <c10/fpga/FPGAFunctions.h>
#include <ATen/host/kernel/dtype.h>

namespace at { namespace native { namespace {

void run_kernel(std::string kernel, Tensor &result){
  cl_int err;
  size_t n = result.numel();
  size_t vector_size_bytes = result.element_size()*n;
  cl::Context context = fpgaGetContext();
  cl::Program program = fpgaGetProgram();
  cl::CommandQueue q = fpgaGetQ();
  std::string sub_kernel = kernel.replace(1, 2, toID(result.scalar_type()));

  // Launch Kernel
  OCL_CHECK(err, cl::Kernel k(program, sub_kernel.c_str(), &err));

  // Allocate Global Memory Buffers
  OCL_CHECK(err, cl::Buffer result_buffer(context, OUTPUT_BUFFER, vector_size_bytes, result.data_ptr(), &err));
  size_t stride = XCL_TEST_SIZE/XCL_CU;
  size_t offset = 0;

  // Set Kernel Arguments
  OCL_CHECK(err, err = k.setArg(0, result_buffer));

  // Enqueue Kernels Tasks for Execution
  std::vector<cl::Event> events{};
  std::vector<cl::Event> task_events;
  OCL_CHECK(err, err = k.setArg(1, stride));
  for(size_t cu = 0; cu < XCL_CU; cu++) {
    cl::Event task_event;
    offset = cu*stride;
    OCL_CHECK(err, err = k.setArg(2, offset));
    OCL_CHECK(err, err = q.enqueueTask(k, &events, &task_event));
    task_events.push_back(task_event);
  }
  copy(begin(task_events), end(task_events), std::back_inserter(events));

  // Enqueue Output Memory Transfer
  std::vector<cl::Event> read_events{};
  cl::Event read_event;
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects({result_buffer}, CL_MIGRATE_MEM_OBJECT_HOST, &events, &read_event));
  events.push_back(read_event);

  q.finish();

  // Release Kernel
  k.~Kernel();
  k = cl::Kernel();
}

bool test_vnormal() {
  std::list<ScalarType> dtype_list = {ScalarType::Float};
  bool match = true;
  std::string kernel = "v__normal";
  std::list<ScalarType>::iterator dtype_iter = dtype_list.begin();
  std::cout<<XCL_TEST_SIZE<<std::endl;
  for (; dtype_iter != dtype_list.end(); ++dtype_iter){
    TensorOptions options = TensorOptions(*dtype_iter);
    Tensor hw_result = torch::zeros(XCL_TEST_SIZE, options);

    AT_DISPATCH_FLOATING_TYPES(hw_result.scalar_type(), kernel, [&](){
      run_kernel(kernel, hw_result);
      std::cout<<"result_val[0]: "<<hw_result[0]<<std::endl;
      std::cout<<"result_val[-1]: "<<hw_result[XCL_TEST_SIZE - 1]<<std::endl;
      std::cout<<"mean: "<<hw_result.mean()<<std::endl;
      std::cout<<"std: "<<hw_result.std()<<std::endl;
    });
  }
  return match;
}

}}}  // namespace at::native::<anonymous>


int main() {
  std::string bitname = "/home/jenkins/repos/vitis_kernels/aten/src/ATen/fpga/binary_container_1.xclbin";
  fpgaSetProgramFilename(bitname);
  fpgaSetDevice(0);
  bool result = at::native::test_vnormal() ? EXIT_SUCCESS : EXIT_FAILURE;
  fpgaReleaseDevice();
  return result;
}

