#include <ATen/fpga/kernel/Dataflow.h>
#include <third_party/Vitis_Libraries/quantitative_finance/L1/include/xf_fintech/rng.hpp>

namespace at { namespace vec {

namespace r1 {

template <typename T>
void to(T &val, ap_ufixed<SIZE_BITS, 0> &ufixed) {}

template <>
void to<float>(float &val, ap_ufixed<32, 0> &ufixed) {
  #pragma HLS INLINE
  val = ufixed.to_float();
}

template <>
void to<double>(double &val, ap_ufixed<64, 0> &ufixed) {
  #pragma HLS INLINE
  val = ufixed.to_double();
}

template <typename T>
void vnormal_dataflow(hls::stream<T> &result_stream,
                      xf::fintech::MT2203IcnRng<T> &generator,
                      size_t stride) {
  ap_ufixed<SIZE_BITS, 0> return_ufixed;
  T return_val;
  vnormal: for (size_t j = 0; j < stride; j++){
#pragma HLS PIPELINE II=1
    return_val = generator.next();
//    return_ufixed = generator.next();
//    to<T>(return_val, return_ufixed);
    if (j == 0) {std::cout<<"result_val[0]: "<<return_val<<std::endl; }
    else if (j == stride -1) { std::cout<<"result_val[-1]: "<<return_val<<std::endl; }
    result_stream << return_val;
  }
}

template <typename T>
void vnormal(ap_uint<SIZE_BITS>* result,
             size_t stride,
             size_t offset) {

#pragma HLS dataflow
  hls::stream<T> result_stream("result_write");

  static xf::fintech::MT2203IcnRng<T> generator = xf::fintech::MT2203IcnRng<T>(1.0);
  vnormal_dataflow<T>(result_stream, generator, stride);
  write_dataflow<T>(result, result_stream, stride, offset);
}

} // namespace r1

} } // namespace at::vec
