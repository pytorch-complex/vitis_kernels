#include <ATen/fpga/kernel/Dataflow.h>
#include <third_party/Vitis_Libraries/quantitative_finance/L1/include/xf_fintech/rng.hpp>

namespace at { namespace vec {

namespace r1 {

template <typename gen_t, typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vnormal_dataflow(hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > &result_stream,
                      gen_t generator[PAR_SIZE],
                      size_t stride) {
  const size_t par_stride = (stride << Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::rn_log_size()) >> Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_log_size();
  vnormal: for (size_t j = 0; j < par_stride; j++){
    #pragma HLS PIPELINE II=1
    result_stream << Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(generator);
  }
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vnormal(ap_uint<PAR_SIZE_BITS>* result,
             size_t stride,
             size_t offset) {

  using gen_t = xf::fintech::MT2203IcnRng<T>;
  //Todo: figure out whe the following doesn't work in v++
  // char str[3] = "abc"; // str has type char[3] and holds 'a', 'b', 'c'
  static gen_t generator[] = {gen_t(1), gen_t(2), gen_t(3), gen_t(4), gen_t(5), gen_t(6), gen_t(7), gen_t(8),
                              gen_t(9), gen_t(10), gen_t(11), gen_t(12), gen_t(13), gen_t(14), gen_t(15), gen_t(16)};

  #pragma HLS DATAFLOW
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > result_stream("result_write");
  #pragma HLS data_pack variable = result_stream
  #pragma HLS array_partition variable=generator block factor=16 dim=1

  vnormal_dataflow<gen_t, T, PAR_LOG_SIZE, RN_LOG_SIZE>(result_stream, generator, stride);
  write_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result, result_stream, stride, offset);
}

} // namespace r1

namespace r2 {

template <typename gen_t, typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vnormal_dataflow(hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > &result_stream,
                      gen_t generator[PAR_SIZE],
                      size_t stride) {
  const size_t par_stride = (stride << Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::rn_log_size()) >> Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_log_size();
  vnormal: for (size_t j = 0; j < par_stride; j++){
    #pragma HLS PIPELINE II=1
    result_stream << Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(generator);
  }
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vnormal(ap_uint<PAR_SIZE_BITS>* result,
             size_t stride,
             size_t offset) {

  using gen_t = xf::fintech::MT2203IcnRng<T>;
  //Todo: figure out whe the following doesn't work in v++
  // char str[3] = "abc"; // str has type char[3] and holds 'a', 'b', 'c'
  static gen_t generator[] = {gen_t(1), gen_t(2), gen_t(3), gen_t(4), gen_t(5), gen_t(6), gen_t(7), gen_t(8),
                              gen_t(9), gen_t(10), gen_t(11), gen_t(12), gen_t(13), gen_t(14), gen_t(15), gen_t(16)};

  #pragma HLS DATAFLOW
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > result_stream("result_write");
  #pragma HLS data_pack variable = result_stream
  #pragma HLS array_partition variable=generator block factor=16 dim=1

  vnormal_dataflow<gen_t, T, PAR_LOG_SIZE, RN_LOG_SIZE>(result_stream, generator, stride);
  write_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result, result_stream, stride, offset);
}

} // namespace r2

} } // namespace at::vec


