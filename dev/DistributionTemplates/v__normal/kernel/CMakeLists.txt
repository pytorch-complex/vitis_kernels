cmake_minimum_required(VERSION 3.10.0)
project(kernel NONE)

# Include
set(ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../../..)
include(${ROOT_DIR}/root.cmake)

# Variables
set(CMAKE_VXX_COMPILER ${XILINX_VITIS}/bin/v++)
set(ATEN_DIR ${ROOT_DIR}/aten/src)
set(SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/${XCL_OPT})
set(LOG_DIR ${CMAKE_CURRENT_BINARY_DIR}/..)
set(${OBJ_FILENAMES} "")

# Build
set(XCL_KERNEL_NAME vf4normal)
set(OBJ_FILENAME ${CMAKE_CURRENT_BINARY_DIR}/${XCL_KERNEL_NAME}/${XCL_KERNEL_NAME}.xo)
set(OBJ_FILENAMES ${OBJ_FILENAMES} ${OBJ_FILENAME})
add_custom_command(OUTPUT ${OBJ_FILENAME}
	COMMAND ${CMAKE_VXX_COMPILER} -c
	     -t ${XCL_EMULATION_MODE}
         -k ${XCL_KERNEL_NAME}
	     -I ${ROOT_DIR}
	     -I ${ATEN_DIR}
         -I ${SRC_DIR}
	     -o ${OBJ_FILENAME}
	     --debug
	     --platform ${XCL_PLATFORM}
	     --profile_kernel data:all:all:all
	     --advanced.prop kernel.${XCL_KERNEL_NAME}.kernel_flags=-std=c++0x
	     --log_dir ${LOG_DIR}
         --report_dir ${LOG_DIR}
         ${SRC_DIR}/${XCL_KERNEL_NAME}/${XCL_KERNEL_NAME}.cpp
	COMMENT "Compiling target: ${OBJ_FILENAME}"
)

add_custom_target(kernel ALL
	DEPENDS ${OBJ_FILENAMES}
)
