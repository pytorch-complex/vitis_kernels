#include <vadd.hpp>

extern "C" {

void vf4add(float *result, const float *a, const float *b, float alpha, size_t stride, size_t offset) {
#pragma HLS INTERFACE s_axilite port=result bundle=control
#pragma HLS INTERFACE s_axilite port=a  bundle=control
#pragma HLS INTERFACE s_axilite port=b  bundle=control
#pragma HLS INTERFACE s_axilite port=alpha  bundle=control
#pragma HLS INTERFACE s_axilite port=stride bundle=control
#pragma HLS INTERFACE s_axilite port=offset bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

#pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem1
#pragma HLS INTERFACE m_axi port=a  offset=slave bundle=gmem2
#pragma HLS INTERFACE m_axi port=b  offset=slave bundle=gmem3
  vadd<float>(result, a, b, alpha, stride, offset);
}

}
