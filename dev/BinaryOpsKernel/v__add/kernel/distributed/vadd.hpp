#include <ATen/fpga/kernel/Dataflow.h>

template <typename DTYPE>
void vadd_dataflow(hls::stream<DTYPE>& result_stream,
               hls::stream<DTYPE>& a_stream,
               hls::stream<DTYPE>& b_stream,
               DTYPE alpha,
               size_t stride) {

  DTYPE a, b;
  vadd: for (size_t j = 0; j < stride; j++){
#pragma HLS PIPELINE II=1
    a_stream >> a;
    b_stream >> b;
    result_stream << a + alpha * b;
  }
}

template <typename DTYPE>
void vadd(DTYPE *result,
          const DTYPE *a,
          const DTYPE *b,
          DTYPE alpha,
          size_t stride,
          size_t offset) {

#pragma HLS dataflow
  hls::stream<DTYPE> result_stream("result_write");
  hls::stream<DTYPE> a_stream("a_read");
  hls::stream<DTYPE> b_stream("b_read");

  read_binary_dataflow<DTYPE>(a_stream, b_stream, a, b, stride, offset);
  vadd_dataflow<DTYPE>(result_stream, a_stream, b_stream, alpha, stride);
  write_dataflow<DTYPE>(result, result_stream, stride, offset);
}
