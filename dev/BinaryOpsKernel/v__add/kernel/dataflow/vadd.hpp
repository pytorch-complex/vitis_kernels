#include <ATen/fpga/kernel/Dataflow.h>

namespace at { namespace vec {

namespace r1 {

template <typename T>
void vadd_dataflow(hls::stream<T>& result_stream,
               hls::stream<T>& a_stream,
               hls::stream<T>& b_stream,
               T alpha,
               size_t stride) {

  T a, b;
  vadd: for (size_t j = 0; j < stride; j++){
#pragma HLS PIPELINE II=1
    a_stream >> a;
    b_stream >> b;
    result_stream << a + alpha * b;
  }
}

template <typename T>
void vadd(ap_uint<SIZE_BITS>* *result,
          const ap_uint<SIZE_BITS>* a,
          const ap_uint<SIZE_BITS>* b,
          const ap_uint<SIZE_BITS>* alpha,
          size_t stride,
          size_t offset) {

#pragma HLS dataflow
  hls::stream<T> result_stream("result_write");
  hls::stream<T> a_stream("a_read");
  hls::stream<T> b_stream("b_read");

  BitConv<T> conv;
  T alpha_val = conv.toType(alpha(SIZE_BITS - 1, 0));
  read_dataflow<T>(a_stream, a, stride, offset);
  read_dataflow<T>(b_stream, b, stride, offset);
  vadd_dataflow<T>(result_stream, a_stream, b_stream, alpha_val, stride);
  write_dataflow<T>(result, result_stream, stride, offset);
}

} // namespace r1

} } // namespace at::vec
