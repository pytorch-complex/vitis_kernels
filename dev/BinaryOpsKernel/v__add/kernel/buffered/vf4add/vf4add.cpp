#include <ATen/fpga/kernel/Dataflow.h>

#define LOCAL_MEM_SIZE 1024

extern "C" {

void vf4add_buffered(float *result, const float *a, const float *b, float alpha, size_t stride, size_t offset) {
  float result_local[LOCAL_MEM_SIZE];
  float a_local[LOCAL_MEM_SIZE];
  float b_local[LOCAL_MEM_SIZE];

  for(size_t i = 0; i < stride;  i += LOCAL_MEM_SIZE)
  {
    size_t local_stride = ((i + LOCAL_MEM_SIZE) > stride) ? stride - i : LOCAL_MEM_SIZE;

    read: for (size_t j = 0; j < local_stride; j++){
      a_local[j] = a[i + j];
      b_local[j] = b[i + j];
    }

    vadd: for (size_t j = 0; j < local_stride; j++){
    #pragma HLS PIPELINE II=1
      result_local[j] = a_local[j] + alpha * b_local[j];
    }

    write: for (size_t j = 0; j < local_stride; j++){
      result[i + j] = result_local[j];
    }
  }
}

void vf4add(float *result, const float *a, const float *b, float alpha, size_t stride, size_t offset) {
#pragma HLS INTERFACE s_axilite port=result bundle=control
#pragma HLS INTERFACE s_axilite port=a  bundle=control
#pragma HLS INTERFACE s_axilite port=b  bundle=control
#pragma HLS INTERFACE s_axilite port=alpha  bundle=control
#pragma HLS INTERFACE s_axilite port=stride bundle=control
#pragma HLS INTERFACE s_axilite port=offset bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control

#pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem1
#pragma HLS INTERFACE m_axi port=a  offset=slave bundle=gmem2
#pragma HLS INTERFACE m_axi port=b  offset=slave bundle=gmem3

  vf4add_buffered(result, a, b, alpha, stride, offset);
}

}
