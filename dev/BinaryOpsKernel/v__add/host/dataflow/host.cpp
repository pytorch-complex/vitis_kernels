#include <ATen/native/TensorIterator.h>
#include <torch/torch.h>

#include <c10/fpga/FPGAFunctions.h>
#include <ATen/host/kernel/dtype.h>

namespace at { namespace native { namespace {

bool allclose(std::string kernel, Tensor& expected, Tensor& actual){
  bool match = torch::allclose(expected, actual);
  std::string sub_kernel = kernel.replace(1, 2, toID(expected.scalar_type()));
  std::cout << "TEST " << sub_kernel << ": " << (match ? "PASSED" : "FAILED") << std::endl;
  return match;
}

template <typename T>
void run_kernel(std::string kernel, TensorIterator iter, T alpha){
  cl_int err;
  size_t n = iter.numel();
  size_t vector_size_bytes = iter.tensor(0).element_size()*n;
  cl::Context context = fpgaGetContext();
  cl::Program program = fpgaGetProgram();
  cl::CommandQueue q = fpgaGetQ();
  std::string sub_kernel = kernel.replace(1, 2, toID(iter.dtype()));

  // Launch Kernel
  OCL_CHECK(err, cl::Kernel k(program, sub_kernel.c_str(), &err));

  // Allocate Global Memory Buffers
  OCL_CHECK(err, cl::Buffer result_buffer(context, OUTPUT_BUFFER, vector_size_bytes, iter.tensor(0).data_ptr(), &err));
  OCL_CHECK(err, cl::Buffer a_buffer(context, INPUT_BUFFER, vector_size_bytes, iter.tensor(1).data_ptr(), &err));
  OCL_CHECK(err, cl::Buffer b_buffer(context, INPUT_BUFFER, vector_size_bytes, iter.tensor(2).data_ptr(), &err));
  size_t stride = XCL_TEST_SIZE/XCL_CU;
  size_t offset = 0;

  // Set Kernel Arguments
  OCL_CHECK(err, err = k.setArg(0, result_buffer));
  OCL_CHECK(err, err = k.setArg(1, a_buffer));
  OCL_CHECK(err, err = k.setArg(2, b_buffer));
  OCL_CHECK(err, err = k.setArg(3, alpha));
  OCL_CHECK(err, err = k.setArg(4, stride));
  OCL_CHECK(err, err = k.setArg(5, offset));

  // Enqueue Input Memory Transfer
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects({a_buffer, b_buffer}, 0/* 0 means from host*/));

  // Enqueue Kernels Tasks for Execution
  OCL_CHECK(err, err = q.enqueueTask(k));

  // Enqueue Output Memory Transfer
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects({result_buffer}, CL_MIGRATE_MEM_OBJECT_HOST));

  q.finish();

  // Release Kernel
  k.~Kernel();
  k = cl::Kernel();
}

bool test_vadd() {
  std::list<ScalarType> dtype_list = {ScalarType::Float};
  bool match = true;
  std::string kernel = "v__add";
  std::list<ScalarType>::iterator dtype_iter = dtype_list.begin();
  std::cout<<XCL_TEST_SIZE<<std::endl;
  for (; dtype_iter != dtype_list.end(); ++dtype_iter){
    TensorOptions options = TensorOptions(*dtype_iter);
    Tensor a = torch::rand(XCL_TEST_SIZE, options);
    Tensor b = torch::rand(XCL_TEST_SIZE, options);
    Scalar alpha = std::rand() % int(XCL_TEST_SIZE);
    Tensor sw_result = a + alpha*b;
    Tensor hw_result = torch::zeros(XCL_TEST_SIZE, options);
    auto iter = TensorIterator();
    iter.add_output(hw_result);
    iter.add_input(a);
    iter.add_input(b);
    iter.build();

    AT_DISPATCH_FLOATING_TYPES(iter.dtype(), kernel, [&](){
      run_kernel<scalar_t>(kernel, iter, alpha.to<scalar_t>());
      Tensor hw_result_out = iter.output(0);
      match &= allclose(kernel, sw_result, hw_result_out);
    });
  }
  return match;
}

}}}  // namespace at::native::<anonymous>


int main() {
  std::string bitname = "/home/jenkins/repos/vitis_kernels/aten/src/ATen/fpga/binary_container_1.xclbin";
  fpgaSetProgramFilename(bitname);
  fpgaSetDevice(0);
  bool result = at::native::test_vadd() ? EXIT_SUCCESS : EXIT_FAILURE;
  fpgaReleaseDevice();
  return result;
}

