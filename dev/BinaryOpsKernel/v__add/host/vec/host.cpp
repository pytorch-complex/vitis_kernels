#include <torch/torch.h>

#include <c10/fpga/FPGAFunctions.h>
#include <ATen/host/kernel/dtype.h>

namespace at { namespace native { namespace {

bool allclose(std::string kernel, Tensor& expected, Tensor& actual){
  bool match = torch::allclose(expected, actual);
  std::string sub_kernel = kernel.replace(1, 2, toID(expected.scalar_type()));
  std::cout << "TEST " << sub_kernel << ": " << (match ? "PASSED" : "FAILED") << std::endl;
  return match;
}

template <typename T>
void run_kernel(std::string kernel, Tensor& out, Tensor& a, Tensor& b, std::vector<T>& scalar_vec){
  cl_int err;
  size_t n = a.numel();
  size_t vector_size_bytes = a.element_size()*n;
  size_t scalar_size_bytes = scalar_vec[0].par_size_bytes();
  cl::Context context = fpgaGetContext();
  cl::Program program = fpgaGetProgram();
  cl::CommandQueue q = fpgaGetQ();
  std::string sub_kernel = kernel.replace(1, 2, toID(a.scalar_type()));

  // Launch Kernel
  OCL_CHECK(err, cl::Kernel k(program, sub_kernel.c_str(), &err));

  // Allocate Global Memory Buffers
  OCL_CHECK(err, cl::Buffer result_buffer(context, OUTPUT_BUFFER, vector_size_bytes, out.data_ptr(), &err));
  OCL_CHECK(err, cl::Buffer a_buffer(context, INPUT_BUFFER, vector_size_bytes, a.data_ptr(), &err));
  OCL_CHECK(err, cl::Buffer b_buffer(context, INPUT_BUFFER, vector_size_bytes, b.data_ptr(), &err));
  OCL_CHECK(err, cl::Buffer alpha_buffer(context, INPUT_BUFFER, scalar_size_bytes, scalar_vec[0].data_ptr(), &err));
  size_t stride = XCL_TEST_SIZE/XCL_CU;
  size_t offset = 0;

  // Set Kernel Arguments
  OCL_CHECK(err, err = k.setArg(0, result_buffer));
  OCL_CHECK(err, err = k.setArg(1, a_buffer));
  OCL_CHECK(err, err = k.setArg(2, b_buffer));
  OCL_CHECK(err, err = k.setArg(3, alpha_buffer));

  // Enqueue Input Memory Transfer
  std::vector<cl::Event> events{};
  cl::Event write_event;
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects({a_buffer, b_buffer, alpha_buffer}, 0/* 0 means from host*/, nullptr, &write_event));
  events.push_back(write_event);

  // Enqueue Kernels Tasks for Execution
  std::vector<cl::Event> task_events;
  OCL_CHECK(err, err = k.setArg(4, stride));
  for(size_t cu = 0; cu < XCL_CU; cu++) {
    cl::Event task_event;
    offset = cu*stride;
    OCL_CHECK(err, err = k.setArg(5, offset));
    OCL_CHECK(err, err = q.enqueueTask(k, &events, &task_event));
    task_events.push_back(task_event);
  }
  copy(begin(task_events), end(task_events), std::back_inserter(events));

  // Enqueue Output Memory Transfer
  std::vector<cl::Event> read_events{};
  cl::Event read_event;
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects({result_buffer}, CL_MIGRATE_MEM_OBJECT_HOST, &events, &read_event));
  events.push_back(read_event);

  q.finish();

  // Release Kernel
  k.~Kernel();
  k = cl::Kernel();
}

bool test_vadd() {
  std::list<ScalarType> dtype_list = {ScalarType::Float};
  bool match = true;
  std::string kernel = "v__add";
  std::list<ScalarType>::iterator dtype_iter = dtype_list.begin();
  std::cout<<"NUMEL"<<XCL_TEST_SIZE<<std::endl;
  for (; dtype_iter != dtype_list.end(); ++dtype_iter){
    TensorOptions options = TensorOptions(*dtype_iter);
    Tensor a = torch::rand(XCL_TEST_SIZE, options);
    Tensor b = torch::rand(XCL_TEST_SIZE, options);
    Scalar alpha = std::rand() % int(XCL_TEST_SIZE);
    Tensor sw_result = a + alpha * b;
    Tensor hw_result = torch::zeros(XCL_TEST_SIZE, options);

    switch (*dtype_iter)
    {
      case ScalarType::Float:
      {
        using vec_t = at::native::ztype<float>::vec_t;
        std::vector<vec_t> scalar_vec = {vec_t(alpha.to<float>())};
        run_kernel<vec_t>(kernel, hw_result, a, b, scalar_vec);
        match &= allclose(kernel, sw_result, hw_result);
        break;
      }
      default:
      {
        match &= false;
        break;
      }
    }
  }
  return match;
}

}}}  // namespace at::native::<anonymous>


int main() {
  std::string bitname = "/home/dylan_bespalko/repos/vitis_kernels/aten/src/ATen/fpga/binary_container_1.xclbin";
  fpgaSetProgramFilename(bitname);
  fpgaSetDevice(0);
  bool result = at::native::test_vadd() ? EXIT_SUCCESS : EXIT_FAILURE;
  fpgaReleaseProgram();
  fpgaReleaseDevice();
  return result;
}

