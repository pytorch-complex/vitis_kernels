import numpy as np
from timeit import Timer

def vf4add(result, a, b, alpha):
    result = a + alpha * b

if __name__ == "__main__":
    size = 16384
    a = np.random.ranf(size)
    b = np.random.ranf(size)
    alpha = 1.0
    result = np.zeros(size, dtype=np.float)

    t = Timer(lambda: vf4add(result, a, b, alpha))
    print(t.timeit(number=1))
