cmake_minimum_required(VERSION 3.10.0)
project(link NONE)

# Include
set(ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../../..)
include(${ROOT_DIR}/root.cmake)

# Variables
set(CMAKE_VXX_COMPILER ${XILINX_VITIS}/bin/v++)
set(ATEN_DIR ${ROOT_DIR}/aten/src)
set(LOG_DIR ${CMAKE_CURRENT_BINARY_DIR}/..)
set(CFG_FILENAME ${ROOT_DIR}/${XCL_PLATFORM}.cfg)
set(BIN_FILENAME ${ATEN_DIR}/ATen/fpga/binary_container_1.xclbin)

# Link
add_custom_command(OUTPUT ${BIN_FILENAME}
        COMMAND ${CMAKE_VXX_COMPILER} -l
             --platform ${XCL_PLATFORM}
             --target ${XCL_EMULATION_MODE}
             -o ${BIN_FILENAME}
             --debug
             --profile_kernel data:all:all:all
	     --config ${CFG_FILENAME}
             #--temp_dir ${LOG_DIR}
             --log_dir ${LOG_DIR}
             --report_dir ${LOG_DIR}
             #--save-temps
             ${XCL_OBJ_FILENAMES}
        COMMENT "Linking target: ${BIN_FILENAME}"
        DEPENDS ${XCL_OBJ_FILENAMES}
)

add_custom_target(link ALL
	DEPENDS ${BIN_FILENAME}
)
