#include <ap_int.h>
#include <iostream>
#include <iomanip> // For std::setw
#include <cstdint>  // For uint64_t

#include <torch/torch.h>

#include <c10/fpga/FPGAFunctions.h>
#include <ATen/host/kernel/dtype.h>

namespace at { namespace native { namespace {

bool allclose(std::string kernel, Tensor& expected, Tensor& actual){
  for (int n = 0; n < expected.numel(); ++n) {
    std::cout << "    IND = "  << std::left  << std::setw(6) << n
              << "    Real = " << std::right << std::setw(6) << torch::real(expected.index({n})).item() 
              << " : "         << std::left  << std::setw(6) << torch::real(actual.index({n})).item()
              << "    Imag = " << std::right << std::setw(6) << torch::imag(expected.index({n})).item() 
              << " : "         << std::left  << std::setw(6) << torch::imag(actual.index({n})).item()
              << std::endl;
  }
  bool match = torch::allclose(expected, actual);
  std::string sub_kernel = kernel.replace(1, 2, toID(expected.scalar_type()));
  std::cout << "TEST " << sub_kernel << ": " << (match ? "PASSED" : "FAILED") << std::endl;
  return match;
}

template <typename T>
T* aligned_alloc(std::size_t num) {
    void* ptr = nullptr;
    if (posix_memalign(&ptr, 4096, num * sizeof(T))) throw std::bad_alloc();
    return reinterpret_cast<T*>(ptr);
}

Tensor mytrunc(const Tensor& in) {
    return torch::view_as_complex(torch::view_as_real(in).trunc());
}

template <typename T>
void run_kernel(std::string kernel, Tensor& out, Tensor& a){
  // impulse as input
  Tensor a_qint = torch::view_as_real(a).trunc().to(torch::kInt);
  Tensor out_qint = torch::zeros_like(a_qint);

  /* Optional: Copy Tensor<ComplexFloat> in to ap_uint<512>
  ap_uint<512>* in = aligned_alloc<ap_uint<512> >(FFT_LEN * N_FFT / SSR);
  ap_uint<512>* ou = aligned_alloc<ap_uint<512> >(FFT_LEN * N_FFT / SSR);
  float r_val, i_val;
  for (int n = 0; n < N_FFT; ++n) {
      for (int t = 0; t < FFT_LEN / SSR; ++t) {
          for (int r = 0; r < SSR; r++) {
              r_val = torch::real(a).index({n * (FFT_LEN/SSR*SSR) + t * (SSR) + r}).data<float>()[0];
              in[n * FFT_LEN / SSR + t].range(31 + 64 * r, 0  + 64 * r) = static_cast<uint64_t>(std::trunc(r_val));
              i_val = torch::imag(a).index({n * (FFT_LEN/SSR*SSR) + t * (SSR) + r}).data<float>()[0];
              in[n * FFT_LEN / SSR + t].range(63 + 64 * r, 32 + 64 * r) = static_cast<uint64_t>(std::trunc(i_val));
          }
      }
  }
  */
  std::cout << "Host buffer has been allocated and set.\n";

  // Get CL devices.
  cl_int err;
  size_t n = a.numel();
  size_t vector_size_bytes = a.element_size()*n;
  std::cout << "numel: " << n << std::endl;
  std::cout << "vector_size_bytes: " << vector_size_bytes << std::endl;
  std::cout << "kernel_size_bytes: " << (size_t)(sizeof(ap_uint<512>) * (FFT_LEN * N_FFT / SSR)) << std::endl;
  cl::Context context = fpgaGetContext();
  cl::Program program = fpgaGetProgram();
  cl::CommandQueue q = fpgaGetQ();
  std::string sub_kernel = kernel.replace(1, 2, toID(a.scalar_type()));

  // Launch Kernel
  OCL_CHECK(err, cl::Kernel k(program, sub_kernel.c_str(), &err));

  // Allocate Global Memory Buffers 
  /* Optional: Pass ap_uint<512> buffers
  OCL_CHECK(err, cl::Buffer out_buffer(context, OUTPUT_BUFFER, vector_size_bytes, reinterpret_cast<void*>(ou), &err));
  OCL_CHECK(err, cl::Buffer a_buffer(context, INPUT_BUFFER, vector_size_bytes, reinterpret_cast<void*>(in), &err));
  */
  OCL_CHECK(err, cl::Buffer out_buffer(context, OUTPUT_BUFFER, vector_size_bytes, out_qint.data_ptr(), &err));
  OCL_CHECK(err, cl::Buffer a_buffer(context, INPUT_BUFFER, vector_size_bytes, a_qint.data_ptr(), &err));
  std::cout << "DDR buffers have been mapped/copy-and-mapped\n";

  // Set Kernel Arguments
  OCL_CHECK(err, err = k.setArg(0, out_buffer));
  OCL_CHECK(err, err = k.setArg(1, a_buffer));
  OCL_CHECK(err, err = k.setArg(2, N_FFT));

  // Enqueue Input Memory Transfer
  std::vector<cl::Event> events{};
  cl::Event write_event;
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects({a_buffer}, 0/* 0 means from host*/, nullptr, &write_event));
  events.push_back(write_event);

  // Enqueue Kernels Tasks for Execution
  std::vector<cl::Event> task_events;
  cl::Event task_event;
  OCL_CHECK(err, err = q.enqueueTask(k, &events, &task_event));
  task_events.push_back(task_event);
  copy(begin(task_events), end(task_events), std::back_inserter(events));

  // Enqueue Output Memory Transfer
  std::vector<cl::Event> read_events{};
  cl::Event read_event;
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects({out_buffer}, CL_MIGRATE_MEM_OBJECT_HOST, &events, &read_event));
  events.push_back(read_event);

  q.finish();

  // Release Kernel
  k.~Kernel();
  k = cl::Kernel();

  // step as output
  out = torch::view_as_complex(out_qint.to(torch::kFloat));

  /* Optional: Copy ap_uint<512> into Tensor<ComplexFloat>
  c10::complex<float> val;
  for (int n = 0; n < N_FFT; ++n) {
      for (int t = 0; t < FFT_LEN / SSR; ++t) {
          for (int r = 0; r < SSR; r++) {
              r_val = static_cast<int32_t>(ou[n * FFT_LEN / SSR + t].range(31 + 64 * r, 0  + 64 * r));
              i_val = static_cast<int32_t>(ou[n * FFT_LEN / SSR + t].range(63 + 64 * r, 32 + 64 * r));
              val = c10::complex<float>(r_val, i_val);
              out.index_put_({n * (FFT_LEN/SSR*SSR) + t * (SSR) + r}, val);
          }
      }
  }
  */
}

bool test_vfft(){
  std::list<ScalarType> dtype_list = {ScalarType::ComplexFloat};
  bool match = true;
  std::string kernel = "v__fft";
  std::list<ScalarType>::iterator dtype_iter = dtype_list.begin();
  for (; dtype_iter != dtype_list.end(); ++dtype_iter){
    TensorOptions options = TensorOptions(*dtype_iter);
    Tensor a = torch::rand(N_FFT * FFT_LEN, options) * 10;
    // Tensor a = torch::zeros(N_FFT * FFT_LEN, options) * 10;
    // a.index_put_({0}, 1);
    Tensor sw_result = mytrunc(torch::fft::fft(mytrunc(a)));
    Tensor hw_result = torch::zeros(N_FFT * FFT_LEN, options);
    switch (*dtype_iter)
    {
      case ScalarType::ComplexFloat:
      {
        using vec_t = at::native::ztype<float>::vec_t;
        run_kernel<vec_t>(kernel, hw_result, a);
        match &= allclose(kernel, sw_result, hw_result);
        break;
      }
      default:
      {
        match &= false;
        break;
      }
    }
  }
  return match;
}

}}}  // namespace at::native::<anonymous>


int main() {
  std::string bitname = "/home/dylan_bespalko/repos/vitis_kernels/aten/src/ATen/fpga/binary_container_1.xclbin";
  fpgaSetProgramFilename(bitname);
  fpgaSetDevice(0);
  bool result = at::native::test_vfft() ? EXIT_SUCCESS : EXIT_FAILURE;
  fpgaReleaseProgram();
  fpgaReleaseDevice();
  return result;
}

