# pytorch-vitis-strided-complex

PyTorch extension that adds support for:

* device: fpga
* layout: strided
* dtype: floating-point

## Vitis Requirements

1. Xilinx Vitis High-Level Synthesis (HLS) C++ Design
2. Xilinx Vivado FPGA Design Suite
2. Xilinx XRT Runtime Libraries
3. C++ std::17

Full Instructions are found on the [Xilinx Vitis Website](https://www.xilinx.com/support/documentation/sw_manuals/xilinx2019_2/ug1393-vitis-application-acceleration.pdf)

## Pytorch Requirements

<table style="width:100%">
  <tr>
    <th>Sub-Project</th>
    <th>PyTorch</th>
    <th>LibTorch</th> 
  </tr>
  <tr>
    <th>dev</th>
    <td>No</td> 
    <td>Yes</td> 
  </tr>
  <tr>
    <th>aten</th>
    <td>Yes</td> 
    <td>No</td> 
  </tr>
  <tr>
    <th>test</th>
    <td>Yes</td> 
    <td>No</td> 
  </tr>
</table>

## Supported Platforms

<table style="width:100%">
  <tr>
    <th>Vendor</th>
    <th>Product Number</th> 
    <th>${XCL_PLATFORM}</th> 
  </tr>
  <tr>
    <td>Xilinx</td>
    <td>Alveo U200 Data Center Accelerator Card</td> 
    <td>xilinx_u200_xdma_201830_2</td>
  </tr>
</table>

## Build-Flow: 

### C++ Build Flow:

```bash
rm -rf build ; mkidir -p build ; cd build             # Clean Project
cmake ..  -> cppproject.toml                          # Configure Project
make -j 8                                             # Build Project
make install                                          # Install Project
```

### Python Build Flow:

```bash
python setup.py clean                                 # Clean Project
python setup.py install -> pyproject.toml             # Configure/Build/Install Project
```

You can diff build flows: `diff pyproject.toml cppproject.toml`

Since most people install `pip install pytorch` in Python instead of `apt install libtorch` in C++, you will likely need to subtitute the following commands where needed:

  - `Torch_DIR=/usr/lib/python3.11/site-packages/torch/share/cmake/Torch PYTORCH_DIR=/usr/lib/python3.11/site-packages/torch python setup.py install`
  - `Torch_DIR=/usr/lib/python3.11/site-packages/torch/share/cmake/Torch cmake ..`

## Environment Variables:

```bash
#source /tools/Xilinx/Vitis_HLS/2023.2/settings64.sh  # XILINX_VITIS, XILINX_VIVADO, XILINX_HLS
#source /opt/xilinx/xrt/setup.sh # XILINX_XRT
export Torch_DIR=/usr/lib/python3.11/site-packages/torch/share/cmake/Torch \                       # Location of the Meta `TorchConfig.cmake` loaded by the CMake `find_package(Torch REQUIRED COMPONENTS Torch)` command
export PYTORCH_DIR=$(STAGING_DIR)/usr/lib/python3.11/site-packages/torch \                         # Location of the Meta PyTorch module loaded by Python `import torch` command
export XILINX_XRT=/opt/xilinx/xrt \                                                                # Location of the Xilinx XRT directory
export XILINX_VIVADO=/tools/Xilinx/Vivado/${XILINX_VERSION} \                                      # Location of the Xilinx Vivado direcotry
export XILINX_HLS=/tools/Xilinx/Vitis_HLS/$(PYTHON_PYTORCH_VITIS_STRIDED_COMPLEX_XILINX_VERSION) \ # Location of the Xilinx HLS directory
export XILINX_VITIS=/tools/Xilinx/Vitis/$(PYTHON_PYTORCH_VITIS_STRIDED_COMPLEX_XILINX_VERSION) \   # Location of the Xilinx Vitis directory
export XCL_PLATFORM="xilinx_u200_gen3x16_xdma_2_202110_1" \                                        # Xilinx Platform ID
export XCL_OPT_INDEX=4 \                                                                           # Optimization level of design (1: buffered 2: dataflow 3: distributed 4: vec)
export XCL_CU=1 \                                                                                  # Number of compute units (> 0)
export XCL_EMULATION_MODE="sw_emu" \                                                               # Xilinx Emulation Mode (sw_emu | hw_emu | hw) 
export XCL_KERNEL_NAME="vvc4fft"                                                                   # Manually Specify Which Math Kernel to build (Only used in the /dev folder for prototyping)
```

The XCL_EMULATION_MODE environment variable seems to be required by a downstream Xilinx dependency.

## IDE Support (VSCode)

Try Adding the following include paths to help navigate the code:

```bash
${default}
${workspaceFolder}
${workspaceFolder}/aten/src
/home/dylan_bespalko/repos/pytorch/torch/include
/home/dylan_bespalko/repos/pytorch/torch/include/torch/csrc/api/include
```

## Configure

This extension allows you to:

    - Compile an infinite number of kernels (.xo files).
    - Link a finite number of kernels into a binary (.xclbin files).

OpenCL is used to create a client-server relationship between the CPU (PS) and FPGA (PL).
This means you only need to compile the kernels that you need on the FPGA (PL). Before
you run the build, configure the desired kernels in CMakeLists.txt.

### Add dtype

```cmake
#         Name              CHAR    SIZE_BYTES  T           PAR_LOG_SIZE    RN_LOG_SIZE     ENABLED
add_dtype(Byte              u       1           byte        6               0               0)  # 0
add_dtype(Char              i       1           char        6               0               0)  # 1
add_dtype(Short             i       2           short       5               0               0)  # 2
add_dtype(Int               i       4           int         4               0               0)  # 3
add_dtype(Long              i       8           long        3               0               0)  # 4
add_dtype(Half              f       2           half        5               0               0)  # 5
add_dtype(Float             f       4           float       4               0               1)  # 6
add_dtype(Double            f       8           double      3               0               0)  # 7
add_dtype(ComplexHalf       c       2           half        5               1               0)  # 8
add_dtype(ComplexFloat      c       4           float       4               1               1)  # 9
add_dtype(ComplexDouble     c       8           double      3               1               0)  # 10
add_dtype(Bool              b       1           bool        6               0               0)  # 11
```

<table style="width:100%">
  <tr>
    <th>Name</th>
    <td>Torch ScalarType ID</td>  
  </tr>
  <tr>
    <th>CHAR</th>
    <td>Torch dtype character ID</td>  
  </tr>
  <tr>
    <th>SIZE_BYTES</th>
    <td>Number of bytes in scalar dtype</td>  
  </tr>
  <tr>
    <th>T</th>
    <td>C++ built-in data type</td>  
  </tr>
  <tr>
    <th>PAR_LOG_SIZE</th>
    <td>log2(number of elements in vector dtype)</td>  
  </tr>
  <tr>
    <th>RN_LOG_SIZE</th>
    <td>log2(R<sup>N</sup>)</td>  
  </tr>
  <tr>
    <th>ENABLED</th>
    <th>Enable compilation of dtype.</th> 
  </tr>
</table>

Example:
```cmake
add_dtype(Double            f       8           double      3               0               0)
 ```
Will compile kernels with a vf8_ prefix with a 8bit * 8byte 2^3 elements = 512bit width.

```cmake
add_dtype(ComplexFloat      c       4           float       4               1               1)
 ```
Will compile kernels with a vc4_ prefix with a 8bit * 4byte 2^4 elements = 512bit width.

### Register Kernel

<table style="width:100%">
  <tr>
    <th>Dict</th>
    <td>The CMake Dictionary the stores Registered Kerenels</td>  
  </tr>
  <tr>
    <th>KERNEL</th>
    <td>Name of the Kernel </td>  
  </tr>
  <tr>
    <th>CATEGORY</th>
    <td>Category of Kernels that have a similar algorithm</td>  
  </tr>
  <tr>
    <th>USE_TEMPLATE</th>
    <td>Use a predefined template in aten/src/ATen/fpga/kernel/&lt;CATEGORY&gt;/template</td>  
  </tr>
</table>

Example:
```cmake
add_to_dict(kernel "abs" "UnaryComplexKernel" 1)  # Registerd
# add_to_dict(kernel "abs" "UnaryComplexKernel" 1)  # UnRegisterd
```

### Add Kernel

Add Kernels(.xo files) defined in ${XCL_PLATFORM}.cfg:
```cfg
#nk=v<CHAR><SIZE_BITES><KERNEL>:${XCL_CU}
nk=vf4add:1  # Enabled
#nk=vf4add:1  # Disabled
```

# Install

``` bash
git clone git@gitlab.com:pytorch-complex/vitis_kernels.git
cd vitis_kernels
git submodule sync
git submodule update --init --recursive --remote
rm -rf _skbuild/
python setup.py install
```

## Test

Because you cannot link all of the possible kernels into a signal binary,
unit-tests are organized by category and can be run using pytest.
``` bash
cd test
pytest --capture=sys test_torch.py::TestTensor::test_${Category_Name}
```

## Internals

- /dev: Kernel developement sandbox using the Vitis optimization methodology.
- /aten/src/ATen: Kernel deployment using the Pytorch sw architecture.
    - aten/src/ATen/opencl: C++ host code running on CPU.
    - aten/src/ATen/fpga: C++ HLS code running on FPGA.
- /test: Python test_scripts.

## Embedded Build-Flow

Refer to [https://gitlab.com/scikit-nonlinear/buildroot-external-sknrf](buildroot-external-sknrf) for embedded build-flow instructions.

After booting the device:

```bash
 dfx-mgrd &  # Launch the DFX server
 dfx-mgr-client -listPackage  # List acceleration apps
 dfx-mgr-client -load torch_fpga_strided_complex  # Currently failing to program FPGA
```

## License

Torch-Vitis is BSD-style licensed, as found in the [LICENSE](LICENSE) file.

## Bibtex

Much appreciated

``` bash
@article{bespalko2020pytorch,
  title={Torch Vitis},
  author={Bespalko, DT},
  journal={GitLab. Note: https://gitlab.com/pytorch-complex/vitis_kernels},
  volume={1},
  year={2020}
}
```
