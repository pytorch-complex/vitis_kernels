#include <torch/extension.h>

#include <c10/fpga/FPGAFunctions.h>

PYBIND11_MODULE(TORCH_EXTENSION_NAME, m) {
  // FUNCTIONS
  m.def("set_program", &fpgaSetProgramFilename, "Set an FPGA Program", py::arg("xclbin_filename") = "");
  m.def("release_device", &fpgaReleaseDevice, "Release an FPGA Device");
  m.def("set_device", &fpgaSetDevice, "Set an FPGA Device", py::arg("index") = "");
}
