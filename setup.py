# Build-Flow: 
#    - C++ Build Flow:
#      ```bash
#      rm -rf build ; mkidir -p build ; cd build             # Clean Project
#      cmake ..  -> cppproject.toml                          # Configure Project
#      make -j 8                                             # Build Project
#      make install                                          # Install Project
#      ```
#    - Python Build Flow:
#      ```bash
#      python setup.py clean                                 # Clean Project
#      python setup.py install -> pyproject.toml             # Configure/Build/Install Project
#      ```
#    - Diff build flows: `diff pyproject.toml cppproject.toml`
# Since most people install `pip install pytorch` in Python instead of `apt install libtorch` in C++, you will likely need to subtitute the following commands where needed:
#    - `Torch_DIR=/usr/lib/python3.11/site-packages/torch/share/cmake/Torch PYTORCH_DIR=/usr/lib/python3.11/site-packages/torch python setup.py install`
#    - `Torch_DIR=/usr/lib/python3.11/site-packages/torch/share/cmake/Torch cmake ..`
# Environment Variables:
#    ```bash
#    export Torch_DIR=/usr/lib/python3.11/site-packages/torch/share/cmake/Torch \                       # Location of the Meta `TorchConfig.cmake` loaded by the CMake `find_package(Torch REQUIRED COMPONENTS Torch)` command
#    export PYTORCH_DIR=$(STAGING_DIR)/usr/lib/python3.11/site-packages/torch \                         # Location of the Meta PyTorch module loaded by Python `import torch` command
#    export XILINX_XRT=/opt/xilinx/xrt \                                                                # Location of the Xilinx XRT directory
#    export XILINX_VIVADO=/tools/Xilinx/Vivado/${XILINX_VERSION} \                                      # Location of the Xilinx Vivado direcotry
#    export XILINX_HLS=/tools/Xilinx/Vitis_HLS/$(PYTHON_PYTORCH_VITIS_STRIDED_COMPLEX_XILINX_VERSION) \ # Location of the Xilinx HLS directory
#    export XILINX_VITIS=/tools/Xilinx/Vitis/$(PYTHON_PYTORCH_VITIS_STRIDED_COMPLEX_XILINX_VERSION) \   # Location of the Xilinx Vitis directory
#    export XCL_PLATFORM="xilinx_u200_gen3x16_xdma_2_202110_1" \                                        # Xilinx Platform ID
#    export XCL_OPT_INDEX=4 \                                                                           # Optimization level of design (1: buffered 2: dataflow 3: distributed 4: vec)
#    export XCL_CU=1 \                                                                                  # Number of compute units (> 0)
#    export XCL_EMULATION_MODE="sw_emu" \                                                               # Xilinx Emulation Mode (sw_emu | hw_emu | hw) 
#    export XCL_KERNEL_NAME="vvc4fft"                                                                   # Manually Specify Which Math Kernel to build (Only used in the /dev folder for prototyping) 

import sys
import os
from pathlib import Path
import argparse

import toml
from skbuild import setup
from aten.src.ATen.gen import generate_device, generated_code_template_py
from setuptools import Extension
from aten.utils.cpp_extension import BuildExtension, include_paths  # copy of torch.utils needed for cross-compiler support

## Parse input argurments: `python setup.py ARGS... install` is allowed, but is identical to `ENV_VARS=.... python setup.py install`
parser = argparse.ArgumentParser(description="torch-vitis-strided-complex setup.py config:")
parser.add_argument('-p', '--PYTORCH_DIR', type=str, default=os.environ['PYTORCH_DIR'], help="The location of the PyTorch Module")
parser.add_argument('-r', '--XILINX_XRT', type=str, default=os.environ['XILINX_XRT'], help="The location of Xilinx XRT")
parser.add_argument('-d', '--XILINX_VIVADO', type=str, default=os.environ['XILINX_VIVADO'], help="The location of Xilinx Vivado")
parser.add_argument('-i', '--XILINX_VITIS', type=str, default=os.environ['XILINX_VITIS'], help="The location of Xilinx Vitis")
parser.add_argument('-l', '--XILINX_HLS', type=str, default=os.environ['XILINX_HLS'], help="The location of Xilinx HLS")
args, unknown = parser.parse_known_args()
sys.argv = [sys.argv[0]] + unknown
PYTORCH_DIR, XILINX_XRT, XILINX_VIVADO, XILINX_VITIS, XILINX_HLS = vars(args).values()
ROOT_DIR = Path(__file__).resolve().parent
PYTORCH_DIR = Path(PYTORCH_DIR)
PYTORCH_EXT_DIR = ROOT_DIR
PYTORCH_LIB_DIR = PYTORCH_DIR / 'lib'
XILINX_XRT = Path(XILINX_XRT)
XILINX_VIVADO = Path(XILINX_VIVADO)
XILINX_VITIS = Path(XILINX_VITIS)
XILINX_HLS = Path(XILINX_HLS)

## Parse math kernel schemas: aten/src/ATen/host/fpga_functions.yaml -> aten/src/ATen/{FPGAFunctions.h|FPGAFunctions.cpp|ops/}
generate_device()

## Format and write C++ extension config: setup.py -> pyproject.toml
def format_toml(unformatted_toml):
    formatted_toml = ''
    for line in unformatted_toml.splitlines():
        if '[' in line and ',' in line and ']' in line:
            line = line.replace('[ ', '[\n\t')
            line = line.replace(', ', ',\n\t')
            line = line.replace(']', '\n]')
        formatted_toml += f'{line:s}\n'
    return formatted_toml

python_toml = dict()
python_toml['build-system'] = {'requires': ['toml', 'setuptools', 'wheel', 'scikit-build']} # 'torch', 'cmake', 'ninja'
python_toml['build-settings'] = {
    'sources': [
        str(PYTORCH_EXT_DIR / 'torch_vitis.cpp'),
        str(PYTORCH_EXT_DIR / 'c10' / 'fpga' / 'impl' / 'FPGAGuardImpl.cpp'),
        str(PYTORCH_EXT_DIR / 'aten' / 'src' / 'ATen' / 'RegisterFPGA.cpp'),
        str(PYTORCH_EXT_DIR / 'aten' / 'src' / 'ATen' / 'FPGAEmptyTensor.cpp'),
        str(PYTORCH_EXT_DIR / 'aten' / 'src' / 'ATen' / 'host' / 'TensorFactories.cpp'),
        str(PYTORCH_EXT_DIR / 'aten' / 'src' / 'ATen' / 'host' / 'BinaryOps.cpp'),
        str(PYTORCH_EXT_DIR / 'aten' / 'src' / 'ATen' / 'host' / 'UnaryOps.cpp'),
        str(PYTORCH_EXT_DIR / 'aten' / 'src' / 'ATen' / 'host' / 'ReduceOps.cpp'),
        str(PYTORCH_EXT_DIR / 'aten' / 'src' / 'ATen' / 'host' / 'TensorCompare.cpp'),
        str(PYTORCH_EXT_DIR / 'aten' / 'src' / 'ATen' / 'host' / 'SpectralOps.cpp'),
     ],
    'include_dirs': [
        str(PYTORCH_EXT_DIR),
        str(PYTORCH_EXT_DIR / 'aten' / 'src'),
        str(PYTORCH_EXT_DIR / 'third_party' / 'Vitis_Libraries' / 'dsp' / 'L1' / 'include' / 'hw' / 'vitis_fft' / 'float'),
        str(PYTORCH_EXT_DIR / 'third_party' / 'Vitis_Libraries' / 'dsp' / 'L2' / 'include' / 'hw' / 'vitis_fft' / 'float'),
        str(PYTORCH_EXT_DIR / 'third_party' / 'Vitis_Libraries' / 'utils' / 'L1' / 'include'),
        str(PYTORCH_EXT_DIR / 'third_party' / 'Vitis_Libraries' / 'dsp' / 'L1' / 'include' / 'hw'),
        str(XILINX_XRT / 'include'),
        str(XILINX_VIVADO / 'include'),
        str(XILINX_HLS / 'include'),
    ] + include_paths(),
    'library_dirs': [],
    'libraries': [
        str(PYTORCH_LIB_DIR / 'torch_python'),
        str(PYTORCH_LIB_DIR / 'torch_cpu'),
        str('OpenCL'),
    ],
    'extra_compile_args': [
        '-std=c++17', 
        '-Wno-unknown-pragmas'
        ] if sys.platform == 'win32' else [
        '-std=c++17', 
        '-Wno-unknown-pragmas'
    ],
    'extra_link_args': [],
    'language': 'c++',
    'export_symbols': [],
    'runtime_library_dirs': [
        str(PYTORCH_LIB_DIR), 
        str(XILINX_XRT / 'lib'),
    ],
}
unformatted_toml = toml.dumps(python_toml)
formatted_toml = format_toml(unformatted_toml)
with open('pyproject.toml', 'w') as f:
    f.write(generated_code_template_py)
    f.write(formatted_toml)

# Define the C++ Extension to be build by scikit-build: pyproject.toml -> setup.py::setup()
ext_module = Extension(
    'torch_fpga_strided_complex.fpga',
    python_toml['build-settings']['sources'],
    include_dirs=python_toml['build-settings']['include_dirs'],
    library_dirs=python_toml['build-settings']['library_dirs'],
    libraries=python_toml['build-settings']['libraries'],
    extra_compile_args=python_toml['build-settings']['extra_compile_args'],
    extra_link_args=python_toml['build-settings']['extra_link_args'],
    runtime_library_dirs=python_toml['build-settings']['runtime_library_dirs'],
)

# Build and Install the C++ Extension: CMakeLists.txt, setup.py::setup() -> import torch_fpga_strided_complex
setup(name='torch_fpga_strided_complex',
      packages=['torch_fpga_strided_complex'],
      ext_modules=[ext_module],
      cmdclass={'build_ext': BuildExtension},
)

