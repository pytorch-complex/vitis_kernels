import re
from pathlib import Path
import argparse

ROOT_DIR = Path(__file__).resolve().parent

dtype_id_map = {
    'Byte': 'u1',
    'Char': 'i1',
    'Short': 'i2',
    'Int': 'i4',
    'Long': 'i8',
    'Half': 'f2',
    'Float': 'f4',
    'Double': 'f8',
    'ComplexHalf': 'c2',
    'ComplexFloat': 'c4',
    'ComplexDouble': 'c8',
}

# PyTorch does not support complex integer types, but Spectral Ops kernels uses integers
# See https://pytorch.org/docs/stable/tensor_attributes.html#torch-dtype 
spectral_dtype_id_map = {
    'Byte': 'u1',
    'Char': 'i1',
    'Short': 'i2',
    'Int': 'i4',
    'Long': 'i8',
    'Half': 'f2',
    'Float': 'f4',
    'Double': 'f8',
    'ComplexHalf': 'i2',
    'ComplexFloat': 'i4',
    'ComplexDouble': 'i8',
}

dtype_str_map = {
    'Byte': 'th.byte',
    'Char': 'th.char',
    'Short': 'th.int16',
    'Int': 'th.int32',
    'Long': 'th.int64',
    'Half': 'th.float16',
    'Float': 'th.float32',
    'Double': 'th.float64',
    'ComplexHalf': 'th.complex32',
    'ComplexFloat': 'th.complex64',
    'ComplexDouble': 'th.complex128',
}

category_top_map = {
    'BinaryArithmeticKernel': 'BinaryOps',
    'BinaryEqualsKernel': 'BinaryOps',
    'BinaryCompareKernel': 'BinaryOps',
    'UnaryTrigKernel': 'UnaryOps',
    'UnaryHypTrigKernel': 'UnaryOps',
    'UnaryArcTrigKernel': 'UnaryOps',
    'UnaryPowKernel': 'UnaryOps',
    'UnaryLogKernel': 'UnaryOps',
    'UnaryExpKernel': 'UnaryOps',
    'UnaryRoundKernel': 'UnaryOps',
    'UnaryComplexKernel': 'UnaryOps',
    'UnaryClampKernel': 'UnaryOps',
    'ReduceStatsOpsKernel': 'ReduceOps',
    'TensorCompareKernel' : 'TensorCompare',
    'SpectralOpsKernel': 'SpectralOps',
}

platform_cfg_filename_map = {
    'xilinx_u200_gen3x16_xdma_2_202110_1': 'xilinx_u200_gen3x16_xdma_2_202110_1.cfg',
    'xilinx_zcu102_base_202320_1': 'xilinx_zcu102_base_202320_1.cfg',
}


def write_cmakefile(config, cmake_filename):
    with open(cmake_filename, 'r') as file:
        data = file.read()

    # Disable all dtypes
    pattern = re.compile(r'^add_dtype\((.*)\s[0|1]\)')
    replace_str = r'add_dtype(\1 %d)' % (0,)
    data = re.sub(pattern, replace_str, data)

    # Enable dtype
    pattern = re.compile(r'^add_dtype\((%s.*)\s[0|1]\)' % (config.dtype,))
    replace_str = r'add_dtype(\1 %d)' % (1,)
    data = re.sub(pattern, replace_str, data)

    # Find all kernels in category
    pattern = re.compile(r'^add_to_dict\(kernel\s+\"(\w+)\"\s+\"%s"' % (config.category,), re.MULTILINE)
    kernels = re.findall(pattern, data)

    with open(cmake_filename, 'w') as file:
        file.write(data)
    return kernels


def write_xclbin(config, config_filename, kernels):
    with open(config_filename, 'w') as file:
        file.write('[connectivity]\n')
        file.write(f'# {config.category:s}\n')
        if config.category == 'SpectralOpsKernel':
            dtype = spectral_dtype_id_map[config.dtype]
        else:
            dtype = dtype_id_map[config.dtype]
        for kernel in kernels:
            file.write("nk=v%s%s:%d\n" % (dtype, kernel, config.cu))  # nk=
            for cu in range(config.cu):
                pass


def write_test(config, test_filename):
    with open(test_filename, 'r') as file:
        data = file.read()

    # Enable category
    pattern = re.compile(r'(dtypes = \(.*\))')
    replace_str = f'dtypes = ({dtype_str_map[config.dtype]:s},)'
    data = re.sub(pattern, replace_str, data)

    with open(test_filename, 'w') as file:
        file.write(data)


def main():
    parser = argparse.ArgumentParser(description='vitis-kernels configure script')
    
    # Add arguments
    parser.add_argument('-c', '--category', required=False, type=str, choices=list(category_top_map.keys()),          default='BinaryArithmeticKernel',              help='Specify XCL_CATEGORY')
    parser.add_argument('-d', '--dtype',    required=False, type=str, choices=list(dtype_id_map.keys()),              default='Float',                               help='Specify XCL_DTYPE')
    parser.add_argument('-p', '--platform', required=False, type=str, choices=list(platform_cfg_filename_map.keys()), default='xilinx_u200_gen3x16_xdma_2_202110_1', help='Specify XCL_PLATFORM')
    parser.add_argument('-m', '--mode',     required=False, type=str, choices=['sw_emu', 'hw_emu', 'hw'],             default='sw_emu',                              help='Specify XCL_EMULATION_MODE')
    parser.add_argument('-o', '--opt',      required=False, type=int, choices=[1, 2, 3, 4],                           default=4,                                     help='Specify XCL_OPT_INDEX')
    parser.add_argument('-u', '--cu',       required=False, type=int,                                                 default=1,                                     help='Specify XCL_CU')
    
    # Parse the arguments
    config = parser.parse_args()    
    print('Selected Configuration:')
    print(f'  - category: {config.category:s}')
    assert config.category in category_top_map, f'Error: --category: {config.categorya:s} could not be found'
    print(f'  - dtype: {config.dtype:s}')
    assert config.dtype in dtype_id_map, f'Error: --dtype: {config.dtype:s} could not be found'
    print(f'  - platform: {config.platform:s}')
    assert config.platform in platform_cfg_filename_map, f'Error: --platform: {config.platform:s} could not be found'
    assert Path(platform_cfg_filename_map[config.platform]).is_file(), f'Error: --platform filename: {platform_cfg_filename_map[config.platform]:s} could not be found'
    print(f'  - mode: {config.mode:s}')
    print(f'  - opt: {config.opt:d}')
    assert config.opt >= 1, f'Error: --opt: {config.opt:d} < 1'
    assert config.opt <= 4, f'Error: --opt: {config.opt:d} > 4'
    print(f'  - cu: {config.cu:d}')
    assert config.cu >= 1, f'Error: --opt: {config.cu:d} < 1'

    # Write the files
    print('Impacted Files:')
    cmakelist_filename = 'CMakeLists.txt'
    print(f'  - CMakeLists.txt: {cmakelist_filename:s}')
    assert Path(cmakelist_filename).is_file(), f'Error: cmakelist_filename: {cmakelist_filename:s} could not be found'
    xclbin_cfg_filename = platform_cfg_filename_map[config.platform]
    print(f'  - xclbin.cfg: {xclbin_cfg_filename:s}')
    assert Path(xclbin_cfg_filename).is_file(), f'Error: xclbin.cfg: {xclbin_cfg_filename:s} could not be found'
    test_filename = 'test/test_torch.py'
    print(f'  - test.py: {test_filename:s}')
    assert Path(test_filename).is_file(), f'Error: test.py: {test_filename:s} could not be found'
    kernels = write_cmakefile(config, cmakelist_filename)
    write_xclbin(config, xclbin_cfg_filename, kernels)
    write_test(config, test_filename)


if __name__ == "__main__":
    main()
