#!/usr/bin/python3
import os
import sys
import re
import yaml

from lark import Lark
from lark.exceptions import UnexpectedCharacters, UnexpectedToken, VisitError
import torch

sys.path = [".."] + sys.path
from aten.src.ATen.gen import install_dir, TreeToCXX


def test_arg_pair():
    with open(str(install_dir / "gen.lark"), "r") as g:
        arg_parser = Lark(g, start="arg_pair", lexer='auto', parser='lalr')
        test_vectors = [
            ['Tensor var', "('const at::Tensor &', -1, 'var')"],
            ['Tensor(a) var', "('const at::Tensor &', 0, 'var')"],
            ['Tensor(a!) var', "('at::Tensor &', 0, 'var')"],
            ['Tensor[] var', "('TensorList', -1, 'var')"],
            ['int var', "('int64_t', -1, 'var')"],
            ['int[66] var', "('IntArrayRef', -1, 'var')"],
            ['int[] var', "('IntArrayRef', -1, 'var')"],
            ['bool var', "('bool', -1, 'var')"],
            ['float var', "('double', -1, 'var')"],
            ['str var', "('std::string', -1, 'var')"],
            ['Scalar var', "('const at::Scalar &', -1, 'var')"],
            ['TensorOptions var', "('TensorOptions', -1, 'var')"],
        ]
        for i, (text, expected_cxx) in enumerate(test_vectors):
            args = arg_parser.parse(text)
            # print(args.pretty().strip())
            actual_cxx = str(TreeToCXX(False).transform(args))
            if actual_cxx != expected_cxx:
                raise SyntaxWarning("\nEXPECTED - %d: %s\nACTUAL --- %d: %s"
                                    % (i + 1, expected_cxx, i + 1, actual_cxx))


def test_func():
    sig_pattern = re.compile(r"(.+)\((.*)\)")
    with open(str(install_dir / "gen.lark"), "r") as g:
        func_parser = Lark(g, start="func_name", lexer='auto', parser='lalr')
        test_vectors = [
            ['add(Tensor self, Tensor other, Scalar alpha)', 'add(const at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha)'],
            ['add_.Tensor(Tensor self, Tensor other, Scalar alpha)', 'add_(const at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha)'],
            ['add.out(Tensor self, Tensor other, Scalar alpha)', 'add_out(const at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha)'],
        ]
        for i, (text, expected_cxx) in enumerate(test_vectors):
            m = sig_pattern.fullmatch(text)
            func = func_parser.parse(m[1])
            expected_cxx = expected_cxx[0:expected_cxx.index("(")]
            actual_cxx = TreeToCXX(False).transform(func)
            # print(func.pretty().strip())
            if actual_cxx != expected_cxx:
                raise SyntaxWarning("\nEXPECTED - %d: %s\nACTUAL --- %d: %s"
                                    % (i + 1, expected_cxx, i + 1, actual_cxx))


def test_all():
    with open(str(install_dir / "gen.lark"), "r") as g:
        parser = Lark(g, start="func", lexer='auto', parser='lalr')
        test_vectors = [
            ['add.Tensor(Tensor self, Tensor other, *, Scalar alpha=1) -> Tensor',
             'TORCH_API at::Tensor add(const at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha);'],
            ['add.Scalar(Tensor self, Scalar other, Scalar alpha=1) -> Tensor',
             'TORCH_API at::Tensor add(const at::Tensor & self, const at::Scalar & other, const at::Scalar & alpha);'],
            ['add_.Tensor(Tensor(a!) self, Tensor other, *, Scalar alpha=1) -> Tensor(a!)',
             'TORCH_API at::Tensor & add_(at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha);'],
            ['add.out(Tensor self, Tensor other, *, Scalar alpha=1, Tensor(a!) out) -> Tensor(a!)',
             'TORCH_API at::Tensor & add_out(const at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha, at::Tensor & out);'],
            ['_amp_non_finite_check_and_unscale_(Tensor(a!) self, Tensor(b!) found_inf, Tensor inv_scale) -> ()',
             'TORCH_API void _amp_non_finite_check_and_unscale_(at::Tensor & self, at::Tensor & found_inf, const at::Tensor & inv_scale);'],
            ['argmax(Tensor self, int? dim=None, bool keepdim=False) -> Tensor',
             'TORCH_API at::Tensor argmax(const at::Tensor & self, ::std::optional<int64_t> dim, bool keepdim);'],
            ['block_diag(Tensor[] tensors) -> Tensor',
             'TORCH_API at::Tensor block_diag(TensorList tensors);'],
            ['adaptive_avg_pool1d(Tensor self, int[1] output_size) -> Tensor',
             'TORCH_API at::Tensor adaptive_avg_pool1d(const at::Tensor & self, IntArrayRef output_size);'],
            ["index.Tensor(Tensor self, Tensor?[] indices) -> Tensor",
             "TORCH_API at::Tensor index(const at::Tensor & self, TensorList indices);"],
            ["abs(Tensor self) -> Tensor",
             "TORCH_API at::Tensor abs(const at::Tensor & self);"],
            ["cudnn_grid_sampler(Tensor self, Tensor grid) -> Tensor output",
             "TORCH_API at::Tensor cudnn_grid_sampler(const at::Tensor & self, const at::Tensor & grid);"],
            ["rot90(Tensor self, int k=1, int[] dims=[0,1]) -> Tensor",
             "TORCH_API at::Tensor rot90(const at::Tensor & self, int64_t k, IntArrayRef dims);"],
        ]
        for i, (text, expected_cxx) in enumerate(test_vectors):
            func = parser.parse(text)
            # print(func.pretty().strip())
            actual_cxx = TreeToCXX(False).transform(func)
            if actual_cxx != expected_cxx:
                raise SyntaxWarning("\nEXPECTED - %d: %s\nACTUAL --- %d: %s"
                                    % (i + 1, expected_cxx, i + 1, actual_cxx))


def test_registration_declarations():
    torch_dir = os.path.dirname(os.path.abspath(torch.__file__))
    filename = os.sep.join((torch_dir, "csrc", "autograd", "generated", "RegistrationDeclarations.h"))
    with open(str(install_dir / "gen.lark"), "r") as g:
        parser = Lark(g, start="func", lexer='auto', parser='lalr')
        with open(filename, 'r') as f:
            for i, line in enumerate(f):
                if len(line) == 0:
                    pass
                elif line.startswith("//"):
                    pass
                elif line.startswith("\n"):
                    pass
                else:
                    expected_cxx, schema_yaml = line.split("//")
                    expected_cxx = expected_cxx.strip()
                    schema_map = yaml.load(schema_yaml, Loader=yaml.FullLoader)
                    schema = schema_map["schema"].strip()[6:]
                    try:
                        func = parser.parse(schema)
                    except (UnexpectedCharacters, UnexpectedToken):
                        print("PARSE ERROR - %d: %s" % (i + 1, schema,))
                        continue
                    try:
                        actual_cxx = TreeToCXX(False).transform(func)
                        try:
                            if expected_cxx != actual_cxx:
                                raise SyntaxWarning("\nEXPECTED - %d: %s\nACTUAL --- %d: %s"
                                                    % (i + 1, expected_cxx, i + 1, actual_cxx))
                        except SyntaxWarning as w:
                            print(w)
                    except VisitError:
                        print("CXX ERROR - %d: %s" % (i + 1, schema,))
                        continue


def test_native_functions():
    torch_dir = os.path.dirname(os.path.abspath(torch.__file__))
    filename = os.sep.join((torch_dir, "..", "aten", "src",  "ATen", "native", "native_functions.yaml"))
    with open(str(install_dir / "gen.lark"), "r") as g:
        parser = Lark(g, start="func", lexer='auto', parser='lalr')
        with open(filename, 'r') as f:
            schema_list = yaml.load(f, Loader=yaml.BaseLoader)
            for i, schema_map in enumerate(schema_list):
                schema = schema_map["func"]
                try:
                    func = parser.parse(schema)
                except (UnexpectedCharacters, UnexpectedToken):
                    print("PARSE ERROR - %d: %s" % (i + 1, schema,))
                    continue
                try:
                    TreeToCXX(False).transform(func)
                except VisitError:
                    print("CXX ERROR - %d: %s" % (i + 1, schema,))
                    continue


def test_fpga_functions():
    filename = os.sep.join((install_dir, "host", "fpga_functions.yaml"))
    with open(str(install_dir / "gen.lark"), "r") as g:
        parser = Lark(g, start="func", lexer='auto', parser='lalr')
        with open(filename, 'r') as f:
            schema_list = yaml.load(f, Loader=yaml.BaseLoader)
            for i, schema_map in enumerate(schema_list):
                schema = schema_map["func"]
                try:
                    func = parser.parse(schema)
                except (UnexpectedCharacters, UnexpectedToken):
                    print("PARSE ERROR - %d: %s" % (i + 1, schema,))
                    continue
                try:
                    TreeToCXX(True).transform(func)
                except VisitError:
                    print("CXX ERROR - %d: %s" % (i + 1, schema,))
                    continue


if __name__ == '__main__':
    # The following tests attempt to parse the partial PyTorch API that I am using.
    test_arg_pair()
    test_func()
    test_all()

    # The following tests attempt to parse the entire PyTorch API
    # test_registration_declarations()
    # test_native_functions()
    # test_fpga_functions()
