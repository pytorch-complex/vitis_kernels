import unittest
import time

import torch as th
from torch.testing import assert_close
import numpy as np
import scipy as sp
from scipy import special as sp_sp
from scipy import linalg as sp_linalg

from torch_fpga_strided_complex import fpga, program_filename

NUMEL = 4096

dtype_map = {th.float32: np.float32,
             th.float64: np.float64,
             th.complex64: np.complex64,
             th.complex128: np.complex128}

c2r_dtype = {th.float32: th.float32,
             th.float64: th.float64,
             th.complex64: th.float32,
             th.complex128: th.float64}
t2n_dtype = {th.float32: np.float32,
             th.float64: np.float64,
             th.complex64: np.complex64,
             th.complex128: np.complex128}

devices = (th.device('fpga'),)
dtypes = (th.float32,)


def t2n(t):
    return t.detach().cpu().numpy()


class TestTensor(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        fpga.set_program(program_filename)
        fpga.set_device(0)

    def test_load(self):
        self.assertTrue(True)

    def test_empty(self):
        for device in devices:
            for dtype in (th.float32,):
                th_kwargs = {"dtype": dtype, "device": device}
                a = th.empty((NUMEL,), **th_kwargs)
                assert a.dtype == dtype, 'Incorrect Tensor Dtype'
                assert str(a.device) == 'cpu', 'Incorrect Tensor Device'
                assert tuple(a.shape) == (NUMEL,), 'Incorrect Tensor Shape'

    def test_to_cpu(self):
        for device in devices:
            for dtype in (th.float32,):
                th_kwargs = {"dtype": dtype, "device": 'cpu'}
                a = th.empty((NUMEL,), **th_kwargs).to('fpga')
                # assert len(str(a.to('cpu'))) != 0, 'Requires at::fpga::cat.out (Not Implemented)'
                assert len(str(a.to('cpu') + 0)) != 0, 'cat.out Workaround not working'
                assert str(a.to('cpu').device) == "cpu", 'Tensor cannot be copied to CPU'

    def test_copy(self):
        for device in devices:
            for dtype in (th.float32,):
                th_kwargs = {"dtype": dtype, "device": device}
                a = th.empty((2, 2), **th_kwargs)
                b = th.empty((2, 2), **th_kwargs)
                a.copy_(b)

    def test_FillKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                assert_allclose(t2n(th.full((4096,), -2.0, **th_kwargs)), -2.0*np.ones((4096,), t2n_dtype[dtype]), **tol_kwargs)
                assert_allclose(t2n(th.ones((4096,), **th_kwargs)), np.ones((4096,), t2n_dtype[dtype]), **tol_kwargs)

    def test_scalar_ops(self):
        for device in devices:
            for dtype in (th.float32,):  # th.complex64 not working
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(dtype).eps, "atol": th.finfo(dtype).eps}
                a = th.ones((2, 2), **th_kwargs)
                # a = th.tensor([[1+1j, 1-1j], [2.51-2.51j, 2.51+2.51j]], **th_kwargs)

                # assert_allclose(t2n(a + 3.51), t2n(a) + 3.51, **tol_kwargs)
                # assert_allclose(t2n(a - (3.51 + 3.51j)), t2n(a) - (3.51 + 3.51j), **tol_kwargs)
                # assert_allclose(t2n(a * (3.51 + 3.51j)), t2n(a) * (3.51 + 3.51j), **tol_kwargs)
                # assert_allclose(t2n(a / (3.51 + 3.51j)), t2n(a) / (3.51 + 3.51j), **tol_kwargs)
                #
                # assert_allclose(t2n((3.51 + 3.51j) + a), (3.51 + 3.51j) + t2n(a), **tol_kwargs)
                # assert_allclose(t2n((3.51 + 3.51j) - a), (3.51 + 3.51j) - t2n(a), **tol_kwargs)
                # assert_allclose(t2n((3.51 + 3.51j) * a), (3.51 + 3.51j) * t2n(a), **tol_kwargs)
                # assert_allclose(t2n((3.51 + 3.51j) / a), (3.51 + 3.51j) / t2n(a), **tol_kwargs)
   
    def test_BinaryArithmeticKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu"}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.rand((NUMEL,), **th_kwargs)
                a_fpga = a_cpu.to(device)
                assert_close(a_fpga + a_fpga, a_cpu + a_cpu, **tol_kwargs)
                assert_close(a_fpga - a_fpga, a_cpu - a_cpu, **tol_kwargs)
                assert_close(a_fpga * a_fpga, a_cpu * a_cpu, **tol_kwargs)
                assert_close(a_fpga / a_fpga, a_cpu / a_cpu, **tol_kwargs)
                # assert_allclose(t2n(a**a), t2n(a)**t2n(a), **tol_kwargs)

    def test_BinaryEqualsKernel(self):
        for device in devices:
            for dtype in dtypes:
                if dtype not in (th.complex64, th.complex128):
                    th_kwargs = {"dtype": dtype, "device": "cpu"}
                    tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                    a_cpu = b_cpu = th.rand((NUMEL,), **th_kwargs)
                    a_fpga, b_fpga = a_cpu.to(device), b_cpu.to(device)
                    assert_close(a_fpga == b_fpga, a_cpu == b_cpu, **tol_kwargs)
                    assert_close(a_fpga != b_fpga, a_cpu != b_cpu, **tol_kwargs)

    def test_BinaryCompareKernel(self):
        for device in devices:
            for dtype in dtypes:
                if dtype not in (th.complex64, th.complex128):
                    th_kwargs = {"dtype": dtype, "device": "cpu"}
                    tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                    a_cpu = b_cpu = th.rand((NUMEL,), **th_kwargs)
                    a_fpga, b_fpga = a_cpu.to(device), b_cpu.to(device)
                    assert_close(a_fpga >= a_fpga, a_cpu >= a_cpu, **tol_kwargs)
                    assert_close(a_fpga > a_fpga, a_cpu > a_cpu, **tol_kwargs)
                    assert_close(a_fpga <= a_fpga, a_cpu <= a_cpu, **tol_kwargs)
                    assert_close(a_fpga < a_fpga, a_cpu < a_cpu, **tol_kwargs)

    def test_UnaryTrigKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu"}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.rand((NUMEL,), **th_kwargs)
                a_fpga = a_cpu.to(device)
                assert_close(th.sin(a_fpga), np.sin(a_cpu), **tol_kwargs)
                assert_close(th.cos(a_fpga), np.cos(a_cpu), **tol_kwargs)
                assert_close(th.tan(a_fpga), np.tan(a_cpu), **tol_kwargs)

    def test_UnaryTrigKernelAutograd(self):
        # - name: sin(Tensor self) -> Tensor
        #         self: grad * self.cos().conj()
        #         result: auto_element_wise
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu", "requires_grad": True}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.linspace(1, NUMEL+1, NUMEL, **th_kwargs)
                a_fpga = a_cpu.to(device)
                external_grad = th.ones_like(a_cpu)
                F = th.sin(a_fpga)
                F.backward(gradient=external_grad)
                assert a_cpu.grad is not None, 'a_cpu.grad is None'
                assert len(a_cpu.grad) == NUMEL, 'a_cpu.grad is not len(NUMEL)'
        # This runs but terminaates on tearDownClass: fpga.release_device()

    def test_UnaryHypTrigKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu"}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.rand((NUMEL,), **th_kwargs)
                a_fpga = a_cpu.to(device)
                assert_close(th.cosh(a_fpga), np.cosh(a_cpu), **tol_kwargs)
                assert_close(th.sinh(a_fpga), np.sinh(a_cpu), **tol_kwargs)
                assert_close(th.tanh(a_fpga), np.tanh(a_cpu), **tol_kwargs)

    def test_UnaryArcTrigKernel(self):
        for device in devices:
            for dtype in dtypes:
                if dtype not in (th.complex64, th.complex128):
                    th_kwargs = {"dtype": dtype, "device": "cpu"}
                    tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                    a_cpu = th.rand((NUMEL,), **th_kwargs)
                    a_fpga = a_cpu.to(device)
                    assert_close(th.acos(a_fpga), np.arccos(a_cpu), **tol_kwargs)
                    assert_close(th.asin(a_fpga), np.arcsin(a_cpu), **tol_kwargs)
                    assert_close(th.atan(a_fpga), np.arctan(a_cpu), **tol_kwargs)

    def test_UnaryPowKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu"}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.rand((NUMEL,), **th_kwargs)
                a_fpga = a_cpu.to(device)
                assert_close(th.reciprocal(a_fpga), 1/a_cpu, **tol_kwargs)
                assert_close(th.sqrt(a_fpga), np.sqrt(a_cpu), **tol_kwargs)
                assert_close(th.rsqrt(a_fpga), 1/np.sqrt(a_cpu), **tol_kwargs)
   
    def test_UnaryLogKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu"}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.rand((NUMEL,), **th_kwargs)
                a_fpga = a_cpu.to(device)
                assert_close(th.log(a_fpga), np.log(a_cpu), **tol_kwargs)
                assert_close(th.log2(a_fpga), np.log2(a_cpu), **tol_kwargs)
                assert_close(th.log10(a_fpga), np.log10(a_cpu), **tol_kwargs)
                assert_close(th.log1p(a_fpga), np.log1p(a_cpu), **tol_kwargs)

    def test_UnaryExpKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu"}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.rand((256,), **th_kwargs)
                a_fpga = a_cpu.to(device)
                assert_close(th.exp(a_fpga), np.exp(a_cpu), **tol_kwargs)
                assert_close(th.expm1(a_fpga), np.expm1(a_cpu), **tol_kwargs)
                #if dtype not in (th.complex64, th.complex128):
                    # assert_allclose(t2n(th.erf(a)), sp_sp.erf(t2n(a)), **tol_kwargs) # https://github.com/Xilinx/Vitis_Libraries/issues/20
                    # assert_allclose(t2n(th.erfc(a)), sp_sp.erfc(t2n(a)), **tol_kwargs) # https://github.com/Xilinx/Vitis_Libraries/issues/20

    def test_UnaryRoundKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu"}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.rand((256,), **th_kwargs)
                a_fpga = a_cpu.to(device)
                assert_close(th.round(a_fpga), np.round(a_cpu), **tol_kwargs)
                if dtype not in (th.complex64, th.complex128):
                    assert_close(th.ceil(a_fpga), np.ceil(a_cpu), **tol_kwargs)
                    assert_close(th.floor(a_fpga), np.floor(a_cpu), **tol_kwargs)
                    assert_close(th.trunc(a_fpga), np.trunc(a_cpu), **tol_kwargs)

    def test_UnaryComplexKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu"}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.rand((NUMEL,), **th_kwargs)
                a_fpga = a_cpu.to(device)
                assert_close(th.abs(a_fpga), np.abs(a_cpu), **tol_kwargs)
                assert_close(t2n(th.angle(a_fpga)), np.angle(t2n(a_cpu)), **tol_kwargs)
                assert_close(t2n(th.conj_physical(a_fpga)), np.conj(t2n(a_cpu)), **tol_kwargs)
                # assert_close(th.copy_real(a), np.real(a), **tol_kwargs)
                # assert_close(th.copy_imag(a), 1j*np.imag(a), **tol_kwargs)

    def test_UnaryClampKernel(self):
        for device in devices:
            for dtype in dtypes:
                if dtype not in (th.complex64, th.complex128):
                    th_kwargs = {"dtype": dtype, "device": "cpu"}
                    tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                    a_cpu = th.rand((NUMEL,), **th_kwargs)
                    a_fpga = a_cpu.to(device)
                    assert_close(th.clamp(a_fpga, min=0.50, max=0.75), np.clip(a_cpu, a_min=0.50, a_max=0.75), **tol_kwargs)
                    assert_close(th.clamp_min(a_fpga, min=0.50), np.clip(a_cpu, a_min=0.50, a_max=1.00), **tol_kwargs)
                    assert_close(th.clamp_max(a_fpga, max=0.75), np.clip(a_cpu, a_min=0.00, a_max=0.75), **tol_kwargs)

    def test_ReduceStatsOpsKernel(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu"}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                sum_tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps * NUMEL}
                var_tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps * NUMEL, "atol": th.finfo(c2r_dtype[dtype]).eps * NUMEL}
                a_cpu = th.rand((NUMEL,), **th_kwargs)
                a_fpga = a_cpu.to(device)
                assert_close(float(t2n(th.sum(a_fpga))), float(np.sum(t2n(a_cpu))), **sum_tol_kwargs)
                assert_close(float(t2n(th.prod(a_fpga))), float(np.prod(t2n(a_cpu))), **tol_kwargs)
                assert_close(float(t2n(th.mean(a_fpga))), float(np.mean(t2n(a_cpu))), **tol_kwargs)
                assert_close(float(t2n(th.var(a_fpga))), float(np.var(t2n(a_cpu))), **var_tol_kwargs)
                # assert_close(float(t2n(th.std(a_fpga))), float(np.std(t2n(a_cpu)))**2, **var_tol_kwargs)

    def test_TensorCompareKernel(self):
        for device in devices:
            for dtype in dtypes:
                if dtype not in (th.complex64, th.complex128):
                    th_kwargs = {"dtype": dtype, "device": "cpu"}
                    tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                    a_cpu = th.rand((NUMEL,), **th_kwargs)
                    a_fpga = a_cpu.to(device)
                    assert_close(float(t2n(th.max(a_fpga, 0)[0])), float(np.max(t2n(a_cpu))), **tol_kwargs)
                    assert_close(float(t2n(th.min(a_fpga, 0)[0])), float(np.min(t2n(a_cpu))), **tol_kwargs)
                    # assert_close(int(t2n(th.max(a_fpga, 0)[1])), int(np.argmax(t2n(a_cpu))), **tol_kwargs)  # todo: index is wrong and changes on each call
                    # assert_close(int(t2n(th.min(a_fpga, 0)[1])), int(np.argmin(t2n(a_cpu))), **tol_kwargs)  # todo: index is wrong and changes on each call

    def test_SpectralOpsKernel(self):
        dtypes = (th.complex64,)
        def myround(a):
            return th.view_as_complex(th.view_as_real(a).round())
       
        for device in devices:
            for dtype in dtypes:
                if dtype not in ():
                    th_kwargs = {"dtype": dtype, "device": "cpu"}
                    tol_kwargs = {"rtol": 10**-(80/20), "atol": 1e100}  # float32 is converted to int32 and must have 80dB dynamic range
                    a_cpu = 2**15 * th.rand((16,), **th_kwargs)
                    a_fpga = a_cpu.to(device)
                    # a_fpga[0] = 1.0
                    assert_close(t2n(th.fft.fft(a_fpga)), np.fft.fft(myround(a_cpu)).astype(np.complex64), **tol_kwargs)
    
    def test_SpectralOpsKernelAutograd(self):
        dtypes = (th.complex64,)
        # - name: _fft_c2c(Tensor self, SymInt[] dim, int normalization, bool forward) -> Tensor
        #         self: _fft_c2c_symint(grad, dim, normalization, !forward)
        #         result: auto_linear
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": "cpu", "requires_grad": True}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}
                a_cpu = th.linspace(1, 16+1, 16, **th_kwargs)
                a_fpga = a_cpu.to(device)
                external_grad = th.ones_like(a_fpga)
                F = th.fft.fft(a_fpga)
                F.backward(gradient=external_grad)
                assert a_cpu.grad is not None, 'a_cpu.grad is None'
                assert len(a_cpu.grad) == 16, 'a_cpu.grad is not len(NUMEL)'
        # This runs but terminaates on tearDownClass: fpga.release_device()

    def test_DistributionTemplates(self):
        for device in devices:
            for dtype in dtypes:
                th_kwargs = {"dtype": dtype, "device": device}
                tol_kwargs = {"rtol": th.finfo(c2r_dtype[dtype]).eps, "atol": th.finfo(c2r_dtype[dtype]).eps}

                # Commented out to speed up testing.
                # mean = th.full((4096,), 0.0, **th_kwargs)
                # std = th.full((4096,), 1.0, **th_kwargs)
                # normal_std = np.sqrt(2) if dtype in (th.complex64, th.complex128) else 1.0  # Todo: complex std is wrong
                # result = th.normal(mean, std)
                # assert_allclose(t2n(result).mean(), 0.0, rtol=0.15, atol=0.15)
                # assert_allclose(t2n(result).std(), normal_std, rtol=0.15, atol=0.15)

                result = th.empty((4096,), **th_kwargs)
                uniform_mean = 0.5 + 0.5j if dtype in (th.complex64, th.complex128) else 0.5
                result.uniform_(0, 1)
                assert_allclose(t2n(result).mean(), uniform_mean, rtol=0.15, atol=0.15)
                assert_allclose(t2n(result).std(), np.sqrt(((1 - 0)**2)/12), rtol=0.15, atol=0.15)
                th.rand((4096,), **th_kwargs)
                th.randn((4096,), **th_kwargs)

    @classmethod
    def tearDownClass(cls):
        fpga.release_device()


if __name__ == '__main__':
    unittest.main()
