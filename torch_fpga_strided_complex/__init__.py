import sys
from pathlib import Path
import shutil

device_name = 'Xilinx'
program_name = 'binary_container_1.bin'
program_filepath = Path(sys.executable).parent
package_path = Path(__file__).parent

# Copy binaries into application binary folder
shutil.copyfile(str(package_path / 'shell.json'), str(program_filepath / 'shell.json'))
shutil.copyfile(str(package_path / 'emconfig.json'), str(program_filepath / 'emconfig.json'))
shutil.copyfile(str(package_path / 'xrt.ini'), str(program_filepath / 'xrt.ini'))
shutil.copyfile(str(package_path / program_name), str(program_filepath / program_name))

program_filename = str(Path(program_filepath / program_name))

