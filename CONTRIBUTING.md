# Contributing

Two development workflows are described:

* Kernel-Side Development: Implement the basic building blocks of the math library.
* Host-Side Deployment: Add the PyTorch boiler-plate code to efficiently re-use kernels in derived functions.

## Kernel-Side Development

### Design the Kernel in the /dev Folder

The /dev folder contains the following structure:
 
- /dev
    - Category
         - Kernel
         - Kernel2
    - Category2
         - Kernel1

Each Kernel directory has the following structure:

- Kernel
     - platform
         - CMakeLists.txt
     - kernel
         - CMakeLists.txt
     - link_
         - CMakeLists.txt
     - host
         - CMakeLists.txt
     - xrt
         - CMakeLists.txt
     
Each of these sub-directories contains a folder for each optimization level (${XCL_OPT_INDEX}) of the kernel. 
You don't need to implement every optimization level. The highest deployment level in currently ${XCL_OPT_INDEX} = 4.
For example the kernel sub-directory contains the following:

- kernel
     - buffered (${XCL_OPT_INDEX} = 1)
     - dataflow  (${XCL_OPT_INDEX} = 2)
     - distributed  (${XCL_OPT_INDEX} = 3)
     - vec  (${XCL_OPT_INDEX} = 4)
     - h2k  (${XCL_OPT_INDEX} = 5)
     - k2k  (${XCL_OPT_INDEX} = 6)
     
Implement a new kernel as follows:

1. Copy and Paste an existing Category or Kernel folder and find/replace Category and Kernel names.
2. Implement your kernel using the following resources:
  - [Xilinx Vitis Libraries](https://github.com/Xilinx/Vitis_Libraries).
  - [Vitis-AI](https://github.com/Xilinx/Vitis-AI).
  - Your own idea.
3. Write a simple test function in the **host** directory.

### Deploy the Kernel in the /aten Folder

Once you are happy with the kernel performance, deploy the kernel in the **aten/src/ATen/fpga/kernel** folder.
The directory structure is as follows:

- aten/src/ATen/fpga/kernel
     - Category
         - kernel.hpp
         - template
            - Category.txt (CMakeLists.txt template)
            - Category.cpp (kernel.cpp template)
         - Kernel
           - CMakeLists.txt
           - kernel.cpp
           
Try to deploy as many related kernels as possible.

## Host-Side Deployment

You now need to implement some boiler-plate code to wrap the kernel with the PyTorch API. The folder structure
on the host side is aligned with the PyTorch code organization.

1. Add FPGA dispatch calls to aten/src/ATen/host/fpga_functions.yaml.
2. Run the build to auto-generate aten/src/ATen/FPGAFunctions.cpp
3. Verfiy that the plumbing works:
    - Verify the dispatching in aten/src/ATen/FPGAFunctions.cpp.
    - Uncomment or modify code in the aten/src/ATen/host directory.
    - Added a kernel_stub in the aten/src/ATen/host/kernel directory that calls the kernel.
4. Register the kernel in CMakeLists.txt
    - add_category(Category, 1)
    - add_kernel(Category, kernel)
5. Configure k2k configuration file:
    - nk=kernel:1
6. Create a test function in test/test_torch.py
7. Build the code.
8. Run the test category with pytest: pytest --capture=sys test_torch.py::TestTensor::test_Category.

