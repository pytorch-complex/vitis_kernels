cmake_minimum_required(VERSION 3.10.0)
# Global Variables
if (CMAKE_SYSTEM_NAME MATCHES "Windows")
  set(SEP "\\")
  set(PATH_SEP "\\;")
else ()
  set(SEP "/")
  set(PATH_SEP ":")
endif()
set(CONDA_PREFIX "")
set(CMAKE_PREFIX_PATH $ENV{CMAKE_PREFIX_PATH})

## Xilinx
set(XILINX_VERSION 2023.2)
set(XILINX_XRT $ENV{XILINX_XRT})
set(XILINX_VIVADO /tools/Xilinx/Vivado/${XILINX_VERSION})
set(XILINX_VITIS /tools/Xilinx/Vitis/${XILINX_VERSION})
set(XILINX_HLS /tools/Xilinx/Vitis_HLS/${XILINX_VERSION})
set(XCL_PLATFORM $ENV{XCL_PLATFORM})
if(NOT XCL_PLATFORM)
    set(XCL_PLATFORM "xilinx_u200_gen3x16_xdma_2_202110_1")
endif()
message(STATUS "Selecting XCL_PLATFORM: ${XCL_PLATFORM}")
set(XCL_OPT_INDEX $ENV{XCL_OPT_INDEX})
if(NOT XCL_OPT_INDEX)
    set(XCL_OPT_INDEX 4)
endif()
set(XCL_OPTS none buffered dataflow distributed vec h2k k2k)
list(GET XCL_OPTS ${XCL_OPT_INDEX} XCL_OPT)
message(STATUS "Selecting XCL_OPTS[${XCL_OPT_INDEX}]: ${XCL_OPT}")
set(XCL_CU $ENV{XCL_CU})
if(NOT XCL_CU)
    set(XCL_CU 1)
endif()
message(STATUS "Selecting XCL_CU: ${XCL_CU}")
set(XCL_EMULATION_MODE $ENV{XCL_EMULATION_MODE})
if(NOT XCL_EMULATION_MODE)
    set(XCL_EMULATION_MODE "sw_emu")
endif()
message(STATUS "Selecting XCL_EMULATION_MODE: ${XCL_EMULATION_MODE}")
set(XCL_KERNEL $ENV{XCL_KERNEL})
if(NOT XCL_KERNEL)
    set(XCL_KERNEL "vf4add")
endif()
message(STATUS "Selecting XCL_KERNEL: ${XCL_KERNEL}")

## Xilinx Runtime
set(XRT_PROFILE "true")
set(XRT_TIMELINE_TRACE "true")
set(XRT_DATA_TRANSFER_TRACE "fine")

## CMake
if(WIN32)
    set(CMAKE_INSTALL_PREFIX ${CONDA_PREFIX}/Library)
else()   
    set(CMAKE_INSTALL_PREFIX ${CONDA_PREFIX})
endif()
set(CMAKE_SKIP_BUILD_RPATH FALSE)
set(CMAKE_BUILD_WITH_INSTALL_RPATH TRUE)
set(CMAKE_INSTALL_RPATH 
    ${CMAKE_INSTALL_PREFIX}/lib 
    ${XILINX_XRT}/lib
    )
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

## C++
set(CMAKE_CXX_STANDARD 17)

## Torch
set(Torch_DIR $ENV{Torch_DIR})

