function(UNIQUE ARG_STR OUTPUT)
  set(ARG_LIST ${ARGS})
  separate_arguments(ARG_LIST)
  list(REMOVE_DUPLICATES ARG_LIST)
  string (REGEX REPLACE "([^\\]|^);" "\\1 " _TMP_STR "${ARG_LIST}")
  string (REGEX REPLACE "[\\](.)" "\\1" _TMP_STR "${_TMP_STR}") #fixes escaping
  set (${OUTPUT} "${_TMP_STR}" PARENT_SCOPE)
endfunction()

# Function to resolve target library to its absolute path
function(resolve_target_library target)
    get_target_property(LIBRARY_PATH ${target} LOCATION)
    if(LIBRARY_PATH)
        # message(STATUS "Target ${target} library path: ${LIBRARY_PATH}")
        set(ARG ${LIBRARY_PATH} PARENT_SCOPE)
    else()
        #message(WARNING "Target ${target} does not have a LOCATION property.")
    endif()
    unset(LIBRARY_PATH CACHE)
endfunction()

# Function to resolve external library to its absolute path
function(find_library_path lib_name)
    find_library(LIB_PATH NAMES ${lib_name})
    if(LIB_PATH)
        # message(STATUS "External library ${lib_name} path: ${LIB_PATH}")
        set(ARG ${LIB_PATH} PARENT_SCOPE)
    else()
        # message(WARNING "External library ${lib_name} not found.")
    endif()
    unset(LIB_PATH CACHE)
endfunction()

# Retrieve and print the linked libraries for a specific target
function(get_linked_libraries target)
    get_target_property(LINK_LIBS ${target} INTERFACE_LINK_LIBRARIES)
    set(ARGS "")
    if(LINK_LIBS)
        foreach(lib ${LINK_LIBS})
            if(TARGET ${lib})
                resolve_target_library(${lib})
                list(APPEND ARGS ${ARG})
            else()
                find_library_path(${lib})
                list(APPEND ARGS ${ARG})
            endif()
        endforeach()
    else()
        message(WARNING "No linked libraries found for target ${target}.")
    endif()
    UNIQUE(ARGS ARGS)
    # message(STATUS ${ARGS})
    set(ARGS ${ARGS} PARENT_SCOPE)
endfunction()