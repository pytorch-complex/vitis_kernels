#pragma once

#include <c10/core/ScalarType.h>
#include <c10/fpga/FPGAMacros.h>

namespace c10 {

#define AT_ID_FORALL_SCALAR_TYPES_WITH_COMPLEX_AND_QINTS(_) \
  _("u1", Byte) /* 0 */                                   \
  _("i1", Char) /* 1 */                                   \
  _("i2", Short) /* 2 */                                  \
  _("i4", Int) /* 3 */                                    \
  _("i8", Long) /* 4 */                                   \
  _("f2", Half) /* 5 */                                   \
  _("f4", Float) /* 6 */                                  \
  _("f8", Double) /* 7 */                                 \
  _("c2", ComplexHalf) /* 8 */                            \
  _("c4", ComplexFloat) /* 9 */                           \
  _("c8", ComplexDouble) /* 10 */                         \
  _("b1", Bool) /* 11 */                                  \
  _("q1", QInt8) /* 12 */                                 \
  _("q2", QUInt8) /* 13 */                                \
  _("q3", QInt32) /* 14 */                                \
  _("bf", BFloat16) /* 15 */


static inline std::string toID(ScalarType t) {
#define DEFINE_CASE(id_, name) \
  case ScalarType::name:     \
    return id_;

  switch (t) {
    AT_ID_FORALL_SCALAR_TYPES_WITH_COMPLEX_AND_QINTS(DEFINE_CASE)
    default:
      return "_";
  }
#undef DEFINE_CASE
}

} // namespace c10
