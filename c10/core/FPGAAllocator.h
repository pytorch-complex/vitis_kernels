#pragma once
#include <cstdint>
#include <cstring>
#include <mutex>
#include <unordered_map>

#include <c10/core/Allocator.h>
#include <c10/macros/Export.h>
#include <c10/util/Flags.h>

// TODO: rename to c10
C10_DECLARE_bool(caffe2_report_fpga_memory_usage);

namespace c10 {

using MemoryDeleter = void (*)(void*);


// Get the FPGA Allocator.
C10_API at::Allocator* GetFPGAAllocator();
// Sets the FPGA allocator to the given allocator: the caller gives away the
// ownership of the pointer.
C10_API void SetFPGAAllocator(at::Allocator* alloc, uint8_t priority = 0);

// Get the Default FPGA Allocator
C10_API at::Allocator* GetDefaultFPGAAllocator();

} // namespace c10
