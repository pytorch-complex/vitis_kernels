#include <c10/core/Allocator.h>
#include <c10/core/FPGAAllocator.h>
#include <c10/core/DeviceType.h>
#include <c10/core/alignment.h>
#include <c10/core/impl/alloc_cpu.h>
#include <c10/util/Logging.h>

// TODO: rename flag to C10
C10_DEFINE_bool(
    caffe2_report_fpga_memory_usage,
    false,
    "If set, print out detailed memory usage");

namespace c10 {

struct C10_API DefaultFPGAAllocator final : at::Allocator {
  DefaultFPGAAllocator() = default;
  at::DataPtr allocate(size_t nbytes) override {
    void* data = nullptr;
    try {
      data = c10::alloc_cpu(nbytes);
    } catch (c10::Error& e) {
      throw e;
    }
    return {data, data, &ReportAndDelete, at::Device(at::DeviceType::FPGA)};
  }

  static void ReportAndDelete(void* ptr) {
    if (!ptr) {
      return;
    }
    free_cpu(ptr);
  }

  at::DeleterFnPtr raw_deleter() const override {
      return &ReportAndDelete;
    }

  void copy_data(void* dest, const void* src, std::size_t count) const final {
    default_copy_data(dest, src, count);
  }
};

at::Allocator* GetFPGAAllocator() {
  return GetAllocator(DeviceType::FPGA);
}

void SetFPGAAllocator(at::Allocator* alloc, uint8_t priority) {
  SetAllocator(DeviceType::FPGA, alloc, priority);
}

// Global default FPGA Allocator
static DefaultFPGAAllocator g_fpga_alloc;

at::Allocator* GetDefaultFPGAAllocator() {
  return &g_fpga_alloc;
}

REGISTER_ALLOCATOR(DeviceType::FPGA, &g_fpga_alloc);

} // namespace c10
