#pragma once

#include <c10/core/impl/DeviceGuardImplInterface.h>
#include <c10/macros/Macros.h>
#include <c10/util/Exception.h>

#include <c10/fpga/FPGAException.h>
//#include <c10/fpga/FPGAStream.h>
#include <c10/fpga/FPGAFunctions.h>

namespace c10 {
namespace fpga {
namespace impl {

struct FPGAGuardImpl final : public c10::impl::DeviceGuardImplInterface {
  static constexpr DeviceType static_type = DeviceType::FPGA;

  FPGAGuardImpl() {}
  explicit FPGAGuardImpl(DeviceType t) {
    TORCH_INTERNAL_ASSERT(t == DeviceType::FPGA);
  }
  /**
   * Return the type of device managed by this guard implementation.
   */
  DeviceType type() const override {
    return DeviceType::FPGA;
  }
  /**
   * Set the current device to Device, and return the previous Device.
   */
  Device exchangeDevice(Device d) const override {
    // no-op
//    return Device(DeviceType::FPGA, -1);
    cl_int error;
    TORCH_INTERNAL_ASSERT(d.type() == DeviceType::FPGA);
    Device old_device = getDevice();
    if (old_device.index() != d.index()) {
      C10_FPGA_CHECK(error, fpgaSetDevice(d.index()));
    }
    return old_device;
  }
  /**
   * Get the current device.
   */
  Device getDevice() const override {
//    return Device(DeviceType::FPGA, -1);
    cl_int error;
    DeviceIndex device;
    C10_FPGA_CHECK(error, fpgaGetDevice(device));
    return Device(DeviceType::FPGA, device);
  }
  /**
   * Set the current device to Device.
   */
  void setDevice(Device d) const override {
    // no-op
    cl_int error;
    TORCH_INTERNAL_ASSERT(d.type() == DeviceType::FPGA);
    Device current_device = getDevice();
    if (current_device != d) {
      C10_FPGA_CHECK(error, fpgaSetDevice(d.index()));
    }
  }
  /**
   * Set the current device to Device, without checking for errors
   * (so, e.g., this can be called from a destructor).
   */
  void uncheckedSetDevice(Device d) const noexcept override {
    // no-op
    cl_int warning;
    Device current_device = getDevice();
    if (current_device != d) {
      C10_FPGA_CHECK_WARN(warning, fpgaSetDevice(d.index()));
    }
  }
  /**
   * Get the current stream for a given device.
   */
  Stream getStream(Device d) const noexcept override {
    return Stream(Stream::DEFAULT, Device(d));
  }
  /**
   * Get the default stream for a given device.
   */
  Stream getDefaultStream(Device d) const override {
    return Stream(Stream::DEFAULT, Device(d));
  }

  /**
   * Set a stream to be the thread local current stream for its device.
   * Return the previous stream for that device. You are NOT required
   * to set the current device to match the device of this stream.
   */
  Stream exchangeStream(Stream s) const noexcept override {
    // no-op
  }

  /**
  * Destroys the given event.
  */
  void destroyEvent(
    void* event,
    const DeviceIndex device_index) const noexcept override {
      // no-op
  }

/**
 * Increments the event's version and enqueues a job with this version
 * in the stream's work queue. When the stream process that job
 * it nofifies all streams waiting on / blocked by that version of the
 * event to continue and marks that version as recorded.
 * */
  void record(
    void** event,
    const Stream& stream,
    const DeviceIndex device_index,
    const c10::EventFlag flag) const override {
    TORCH_CHECK(false, "FPGA backend doesn't support events.");
  }

/**
 * Does nothing if the event has not been scheduled to be recorded.
 * If the event was previously enqueued to be recorded, a command
 * to wait for the version of the event that exists at the time of this call
 * is inserted in the stream's work queue.
 * When the stream reaches this command it will stop processing
 * additional commands until that version of the event is marked as recorded.
 */
  void block(
    void* event,
    const Stream& stream) const override {
    TORCH_CHECK(false, "FPGA backend doesn't support events.")
  }

/**
 * Returns true if (and only if)
 *  (1) the event has never been scheduled to be recorded
 *  (2) the current version is marked as recorded.
 * Returns false otherwise.
 */
  bool queryEvent(void* event) const override {
    TORCH_CHECK(false, "FPGA backend doesn't support events.")
  }

  /**
   * Get the number of devices.  WARNING: This is REQUIRED to not raise
   * an exception.  If there is some sort of problem, e.g., driver error,
   * you should report that there are zero available devices.
   */
  DeviceIndex deviceCount() const noexcept override {
    return device_count();
  }
  /**
   * Intended use of this class is to leak the DeviceGuardImpl at program end.
   * So you better not call the destructor, buster!
   */
  virtual ~FPGAGuardImpl() = default;
};

}}} // namespace c10::fpga::impl
