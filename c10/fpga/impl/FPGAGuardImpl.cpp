#include <c10/fpga/impl/FPGAGuardImpl.h>

namespace c10 {
namespace fpga {
namespace impl {

constexpr DeviceType FPGAGuardImpl::static_type;

C10_REGISTER_GUARD_IMPL(FPGA, FPGAGuardImpl);

}}} // namespace c10::fpga::detail
