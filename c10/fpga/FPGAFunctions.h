#pragma once

#include <vector>
#include <unistd.h>
#include <iostream>
#include <fstream>

#include <c10/macros/Macros.h>
#include <c10/core/Device.h>
#include <c10/macros/Export.h>
#include <c10/fpga/FPGAMacros.h>
#include <c10/fpga/FPGAException.h>

#include <CL/opencl.hpp>

/*
A common FPGA interface for ATen.
*/

struct FPGAState
{
    static inline cl::Platform platform = cl::Platform();
    static inline c10::DeviceIndex device_index = -1;
    static inline cl::Device device = cl::Device();
    static inline cl::Context context = cl::Context();
    static inline cl::Program program = cl::Program();
    static inline std::string program_filename = std::string();
    static inline cl::CommandQueue q = cl::CommandQueue();
};


C10_EXPORT inline cl::Platform& fpgaGetPlatform();
C10_EXPORT inline void fpgaReleasePlatform();
C10_EXPORT inline void fpgaLoadPlatform(const std::string &name);

C10_EXPORT inline cl_int fpgaGetDevice();
C10_EXPORT inline void fpgaReleaseDevice();
C10_EXPORT inline cl_int fpgaSetDevice(const c10::DeviceIndex index);

C10_EXPORT inline cl::Context& fpgaGetContext();
C10_EXPORT inline void fpgaReleaseContext();

C10_EXPORT inline cl::Program& fpgaGetProgram();
C10_EXPORT inline void fpgaReleaseProgram();
C10_EXPORT inline void fpgaSetProgram();
C10_EXPORT inline void fpgaSetProgramFilename(const std::string &xclbin_filename);

C10_EXPORT inline cl::CommandQueue& fpgaGetQ();
C10_EXPORT inline void fpgaReleaseQ();


C10_EXPORT inline cl::Platform& fpgaGetPlatform(){
  return FPGAState::platform;
}

C10_EXPORT inline void fpgaReleasePlatform(){
  fpgaReleaseDevice();
  FPGAState::platform.~Platform();
  FPGAState::platform = cl::Platform();
}

C10_EXPORT inline void fpgaLoadPlatform(const std::string &name) {
  cl_int err;
  size_t i;
  std::vector<cl::Platform> platforms;

  OCL_CHECK(err, err = cl::Platform::get(&platforms));
  for (i  = 0 ; i < platforms.size(); i++){
    OCL_CHECK(err, std::string platformName = FPGAState::platform.getInfo<CL_PLATFORM_NAME>(&err));
    if (platformName == name){
        FPGAState::platform = platforms[i];
        return;
    }
  }
  std::cout << "Error: Failed to find Xilinx platform" << std::endl;
  exit(EXIT_FAILURE);
}

C10_EXPORT inline cl_int fpgaGetDevice(c10::DeviceIndex &index){
  cl_int err = CL_SUCCESS;
  index = FPGAState::device_index;
  return err;
}

C10_EXPORT inline void fpgaReleaseDevice(){
  fpgaReleaseContext();
  FPGAState::device.~Device();
  FPGAState::device = cl::Device();
  FPGAState::device_index = -1;
}

C10_EXPORT inline cl_int fpgaSetDevice(const c10::DeviceIndex index){
  cl_int err;
  std::vector<cl::Device> _devices;
  size_t device_index = static_cast<size_t>(index);

  fpgaReleasePlatform();
  fpgaLoadPlatform("Xilinx");
  OCL_CHECK(err, err = FPGAState::platform.getDevices(CL_DEVICE_TYPE_ACCELERATOR, &_devices));
  if (device_index >= _devices.size()) {
    std::cout << "Error: Failed to find Device Index" << std::endl;
    exit(EXIT_FAILURE);
  }

  //Set Context and Command Queue
  FPGAState::device = _devices[device_index];
  FPGAState::device_index = index;
  OCL_CHECK(err, FPGAState::context = cl::Context(_devices[device_index], NULL, NULL, NULL, &err));
  OCL_CHECK(err, FPGAState::q = cl::CommandQueue(FPGAState::context, _devices[device_index], CL_QUEUE_TYPE, &err));
  fpgaSetProgram();
  return err;
}

C10_EXPORT inline c10::DeviceIndex device_count() noexcept {
  cl_int err;
  std::vector<cl::Device> _devices;
  OCL_CHECK(err, err = FPGAState::platform.getDevices(CL_DEVICE_TYPE_ACCELERATOR, &_devices));
  return static_cast<c10::DeviceIndex>(_devices.size());
}

C10_EXPORT inline cl::Context& fpgaGetContext(){
  return FPGAState::context;
}

C10_EXPORT inline void fpgaReleaseContext(){
  fpgaReleaseProgram();
  FPGAState::context.~Context();
  FPGAState::context = cl::Context();
}

C10_EXPORT inline cl::Program& fpgaGetProgram(){
  return FPGAState::program;
}

C10_EXPORT inline void fpgaReleaseProgram(){
  fpgaReleaseQ();
  FPGAState::program.~Program();
  FPGAState::program = cl::Program();
}

C10_EXPORT inline void fpgaSetProgram(){
  cl_int err;
  std::cout << "INFO: Reading " << FPGAState::program_filename << std::endl;
  if(access(FPGAState::program_filename.c_str(), R_OK) != 0) {
    printf("ERROR: %s xclbin not available please build\n", FPGAState::program_filename.c_str());
    exit(EXIT_FAILURE);
  }
  //Loading XCL Bin into char buffer
  std::ifstream bin_file(FPGAState::program_filename.c_str(), std::ifstream::binary);
  bin_file.seekg (0, bin_file.end);
  unsigned nb = bin_file.tellg();
  bin_file.seekg (0, bin_file.beg);
  char *buf = new char [nb];
  bin_file.read(buf, nb);

  cl::Program::Binaries bins{{buf, nb}};
  std::vector<cl::Device> _devices = {FPGAState::device};
  OCL_CHECK(err, FPGAState::program = cl::Program(FPGAState::context, _devices, bins, NULL, &err));
  delete[] buf;
}

C10_EXPORT inline void fpgaSetProgramFilename(const std::string& xclbin_filename){
  FPGAState::program_filename = xclbin_filename;
}

C10_EXPORT inline cl::CommandQueue& fpgaGetQ(){
  return FPGAState::q;
}

C10_EXPORT inline void fpgaReleaseQ(){
  FPGAState::q.~CommandQueue();
  FPGAState::q = cl::CommandQueue();
}
