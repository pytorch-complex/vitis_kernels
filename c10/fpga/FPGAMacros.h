#pragma once

#include <ap_fixed.h>
#include <complex>
#include <vt_fft.hpp>
#include <vt_fft_L2.hpp>


#define CL_HPP_CL_1_2_DEFAULT_BUILD
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_ENABLE_PROGRAM_CONSTRUCTION_FROM_ARRAY_COMPATIBILITY 1
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

#define XCL_OPT_INDEX 4
#define XCL_CU 1
#define XCL_TEST_SIZE XCL_CU*65536
#define XCL_PLATFORM "xilinx_u200_gen3x16_xdma_2_202110_1"
#define XCL_EMULATION_MODE "sw_emu"
#define USE_LIBTORCH 
#define INPUT_BUFFER  CL_MEM_USE_HOST_PTR | CL_MEM_READ_ONLY
#define OUTPUT_BUFFER CL_MEM_USE_HOST_PTR | CL_MEM_WRITE_ONLY

#define BYTE_SIZE 8
#define PAR_SIZE (1 << PAR_LOG_SIZE)
#define RN_PAR_SIZE (1 << RN_LOG_SIZE)

#define SIZE_BITS sizeof(T)*BYTE_SIZE
#define PAR_SIZE_BYTES sizeof(T)*PAR_SIZE
#define PAR_SIZE_BITS sizeof(T)*BYTE_SIZE*PAR_SIZE
#define DEST_SIZE_BITS sizeof(DEST_T)*BYTE_SIZE
#define DEST_PAR_SIZE_BYTES sizeof(DEST_T)*PAR_SIZE
#define DEST_PAR_SIZE_BITS sizeof(DEST_T)*BYTE_SIZE*PAR_SIZE
#define SRC_SIZE_BITS sizeof(SRC_T)*BYTE_SIZE
#define SRC_PAR_SIZE_BYTES sizeof(SRC_T)*PAR_SIZE
#define SRC_PAR_SIZE_BITS sizeof(SRC_T)*BYTE_SIZE*PAR_SIZE

// Define Numfer of FFTs, FFT Size, and Super Sample Rate
#define N_FFT 1
#define FFT_LEN 16
#define SSR 8
#define IID 0

// Define parameter structure for FFT
struct fftParams : xf::dsp::fft::ssr_fft_default_params {
    static const int NUM_FFT_MAX = N_FFT;
    static const int N = FFT_LEN;
    static const int R = SSR;
};

#ifndef XCL_OPT_INDEX
  #error("XCL_OPT_INDEX is undefined")
#else
  #if XCL_OPT_INDEX==1
    #define XCL_OPT "buffered"
    #define CL_QUEUE_TYPE CL_QUEUE_PROFILING_ENABLE
  #elif XCL_OPT_INDEX==2
    #define XCL_OPT "dataflow"
    #define CL_QUEUE_TYPE CL_QUEUE_PROFILING_ENABLE
  #elif XCL_OPT_INDEX==3
    #define XCL_OPT "distributed"
    #define CL_QUEUE_TYPE CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
  #elif XCL_OPT_INDEX==4
    #define XCL_OPT "vec"
    #define CL_QUEUE_TYPE CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
  #elif XCL_OPT_INDEX==5
    #define XCL_OPT "h2k"
    #define CL_QUEUE_TYPE CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
  #elif XCL_OPT_INDEX==6
    #define XCL_OPT "k2k"
    #define CL_QUEUE_TYPE CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
  #else
    #define CL_QUEUE_TYPE CL_QUEUE_PROFILING_ENABLE | CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
  #endif
#endif
