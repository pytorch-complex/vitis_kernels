#pragma once

#include <c10/util/Exception.h>
#include <c10/macros/Macros.h>

//OCL_CHECK doesn't work if call has templatized function call
#define OCL_CHECK(error,call)                                       \
    call;                                                           \
    if (error != CL_SUCCESS) {                                      \
      printf("%s:%d Error calling " #call ", error code is: %d\n",  \
              __FILE__,__LINE__, error);                            \
      exit(EXIT_FAILURE);                                           \
    }

#define C10_FPGA_CHECK(error, call)                                     \
    error = call;                                                       \
    if (error != CL_SUCCESS) {                                          \
      char* str;                                                        \
      sprintf(str, "%s:%d Error calling " #call ", error code is: %d\n",\
              __FILE__,__LINE__, error);                                \
      TORCH_CHECK(false, "FPGA error: ", str);                          \
      exit(EXIT_FAILURE);                                               \
    }

#define C10_FPGA_CHECK_WARN(warning, call)                                  \
    warning = call;                                                         \
    if (warning != CL_SUCCESS) {                                            \
      char* str;                                                            \
      sprintf(str, "%s:%d Warning calling " #call ", warning code is: %d\n",\
              __FILE__,__LINE__, warning);                                  \
      TORCH_WARN("FPGA warning: ", str);                                    \
    }

