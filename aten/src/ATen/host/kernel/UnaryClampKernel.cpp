#include <cmath>
#include <iostream>
#include <ATen/Dispatch.h>
#include <ATen/Parallel.h>
#include <ATen/native/TensorIterator.h>
#include <ATen/host/kernel/Loops.h>

namespace at { namespace fpga {
namespace {

#ifdef _MSC_VER
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif

void clamp_stub(TensorIterator& iter, Scalar min_scalar, Scalar max_scalar) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "clamp_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    std::vector<vec_t> scalar_vec = {vec_t(min_scalar.to<scalar_t>()), vec_t(max_scalar.to<scalar_t>()) };
    fpga_kernel<vec_t>("v__clamp", iter, scalar_vec);
  });
}

void clamp_min_stub(TensorIterator& iter, Scalar min_scalar) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "clamp_min_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    std::vector<vec_t> scalar_vec = {vec_t(min_scalar.to<scalar_t>()) };
    fpga_kernel<vec_t>("v__clamp_min", iter, scalar_vec);
  });
}

void clamp_max_stub(TensorIterator& iter, Scalar max_scalar) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "clamp_max_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    std::vector<vec_t> scalar_vec = {vec_t(max_scalar.to<scalar_t>()) };
    fpga_kernel<vec_t>("v__clamp_max", iter, scalar_vec);
  });
}

#ifdef _MSC_VER
#else
#pragma GCC diagnostic pop
#endif

} // anonymous namespace

}} // namespace at::fpga
