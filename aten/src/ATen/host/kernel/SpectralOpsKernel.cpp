#include <cmath>
#include <iostream>
#include <ATen/Dispatch.h>
#include <ATen/Parallel.h>
#include <ATen/native/TensorIterator.h>
#include <ATen/host/kernel/invoke.h>

namespace at { namespace fpga {
namespace {

#ifdef _MSC_VER
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif

Tensor fft_stub(Tensor& result, const Tensor& self) {

    auto iter = TensorIteratorConfig()
    .add_output(result)
    .add_input(self)
    .build();

    AT_DISPATCH_INTEGRAL_TYPES(iter.dtype(), "fft_stub", [&](){
      using vec_t = at::native::ztype<scalar_t>::vec_t;
      fpga_kernel_no_scalar_vec<vec_t>("v__fft", iter);
    });
    return result;
}

#ifdef _MSC_VER
#else
#pragma GCC diagnostic pop
#endif

} // anonymous namespace

}} // namespace at::fpga
