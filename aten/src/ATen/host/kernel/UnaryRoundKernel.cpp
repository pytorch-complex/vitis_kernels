#include <cmath>
#include <iostream>
#include <ATen/Dispatch.h>
#include <ATen/Parallel.h>
#include <ATen/native/TensorIterator.h>
#include <ATen/host/kernel/Loops.h>

namespace at { namespace fpga {
namespace {

#ifdef _MSC_VER
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif

void ceil_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "ceil_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__ceil", iter);
  });
}

void floor_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "floor_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__floor", iter);
  });
}

void round_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "round_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__round", iter);
  });
}

void trunc_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "trunc_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__trunc", iter);
  });
}

#ifdef _MSC_VER
#else
#pragma GCC diagnostic pop
#endif

} // anonymous namespace

}} // namespace at::fpga
