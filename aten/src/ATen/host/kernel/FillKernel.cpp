#include <ATen/Dispatch.h>
#include <ATen/Parallel.h>
#include <ATen/native/TensorIterator.h>
#include <ATen/host/kernel/Loops.h>

namespace at { namespace fpga {
namespace {

void fill_kernel(TensorIterator& iter, Scalar value_scalar) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "fill_cpu", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    std::vector<vec_t> scalar_vec = {vec_t(value_scalar.to<scalar_t>())};
    fpga_kernel<vec_t>("v__fill", iter, scalar_vec);
  });
}

} // namespace

} // namespace fpga
} // namespace at
