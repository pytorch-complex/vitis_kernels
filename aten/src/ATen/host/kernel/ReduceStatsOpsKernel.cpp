#include <numeric>
#include <iterator>
#include <algorithm>
#include <limits>

#include <ATen/Dispatch.h>
#include <ATen/native/TensorIterator.h>
#include <c10/util/Optional.h>
#include <ATen/host/kernel/Reduce.h>

namespace at { namespace fpga {
namespace {

#ifdef _MSC_VER
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif

void sum_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "sum_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_reduce_no_vec<vec_t>("v__sum", iter);
  });
}

void prod_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "prod_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_reduce_no_vec<vec_t>("v__prod", iter);
  });
}

void mean_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "mean_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_reduce_no_vec<vec_t>("v__mean", iter);
  });
}

void std_var_stub(TensorIterator& iter, bool unbiased, bool take_sqrt) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "std_var_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_reduce_no_vec<vec_t>("v__std_var", iter);
  });
}

#ifdef _MSC_VER
#else
#pragma GCC diagnostic pop
#endif

} // anonymous namespace

}} // namespace at::fpga
