#include <cmath>
#include <iostream>
#include <ATen/Dispatch.h>
#include <ATen/Parallel.h>
#include <ATen/native/TensorIterator.h>
#include <ATen/host/kernel/Loops.h>

namespace at { namespace fpga {
namespace {

#ifdef _MSC_VER
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif

void cos_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "cos_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__cos", iter);
  });
}

void sin_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "sin_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__sin", iter);
  });
}

void tan_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "tan_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__tan", iter);
  });
}

#ifdef _MSC_VER
#else
#pragma GCC diagnostic pop
#endif

} // anonymous namespace

}} // namespace at::fpga
