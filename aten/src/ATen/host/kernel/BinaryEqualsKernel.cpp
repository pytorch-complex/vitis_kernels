#include <cmath>
#include <iostream>
#include <ATen/Dispatch.h>
#include <ATen/Parallel.h>
#include <ATen/native/TensorIterator.h>
#include <ATen/host/kernel/Loops.h>

namespace at { namespace fpga {
namespace {

#ifdef _MSC_VER
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif

void eq_stub(TensorIterator& iter) {
  if (iter.dtype() == ScalarType::Bool) {
    AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES_AND1(kBool, iter.input_dtype(), "eq_out", [&](){
      using vec_t = at::native::ztype<scalar_t>::vec_t;
      fpga_kernel_no_scalar_vec<vec_t>("v__eq", iter);
    });
  } else {
    AT_ERROR("Only boolean return type is supported.");
  }
}

void ne_stub(TensorIterator& iter) {
  if (iter.dtype() == ScalarType::Bool) {
    AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES_AND1(kBool, iter.input_dtype(), "ne_out", [&](){
      using vec_t = at::native::ztype<scalar_t>::vec_t;
      fpga_kernel_no_scalar_vec<vec_t>("v__ne", iter);
    });
  } else {
    AT_ERROR("Only boolean return type is supported.");
  }
}

#ifdef _MSC_VER
#else
#pragma GCC diagnostic pop
#endif

} // anonymous namespace

}} // namespace at::fpga
