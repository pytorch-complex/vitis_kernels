#pragma once

#include <c10/core/FPGAScalarType.h>
#include <ATen/fpga/vec/vec_complex.h>

namespace at { namespace native {
namespace {

template <typename VALUE_TYPE>
struct ztype {
  using vec_t = at::vec::Vec<VALUE_TYPE, 0, 0>;
};

//template <>
//struct ztype<byte> {
//  using vec_t = at::vec::Vec<byte, 6, 0>;
//};

template <>
struct ztype<char> {
  using vec_t = at::vec::Vec<char, 6, 0>;
};

template <>
struct ztype<short> {
  using vec_t = at::vec::Vec<short, 5, 0>;
};

template <>
struct ztype<int> {
  using vec_t = at::vec::Vec<int, 4, 0>;
};

template <>
struct ztype<long> {
  using vec_t = at::vec::Vec<long, 3, 0>;
};

//template <>
//struct ztype<half> {
//  using vec_t = at::vec::Vec<half, 5, 0>;
//};

template <>
struct ztype<float> {
  using vec_t = at::vec::Vec<float, 4, 0>;
};

template <>
struct ztype<double> {
  using vec_t = at::vec::Vec<double, 3, 0>;
};

//template <>
//struct ztype<std::complex<half>> {
//  using vec_t = at::vec::Vec<half, 5, 1>;
//};

template <>
struct ztype<std::complex<float>> {
  using vec_t = at::vec::Vec<float, 4, 1>;
};

template <>
struct ztype<std::complex<double>> {
  using vec_t = at::vec::Vec<double, 4, 1>;
};

template <>
struct ztype<bool> {
  using vec_t = at::vec::Vec<bool, 6, 0>;
};

} // end namespace
}} //end at::native
