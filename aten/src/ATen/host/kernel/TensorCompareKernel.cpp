#include <numeric>
#include <iterator>
#include <algorithm>
#include <iostream>

#include <ATen/Dispatch.h>
#include <ATen/Parallel.h>
#include <ATen/NumericUtils.h>
#include <c10/util/Optional.h>
#include <ATen/native/TensorIterator.h>
#include <ATen/native/ReduceOpsUtils.h>
#include <ATen/host/kernel/Reduce.h>

namespace at { namespace fpga {
namespace {

#ifdef _MSC_VER
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif

void max_stub(
        Tensor& result,
        Tensor& indice,
        const Tensor& self,
        int64_t dim,
        bool keepdim) {
  auto ind = indice.toType(result.scalar_type());
  auto iter = TensorIteratorConfig()
    .add_output(result)
    .add_output(ind)
    .add_input(self)
    .build();
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "max_cpu", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_reduce_no_vec<vec_t>("v__max", iter);
  });
}

void min_stub(
        Tensor& result,
        Tensor& indice,
        const Tensor& self,
        int64_t dim,
        bool keepdim) {
  auto ind = indice.toType(result.scalar_type());
  auto iter = TensorIteratorConfig()
    .add_output(result)
    .add_output(ind)
    .add_input(self)
    .build();
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "min_cpu", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_reduce_no_vec<vec_t>("v__min", iter);
  });
}

#ifdef _MSC_VER
#else
#pragma GCC diagnostic pop
#endif

} // anonymous namespace

}} // namespace at::fpga
