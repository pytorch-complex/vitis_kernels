#pragma once

// This file provides a function to help write FPGA elementwise kernels:
//
//   fpga_kernel<scalar_t>(TensorIterator &iter, c10:Scalar &scalar, std::string kernel)

#include <ATen/native/TensorIterator.h>
#include <torch/torch.h>

#include <c10/fpga/FPGAFunctions.h>
#include <ATen/host/kernel/dtype.h>

namespace at { namespace fpga { namespace {

static constexpr size_t cu_bound = static_cast<size_t>(XCL_CU);

template<int cu_size>
static inline void execute_op(cl::Kernel &kernel, std::vector<cl::Event> &events, size_t n, size_t arg_num) {
  cl_int err;
  size_t stride = n/cu_size;
  size_t offset = 0;
  cl::CommandQueue q = fpgaGetQ();

  // Enqueue Kernels Tasks for Execution
  std::vector<cl::Event> task_events;
  OCL_CHECK(err, err = kernel.setArg(arg_num + 0, stride));
  for(size_t cu = 0; cu < cu_size; cu++) {
    cl::Event task_event;
    offset = cu*stride;
    OCL_CHECK(err, err = kernel.setArg(arg_num + 1, offset));
    OCL_CHECK(err, err = q.enqueueTask(kernel, &events, &task_event));
    task_events.push_back(task_event);
  }
  copy(begin(task_events), end(task_events), std::back_inserter(events));
}

static inline void set_tensor_arg(cl::Kernel &k, size_t &arg_num, const Tensor &tensor, std::vector<cl::Buffer> &buffers, size_t n) {
  cl_int err;
  size_t size_bytes = tensor.element_size()*n;
  cl::Context context = fpgaGetContext();
  unsigned long buffer_type = (arg_num > 0) ? INPUT_BUFFER : OUTPUT_BUFFER;
  OCL_CHECK(err, cl::Buffer buffer(context, buffer_type, size_bytes, tensor.data_ptr(), &err));
  OCL_CHECK(err, err = k.setArg(uint(arg_num), buffer));
  buffers[arg_num] = buffer;
  arg_num++;
}

static inline void set_tensor_arg(cl::Kernel &k, size_t &arg_num, Tensor &tensor, std::vector<cl::Buffer> &buffers, size_t n) {
  cl_int err;
  size_t size_bytes = tensor.element_size()*n;
  cl::Context context = fpgaGetContext();
  unsigned long buffer_type = (arg_num > 0) ? INPUT_BUFFER : OUTPUT_BUFFER;
  OCL_CHECK(err, cl::Buffer buffer(context, buffer_type, size_bytes, tensor.data_ptr(), &err));
  OCL_CHECK(err, err = k.setArg(uint(arg_num), buffer));
  buffers[arg_num] = buffer;
  arg_num++;
}

template <typename DTYPE>
static inline void set_scalar_vec_arg(cl::Kernel &k, size_t &arg_num, DTYPE& scalar_vec, std::vector<cl::Buffer> &buffers) {
  cl_int err;
  size_t size_bytes = scalar_vec.par_size_bytes();
  cl::Context context = fpgaGetContext();
  unsigned long buffer_type = INPUT_BUFFER;
  OCL_CHECK(err, cl::Buffer buffer(context, buffer_type, size_bytes, scalar_vec.data_ptr(), &err));
  OCL_CHECK(err, err = k.setArg(uint(arg_num), buffer));
  buffers[arg_num] = buffer;
  arg_num++;
}

template <typename T>
static inline void set_scalar_arg(cl::Kernel &k, size_t &arg_num, T &scalar) {
  cl_int err;
  OCL_CHECK(err, err = k.setArg(uint(arg_num), scalar));
  arg_num++;
}

template <typename DTYPE, typename ... Args>
static inline void invoke(cl::Kernel &kernel, TensorIterator& iter, std::vector<DTYPE>& scalar_vec, Args&& ... scalars) {
  cl_int err;
  size_t arg_num = 0;
  size_t num_tensors = iter.ntensors();
  size_t num_scalar_vecs = scalar_vec.size();
  size_t n = iter.numel();
  cl::CommandQueue q = fpgaGetQ();

  // Allocate Global Memory Buffers
  std::vector<cl::Buffer> buffer(num_tensors + num_scalar_vecs);
  for (size_t i = 0; i < num_tensors; ++i) {
    set_tensor_arg(kernel, arg_num, iter.tensor(i), buffer, n);
  }
  for (size_t i = 0; i < num_scalar_vecs; ++i) {
    set_scalar_vec_arg(kernel, arg_num, scalar_vec[i], buffer);
  }
  std::invoke([&](auto& ...scalar){(..., set_scalar_arg(kernel, arg_num, scalar));}, scalars ...);

  // Enqueue Input Memory Transfer
  std::vector<cl::Event> events{};
  cl::Event write_event;
  std::vector<cl::Memory> memory_in(buffer.cbegin() + 1, buffer.cbegin() + int(num_tensors + num_scalar_vecs));
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects(memory_in, 0/* 0 means from host*/, nullptr, &write_event));
  events.push_back(write_event);

  execute_op<cu_bound>(kernel, events, n, arg_num);

  // Enqueue Output Memory Transfer
  std::vector<cl::Event> read_events{};
  cl::Event read_event;
  std::vector<cl::Memory> memory_out = {buffer[0]};
  OCL_CHECK(err, err = q.enqueueMigrateMemObjects(memory_out, CL_MIGRATE_MEM_OBJECT_HOST, &events, &read_event));
  events.push_back(read_event);
  q.finish();
}

template <typename DTYPE, typename ... Args>
static inline void launch_kernel(std::string& kernel, TensorIterator& iter, std::vector<DTYPE>& scalar_vec, Args&& ... scalars) {
  cl_int err;
  ScalarType dtype = iter.ninputs() ? iter.input_dtype() : iter.dtype();
  std::string sub_kernel = kernel.replace(1, 2, toID(dtype));
  cl::Program program = fpgaGetProgram();
  OCL_CHECK(err, cl::Kernel k(program, sub_kernel.c_str(), &err));
  invoke(k, iter, scalar_vec, scalars ...);
  k.~Kernel();
  k = cl::Kernel();
}

template <typename DTYPE, typename ... Args>
void fpga_kernel(std::string kernel, TensorIterator& iter, std::vector<DTYPE>& scalar_vec, Args&& ... scalars) {
  launch_kernel(kernel, iter, scalar_vec, scalars ...);
}

template <typename DTYPE, typename ... Args>
void fpga_kernel_no_scalar_vec(std::string kernel, TensorIterator& iter, Args&& ... scalars) {
  std::vector<DTYPE> scalar_vec = {};
  launch_kernel(kernel, iter, scalar_vec, scalars ...);
}

}}}  // namespace at::fpga::<anonymous>
