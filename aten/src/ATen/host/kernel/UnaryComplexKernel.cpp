#include <cmath>
#include <iostream>
#include <ATen/Dispatch.h>
#include <ATen/Parallel.h>
#include <ATen/native/TensorIterator.h>
#include <ATen/host/kernel/Loops.h>

namespace at { namespace fpga {
namespace {

#ifdef _MSC_VER
#else
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-local-typedefs"
#endif

void abs_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "abs_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__abs", iter);
  });
}

void angle_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "angle_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__angle", iter);
  });
}

#if 0
void real_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "real_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__real", iter);
  });
}

void imag_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "imag_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__imag", iter);
  });
}
#endif

void conj_stub(TensorIterator& iter) {
  AT_DISPATCH_FLOATING_AND_COMPLEX_TYPES(iter.dtype(), "conj_out", [&](){
    using vec_t = at::native::ztype<scalar_t>::vec_t;
    fpga_kernel_no_scalar_vec<vec_t>("v__conj", iter);
  });
}

#ifdef _MSC_VER
#else
#pragma GCC diagnostic pop
#endif

} // anonymous namespace

}} // namespace at::fpga
