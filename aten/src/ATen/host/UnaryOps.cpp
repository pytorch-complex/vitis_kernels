#include <ATen/core/Tensor.h>
#include <ATen/ExpandUtils.h>
#include <ATen/MemoryOverlap.h>
#include <ATen/NamedTensorUtils.h>
#include <ATen/Parallel.h>
#include <ATen/ScalarOps.h>
#include <ATen/TensorIterator.h>
#include <ATen/TensorOperators.h>
#include <ATen/WrapDimUtils.h>

#include <ATen/native/Resize.h>
#include <ATen/native/UnaryOps.h>
#include <ATen/native/ComplexHelper.h>

#include <c10/util/MathConstants.h>

#ifndef AT_PER_OPERATOR_HEADERS
#include <ATen/Functions.h>
#include <ATen/NativeFunctions.h>
#else
#include <ATen/ops/_conj_native.h>
#include <ATen/ops/_conj_physical.h>
#include <ATen/ops/_conj_physical_native.h>
#include <ATen/ops/_neg_view_native.h>
#include <ATen/ops/abs.h>
#include <ATen/ops/abs_native.h>
#include <ATen/ops/absolute_native.h>
#include <ATen/ops/acos.h>
#include <ATen/ops/acos_native.h>
#include <ATen/ops/acosh.h>
#include <ATen/ops/acosh_native.h>
#include <ATen/ops/angle.h>
#include <ATen/ops/angle_native.h>
#include <ATen/ops/arange_native.h>
#include <ATen/ops/arccos_native.h>
#include <ATen/ops/arccosh_native.h>
#include <ATen/ops/arcsin_native.h>
#include <ATen/ops/arcsinh_native.h>
#include <ATen/ops/arctan_native.h>
#include <ATen/ops/arctanh_native.h>
#include <ATen/ops/asin.h>
#include <ATen/ops/asin_native.h>
#include <ATen/ops/asinh.h>
#include <ATen/ops/asinh_native.h>
#include <ATen/ops/atan.h>
#include <ATen/ops/atan_native.h>
#include <ATen/ops/atanh.h>
#include <ATen/ops/atanh_native.h>
#include <ATen/ops/bitwise_not_native.h>
#include <ATen/ops/can_cast.h>
#include <ATen/ops/ceil_native.h>
#include <ATen/ops/conj_native.h>
#include <ATen/ops/conj_physical.h>
#include <ATen/ops/conj_physical_native.h>
#include <ATen/ops/cos_native.h>
#include <ATen/ops/cosh_native.h>
#include <ATen/ops/deg2rad.h>
#include <ATen/ops/deg2rad_native.h>
#include <ATen/ops/digamma.h>
#include <ATen/ops/digamma_native.h>
#include <ATen/ops/empty.h>
#include <ATen/ops/empty_like.h>
#include <ATen/ops/erf.h>
#include <ATen/ops/erf_native.h>
#include <ATen/ops/erfc.h>
#include <ATen/ops/erfc_native.h>
#include <ATen/ops/erfinv.h>
#include <ATen/ops/erfinv_native.h>
#include <ATen/ops/exp2.h>
#include <ATen/ops/exp2_native.h>
#include <ATen/ops/exp_native.h>
#include <ATen/ops/expm1.h>
#include <ATen/ops/expm1_native.h>
#include <ATen/ops/fix_native.h>
#include <ATen/ops/floor_native.h>
#include <ATen/ops/frac_native.h>
#include <ATen/ops/frexp.h>
#include <ATen/ops/frexp_native.h>
#include <ATen/ops/i0.h>
#include <ATen/ops/i0_native.h>
#include <ATen/ops/imag_native.h>
#include <ATen/ops/lgamma.h>
#include <ATen/ops/lgamma_native.h>
#include <ATen/ops/log10_native.h>
#include <ATen/ops/log1p.h>
#include <ATen/ops/log1p_native.h>
#include <ATen/ops/log2_native.h>
#include <ATen/ops/log_native.h>
#include <ATen/ops/logical_not.h>
#include <ATen/ops/logical_not_native.h>
#include <ATen/ops/logit.h>
#include <ATen/ops/logit_native.h>
#include <ATen/ops/mul.h>
#include <ATen/ops/mvlgamma.h>
#include <ATen/ops/mvlgamma_native.h>
#include <ATen/ops/nan_to_num.h>
#include <ATen/ops/nan_to_num_native.h>
#include <ATen/ops/neg.h>
#include <ATen/ops/neg_native.h>
#include <ATen/ops/negative_native.h>
#include <ATen/ops/polygamma.h>
#include <ATen/ops/polygamma_native.h>
#include <ATen/ops/positive_native.h>
#include <ATen/ops/pow.h>
#include <ATen/ops/rad2deg.h>
#include <ATen/ops/rad2deg_native.h>
#include <ATen/ops/real.h>
#include <ATen/ops/real_native.h>
#include <ATen/ops/reciprocal_native.h>
#include <ATen/ops/resolve_conj_native.h>
#include <ATen/ops/resolve_neg_native.h>
#include <ATen/ops/round.h>
#include <ATen/ops/round_native.h>
#include <ATen/ops/rsqrt_native.h>
#include <ATen/ops/select.h>
#include <ATen/ops/sgn_native.h>
#include <ATen/ops/sigmoid.h>
#include <ATen/ops/sigmoid_native.h>
#include <ATen/ops/sign_native.h>
#include <ATen/ops/signbit_native.h>
#include <ATen/ops/sin_native.h>
#include <ATen/ops/sinc.h>
#include <ATen/ops/sinc_native.h>
#include <ATen/ops/sinh_native.h>
#include <ATen/ops/special_airy_ai_native.h>
#include <ATen/ops/special_bessel_j0_native.h>
#include <ATen/ops/special_bessel_j1_native.h>
#include <ATen/ops/special_bessel_y0_native.h>
#include <ATen/ops/special_bessel_y1_native.h>
#include <ATen/ops/special_digamma_native.h>
#include <ATen/ops/special_entr_native.h>
#include <ATen/ops/special_erf_native.h>
#include <ATen/ops/special_erfc_native.h>
#include <ATen/ops/special_erfcx_native.h>
#include <ATen/ops/special_erfinv_native.h>
#include <ATen/ops/special_exp2_native.h>
#include <ATen/ops/special_expit_native.h>
#include <ATen/ops/special_expm1_native.h>
#include <ATen/ops/special_gammaln_native.h>
#include <ATen/ops/special_i0_native.h>
#include <ATen/ops/special_i0e_native.h>
#include <ATen/ops/special_i1_native.h>
#include <ATen/ops/special_i1e_native.h>
#include <ATen/ops/special_log1p_native.h>
#include <ATen/ops/special_log_ndtr_native.h>
#include <ATen/ops/special_logit_native.h>
#include <ATen/ops/special_modified_bessel_i0_native.h>
#include <ATen/ops/special_modified_bessel_i1_native.h>
#include <ATen/ops/special_modified_bessel_k0_native.h>
#include <ATen/ops/special_modified_bessel_k1_native.h>
#include <ATen/ops/special_multigammaln_native.h>
#include <ATen/ops/special_ndtr_native.h>
#include <ATen/ops/special_ndtri_native.h>
#include <ATen/ops/special_polygamma_native.h>
#include <ATen/ops/special_psi_native.h>
#include <ATen/ops/special_round_native.h>
#include <ATen/ops/special_scaled_modified_bessel_k0_native.h>
#include <ATen/ops/special_scaled_modified_bessel_k1_native.h>
#include <ATen/ops/special_sinc_native.h>
#include <ATen/ops/special_spherical_bessel_j0_native.h>
#include <ATen/ops/sqrt_native.h>
#include <ATen/ops/square_native.h>
#include <ATen/ops/tan_native.h>
#include <ATen/ops/tanh_native.h>
#include <ATen/ops/trunc.h>
#include <ATen/ops/trunc_native.h>
#include <ATen/ops/view_as_real.h>
#endif

#include <cmath>

#include <ATen/host/kernel/UnaryTrigKernel.cpp>
#include <ATen/host/kernel/UnaryHypTrigKernel.cpp>
#include <ATen/host/kernel/UnaryArcTrigKernel.cpp>
#include <ATen/host/kernel/UnaryPowKernel.cpp>
#include <ATen/host/kernel/UnaryLogKernel.cpp>
#include <ATen/host/kernel/UnaryExpKernel.cpp>
#include <ATen/host/kernel/UnaryRoundKernel.cpp>
#include <ATen/host/kernel/UnaryComplexKernel.cpp>
#include <ATen/host/kernel/UnaryClampKernel.cpp>

namespace at {
namespace fpga {

// NOTE: These are helper functions that reduce redundant code in implementing the most typical kind of unary operators.
// YOU ARE NOT OBLIGED TO USE THESE HELPERS---if you're writing something more specialized, please don't try to make
// them work for your case, but just write something new instead. Here we use helper functions instead of a flat fat
// macro that implements everything, because the former allows some simple preprocessing that are unique to some
// operators (more is foreseeable) and is more flexible and elegant than the latter.
template <typename Stub>
static inline Tensor& unary_op_impl_out(Tensor& result, const Tensor& self, Stub& stub) {
  auto iter = TensorIterator::unary_op(result, self);
  stub(iter);
  return result;
}

// An alternate version of unary_op_impl_out that follows the same pattern
// for non-complex inputs, but returns a floating point tensor
// for complex inputs by default.
// Note: This is done by running the operation as usual and then copying the
// operation's result to the expected result type.
template <typename Stub>
static inline Tensor& unary_op_impl_with_complex_to_float_out(Tensor& result, const Tensor& self, Stub& stub) {
    if (self.is_complex() && !result.is_complex()) {
      // Checks if the corresponding float type can be cast to the desired dtype
      const auto float_type = c10::toRealValueType(self.scalar_type());
      TORCH_CHECK(canCast(float_type, result.scalar_type()),
            "result type ", float_type, " can't be cast to the desired output type ",
            result.scalar_type());

      // Runs the function complex->complex, as TensorIterator expects
      Tensor complex_result = at::empty({0}, self.options());
      auto iter = TensorIterator::unary_op(complex_result, self);
      stub(iter);

      // Copies the complex result to the actual result and returns it
      result.resize_(complex_result.sizes());
      result.copy_(complex_result);
      return result;
    }

    return unary_op_impl_out(result, self, stub);
}

// out_impl passed into unary_op_impl and unary_op_impl_  must go through at:: device dispatch
// otherwise it won't dispatch to out-of-source devices like XLA.
// For example it must be at::bitwise_not_out instead of bitwise_not_out(which is at::native!).
template <typename OutImpl>
static inline Tensor unary_op_impl(const Tensor& self, OutImpl& out_impl) {
  Tensor result = at::empty({0}, self.options());
  return out_impl(result, self);
}

// An alternate version of unary_op_impl that follows the same pattern
// for non-complex inputs, but returns a floating point tensor
// for complex inputs by default.
template <typename OutImpl>
static inline Tensor unary_op_impl_with_complex_to_float(const Tensor& self, OutImpl& out_impl) {
  if (self.is_complex()) {
    const auto float_type = c10::toRealValueType(self.scalar_type());
    Tensor result = at::empty({0}, self.options().dtype(float_type));
    return out_impl(result, self);
  }

  Tensor result = at::empty({0}, self.options());
  return out_impl(result, self);
}

template <typename OutImpl>
static inline Tensor& unary_op_impl_(Tensor& self, OutImpl& out_impl) {
  return out_impl(self, self);
}

TORCH_API at::Tensor & acos_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, acos_stub); }

TORCH_API at::Tensor & asin_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, asin_stub); }

TORCH_API at::Tensor & atan_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, atan_stub); }

// Note [Complex abs and angle]
// Complex inputs to abs and angle return float results by default.
// abs and angle, in both NumPy and C++, returns a float result when given a
// complex input. This makes sense mathematically since the absolute value
// and angle of a complex number has no imaginary part.
TORCH_API at::Tensor & abs_out(at::Tensor & out, const at::Tensor & self) {
  return unary_op_impl_with_complex_to_float_out(out, self, abs_stub);
}

TORCH_API at::Tensor & angle_out(at::Tensor & out, const at::Tensor & self) {
  return unary_op_impl_with_complex_to_float_out(out, self, angle_stub);
}

TORCH_API at::Tensor angle(const at::Tensor & self) {
  return unary_op_impl_with_complex_to_float(self, at::angle_out);
}

TORCH_API at::Tensor & conj_physical_out(TORCH_API at::Tensor & out, const at::Tensor & self) {
  return unary_op_impl_out(out, self, conj_stub);
}

Tensor conj_physical(const Tensor& self) {
  if (!self.is_complex()) return self;
  return at::_conj_physical(self);
}

Tensor& conj_physical_(Tensor& self) {
  if (!self.is_complex()) return self;
  return unary_op_impl_out(self, self, conj_stub);
}

TORCH_API at::Tensor & ceil_out(at::Tensor & out, const at::Tensor & self) {
  // Note: this is consistent with NumPy
  TORCH_CHECK(!self.is_complex(),
    "ceil is not supported for complex inputs");

  return unary_op_impl_out(out, self, ceil_stub);
}

Tensor& exp_out(Tensor& result, const Tensor& self) { return unary_op_impl_out(result, self, exp_stub); }

Tensor& expm1_out(Tensor& result, const Tensor& self) { return unary_op_impl_out(result, self, expm1_stub); }

Tensor& erf_out(Tensor& result, const Tensor& self) { return unary_op_impl_out(result, self, erf_stub); }
Tensor erf(const Tensor& self) { return unary_op_impl(self, at::erf_out); }
Tensor& erf_(Tensor& self) { return unary_op_impl_(self, at::erf_out); }

Tensor& erfc_out(Tensor& result, const Tensor& self) { return unary_op_impl_out(result, self, erfc_stub); }
Tensor erfc(const Tensor& self) { return unary_op_impl(self, at::erfc_out); }
Tensor& erfc_(Tensor& self) { return unary_op_impl_(self, at::erfc_out); }

TORCH_API at::Tensor & floor_out(at::Tensor & out, const at::Tensor & self) {
  // Note: this is consistent with NumPy
  TORCH_CHECK(!self.is_complex(),
    "floor is not supported for complex inputs");

  return unary_op_impl_out(out, self, floor_stub);
}

TORCH_API at::Tensor & log_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, log_stub); }

TORCH_API at::Tensor & log10_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, log10_stub); }

TORCH_API at::Tensor & log1p_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, log1p_stub); }

TORCH_API at::Tensor & log2_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, log2_stub); }


TORCH_API at::Tensor & round_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, round_stub); }

TORCH_API at::Tensor & reciprocal_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, reciprocal_stub); }

TORCH_API at::Tensor & rsqrt_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, rsqrt_stub); }

TORCH_API at::Tensor & sin_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, sin_stub); }
TORCH_API at::Tensor sin(const at::Tensor & self) { return unary_op_impl(self, at::sin_out); }

TORCH_API at::Tensor & cos_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, cos_stub); }
TORCH_API at::Tensor cos(const at::Tensor & self) { return unary_op_impl(self, at::cos_out); }

TORCH_API at::Tensor & sinh_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, sinh_stub); }

TORCH_API at::Tensor & cosh_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, cosh_stub); }

TORCH_API at::Tensor & sqrt_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, sqrt_stub); }

Tensor square(const Tensor& self) { return at::pow(self, 2); }
Tensor& square_(Tensor& self) { return at::pow_out(self, self, 2); }

TORCH_API at::Tensor & tanh_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, tanh_stub); }

TORCH_API at::Tensor & tan_out(at::Tensor & out, const at::Tensor & self) { return unary_op_impl_out(out, self, tan_stub);  }
TORCH_API at::Tensor tan(const at::Tensor & self) { return unary_op_impl(self, at::tan_out);  }

TORCH_API at::Tensor & trunc_out(at::Tensor & out, const at::Tensor & self) {
  // Note: this is consistent with NumPy
  TORCH_CHECK(!self.is_complex(),
    "trunc is not supported for complex inputs");

  return unary_op_impl_out(out, self, trunc_stub);
}

TORCH_API at::Tensor & clamp_out(at::Tensor & out, const at::Tensor & self, const ::std::optional<at::Scalar> & min, const ::std::optional<at::Scalar> & max) {
  TORCH_CHECK(!self.is_complex(), "clamp is not yet implemented for complex tensors.");
  if (min && max) {
    TORCH_CHECK(self.layout() == Layout::Strided,
                "clamp only supports strided layout, got: ", self.layout());
    auto iter = TensorIterator::unary_op(out, self);
    clamp_stub(iter, *min, *max);
  } else if (max) {
    at::clamp_max_out(out, self, *max);
  } else if (min) {
    at::clamp_min_out(out, self, *min);
  } else {
    AT_ERROR("At least one of 'min' or 'max' must not be None");
  }
  return out;
}

TORCH_API at::Tensor & clamp_max_out(at::Tensor & out, const at::Tensor & self, const at::Scalar & max) {
  TORCH_CHECK(!self.is_complex(), "clamp is not yet implemented for complex tensors.");
  TORCH_CHECK(self.layout() == Layout::Strided,
              "clamp_max only supports strided layout, got: ", self.layout());
  auto iter = TensorIterator::unary_op(out, self);
  clamp_max_stub(iter, max);
  return out;
}

TORCH_API at::Tensor & clamp_min_out(at::Tensor & out, const at::Tensor & self, const at::Scalar & min) {
  TORCH_CHECK(!self.is_complex(), "clamp is not yet implemented for complex tensors.");
  TORCH_CHECK(self.layout() == Layout::Strided,
              "clamp_min only supports strided layout, got: ", self.layout());
  auto iter = TensorIterator::unary_op(out, self);
  clamp_min_stub(iter, min);
  return out;
}

}
} // namespace at
