#include <ATen/native/BinaryOps.h>

#include <type_traits>
#include <utility>

#include <ATen/core/Tensor.h>
#include <ATen/ScalarOps.h>
#include <ATen/TensorIterator.h>
#include <ATen/TensorOperators.h>
#include <ATen/TensorMeta.h>

#ifndef AT_PER_OPERATOR_HEADERS
#include <ATen/Functions.h>
#include <ATen/NativeFunctions.h>
#else
#include <ATen/ops/_add_relu_native.h>
#include <ATen/ops/_efficientzerotensor.h>
#include <ATen/ops/_test_serialization_subcmul_native.h>
#include <ATen/ops/_to_copy.h>
#include <ATen/ops/add.h>
#include <ATen/ops/add_native.h>
#include <ATen/ops/add_ops.h>
#include <ATen/ops/and_native.h>
#include <ATen/ops/arctan2_native.h>
#include <ATen/ops/atan2.h>
#include <ATen/ops/atan2_native.h>
#include <ATen/ops/bitwise_and.h>
#include <ATen/ops/bitwise_and_native.h>
#include <ATen/ops/bitwise_left_shift.h>
#include <ATen/ops/bitwise_left_shift_native.h>
#include <ATen/ops/bitwise_or.h>
#include <ATen/ops/bitwise_or_native.h>
#include <ATen/ops/bitwise_right_shift.h>
#include <ATen/ops/bitwise_right_shift_native.h>
#include <ATen/ops/bitwise_xor.h>
#include <ATen/ops/bitwise_xor_native.h>
#include <ATen/ops/copysign.h>
#include <ATen/ops/copysign_native.h>
#include <ATen/ops/div.h>
#include <ATen/ops/div_native.h>
#include <ATen/ops/div_ops.h>
#include <ATen/ops/divide_native.h>
#include <ATen/ops/empty.h>
#include <ATen/ops/eq_native.h>
#include <ATen/ops/floor_divide.h>
#include <ATen/ops/floor_divide_native.h>
#include <ATen/ops/fmax_native.h>
#include <ATen/ops/fmin_native.h>
#include <ATen/ops/fmod.h>
#include <ATen/ops/fmod_native.h>
#include <ATen/ops/full.h>
#include <ATen/ops/gcd_native.h>
#include <ATen/ops/ge.h>
#include <ATen/ops/ge_native.h>
#include <ATen/ops/greater_equal_native.h>
#include <ATen/ops/greater_native.h>
#include <ATen/ops/gt.h>
#include <ATen/ops/gt_native.h>
#include <ATen/ops/heaviside_native.h>
#include <ATen/ops/hypot_native.h>
#include <ATen/ops/igamma.h>
#include <ATen/ops/igamma_native.h>
#include <ATen/ops/igammac.h>
#include <ATen/ops/igammac_native.h>
#include <ATen/ops/lcm_native.h>
#include <ATen/ops/ldexp.h>
#include <ATen/ops/ldexp_native.h>
#include <ATen/ops/le.h>
#include <ATen/ops/le_native.h>
#include <ATen/ops/less_equal_native.h>
#include <ATen/ops/less_native.h>
#include <ATen/ops/linalg_cross_native.h>
#include <ATen/ops/linalg_cross_ops.h>
#include <ATen/ops/logaddexp2_native.h>
#include <ATen/ops/logaddexp_native.h>
#include <ATen/ops/logical_and.h>
#include <ATen/ops/logical_and_native.h>
#include <ATen/ops/logical_or.h>
#include <ATen/ops/logical_or_native.h>
#include <ATen/ops/logical_xor.h>
#include <ATen/ops/logical_xor_native.h>
#include <ATen/ops/logit_backward_native.h>
#include <ATen/ops/lshift_native.h>
#include <ATen/ops/lt.h>
#include <ATen/ops/lt_native.h>
#include <ATen/ops/max_native.h>
#include <ATen/ops/maximum.h>
#include <ATen/ops/maximum_native.h>
#include <ATen/ops/min_native.h>
#include <ATen/ops/minimum.h>
#include <ATen/ops/minimum_native.h>
#include <ATen/ops/mul.h>
#include <ATen/ops/mul_native.h>
#include <ATen/ops/mul_ops.h>
#include <ATen/ops/multiply_native.h>
#include <ATen/ops/ne.h>
#include <ATen/ops/ne_native.h>
#include <ATen/ops/nextafter_native.h>
#include <ATen/ops/not_equal_native.h>
#include <ATen/ops/or_native.h>
#include <ATen/ops/pow.h>
#include <ATen/ops/remainder.h>
#include <ATen/ops/remainder_native.h>
#include <ATen/ops/rshift_native.h>
#include <ATen/ops/rsub_native.h>
#include <ATen/ops/sigmoid_backward_native.h>
#include <ATen/ops/special_chebyshev_polynomial_t.h>
#include <ATen/ops/special_chebyshev_polynomial_t_native.h>
#include <ATen/ops/special_chebyshev_polynomial_u.h>
#include <ATen/ops/special_chebyshev_polynomial_u_native.h>
#include <ATen/ops/special_chebyshev_polynomial_v.h>
#include <ATen/ops/special_chebyshev_polynomial_v_native.h>
#include <ATen/ops/special_chebyshev_polynomial_w.h>
#include <ATen/ops/special_chebyshev_polynomial_w_native.h>
#include <ATen/ops/special_gammainc_native.h>
#include <ATen/ops/special_gammaincc_native.h>
#include <ATen/ops/special_hermite_polynomial_h.h>
#include <ATen/ops/special_hermite_polynomial_h_native.h>
#include <ATen/ops/special_hermite_polynomial_he.h>
#include <ATen/ops/special_hermite_polynomial_he_native.h>
#include <ATen/ops/special_laguerre_polynomial_l.h>
#include <ATen/ops/special_laguerre_polynomial_l_native.h>
#include <ATen/ops/special_legendre_polynomial_p.h>
#include <ATen/ops/special_legendre_polynomial_p_native.h>
#include <ATen/ops/special_shifted_chebyshev_polynomial_t.h>
#include <ATen/ops/special_shifted_chebyshev_polynomial_t_native.h>
#include <ATen/ops/special_shifted_chebyshev_polynomial_u.h>
#include <ATen/ops/special_shifted_chebyshev_polynomial_u_native.h>
#include <ATen/ops/special_shifted_chebyshev_polynomial_v.h>
#include <ATen/ops/special_shifted_chebyshev_polynomial_v_native.h>
#include <ATen/ops/special_shifted_chebyshev_polynomial_w.h>
#include <ATen/ops/special_shifted_chebyshev_polynomial_w_native.h>
#include <ATen/ops/special_xlog1py.h>
#include <ATen/ops/special_xlog1py_native.h>
#include <ATen/ops/special_xlogy_native.h>
#include <ATen/ops/special_zeta.h>
#include <ATen/ops/special_zeta_native.h>
#include <ATen/ops/sub.h>
#include <ATen/ops/sub_native.h>
#include <ATen/ops/subtract_native.h>
#include <ATen/ops/tanh_backward_native.h>
#include <ATen/ops/true_divide_native.h>
#include <ATen/ops/xlogy.h>
#include <ATen/ops/xlogy_native.h>
#include <ATen/ops/xor_native.h>
#endif

#include <ATen/host/kernel/BinaryArithmeticKernel.cpp>
#include <ATen/host/kernel/BinaryEqualsKernel.cpp>
#include <ATen/host/kernel/BinaryCompareKernel.cpp>

namespace at {
namespace fpga {

void alpha_check(const ScalarType dtype, Scalar alpha) {
  TORCH_CHECK(! alpha.isBoolean() || dtype == ScalarType::Bool,
              "Boolean alpha only supported for Boolean results.");
  TORCH_CHECK(isFloatingType(dtype) || alpha.isIntegral(true),
              "For integral input tensors, argument alpha must not be a floating point number.");
}

// Basic checking for all sub functions.
void sub_check(const Tensor& self, const Tensor& other) {
  TORCH_CHECK(self.scalar_type() != kBool || other.scalar_type() != kBool,
              "Subtraction, the `-` operator, with two bool tensors is not supported. "
              "Use the `^` or `logical_xor()` operator instead.")
  TORCH_CHECK(self.scalar_type() != kBool && other.scalar_type() != kBool,
              "Subtraction, the `-` operator, with a bool tensor is not supported. "
              "If you are trying to invert a mask, use the `~` or `logical_not()` operator instead.");
}

TORCH_API at::Tensor & add_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha) {
  auto iter = TensorIterator::binary_op(out, self, other);
  alpha_check(iter.dtype(), alpha);
  add_stub(iter, alpha);
  TORCH_INTERNAL_ASSERT(out.scalar_type() == iter.output().dtype());
  return out;
}

TORCH_API at::Tensor add(const at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha) {
  Tensor out;
  auto iter = TensorIterator::binary_op(out, self, other);
  alpha_check(iter.dtype(), alpha);
  add_stub(iter, alpha);
  return iter.output();
}

TORCH_API at::Tensor & add_(at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha) {
  return at::fpga::add_out(self, self, other, alpha);
}

TORCH_API at::Tensor & div_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other) {
  if (isIntegralType(out.scalar_type(), /*includeBool=*/ true)) {
    TORCH_WARN_ONCE(
      "Integer division of tensors using div or / is deprecated, ",
      "and in a future release div will perform true division as in Python 3. ",
      "Use true_divide or floor_divide (// in Python) instead.");
  }

  auto iter = TensorIterator::binary_op(out, self, other);
  div_stub(iter);
  return out;
}

TORCH_API at::Tensor div(const at::Tensor & self, const at::Tensor & other) {
  if (isIntegralType(self.scalar_type(), /*includeBool=*/ true)
      && isIntegralType(other.scalar_type(), /*includeBool=*/ true)) {
    TORCH_WARN_ONCE(
      "Integer division of tensors using div or / is deprecated, ",
      "and in a future release div will perform true division as in Python 3. ",
      "Use true_divide or floor_divide (// in Python) instead.");
  }
  Tensor out;
  auto iter = TensorIterator::binary_op(out, self, other);
  div_stub(iter);
  return iter.output();
}

TORCH_API at::Tensor & div_(at::Tensor & self, const at::Tensor & other) {
  return at::fpga::div_out(self, self, other);
}

TORCH_API at::Tensor & mul_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other) {
  auto iter = TensorIterator::binary_op(out, self, other);
  mul_stub(iter);
  return out;
}

TORCH_API at::Tensor mul(const at::Tensor & self, const at::Tensor & other) {
  Tensor out;
  auto iter = TensorIterator::binary_op(out, self, other);
  mul_stub(iter);
  return iter.output();
}

TORCH_API at::Tensor & mul_(at::Tensor & self, const at::Tensor & other) {
  return at::fpga::mul_out(self, self, other);
}

TORCH_API at::Tensor & sub_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha) {
  sub_check(self, other);
  auto iter = TensorIterator::binary_op(out, self, other);
  alpha_check(iter.dtype(), alpha);
  sub_stub(iter, alpha);
  TORCH_INTERNAL_ASSERT(out.scalar_type() == iter.output().dtype());
  return out;
}

TORCH_API at::Tensor sub(const at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha) {
  sub_check(self, other);
  Tensor out;
  auto iter = TensorIterator::binary_op(out, self, other);
  alpha_check(iter.dtype(), alpha);
  sub_stub(iter, alpha);
  return iter.output();
}

TORCH_API at::Tensor & sub_(at::Tensor & self, const at::Tensor & other, const at::Scalar & alpha) {
  return at::fpga::sub_out(self, self, other, alpha);
}

// These are still needed because we don't have C++ conversions from number
// types (int, float, etc.) to Tensor (only to Scalar). They're not exposed
// to Python.

static Tensor wrapped_scalar_tensor(Scalar scalar) {
  auto tensor = scalar_to_tensor(scalar);
  tensor.unsafeGetTensorImpl()->set_wrapped_number(true);
  return tensor;
}

static void check_convert(Scalar scalar, ScalarType scalarType) {
  // Validate that is possible to convert scalar to tensor dtype without overflow
  AT_DISPATCH_ALL_TYPES_AND_COMPLEX_AND3(at::ScalarType::Bool, at::ScalarType::BFloat16, at::ScalarType::Half, scalarType, "check_convert", [&]{
    scalar.to<scalar_t>();
  });
}

static Tensor wrapped_scalar_tensor_and_check_convert(Scalar scalar, Tensor tensor) {
  check_convert(scalar, tensor.scalar_type());
  return wrapped_scalar_tensor(scalar);
}

Tensor add(const Tensor& self, Scalar other, Scalar alpha) {
  return at::fpga::add(self, wrapped_scalar_tensor(other), alpha);
}

Tensor& add_(Tensor& self, Scalar other, Scalar alpha) {
  return at::fpga::add_(self, wrapped_scalar_tensor(other), alpha);
}

// WARNING: There doesn't appear to be any testing for this function
// with sparse self input.
Tensor div(const Tensor& self, Scalar other) {
  return self.div(wrapped_scalar_tensor(other)); // redispatch!
}

// WARNING: This function, with a sparse self, is currently only
// exercised by DistributedDataParallelTest.test_sparse_gradients
// (you need to exercise it from C++, because this overload is never
// used for Python)
Tensor& div_(Tensor& self, Scalar other) {
  return self.div_(wrapped_scalar_tensor(other)); // redispatch!
}

Tensor mul(const Tensor& self, Scalar other) {
  return at::fpga::mul(self, wrapped_scalar_tensor(other));
}

Tensor& mul_(Tensor& self, Scalar other) {
  return at::fpga::mul_(self, wrapped_scalar_tensor(other));
}

Tensor sub(const Tensor& self, Scalar other, Scalar alpha) {
  return at::fpga::sub(self, wrapped_scalar_tensor(other), alpha);
}

Tensor& sub_(Tensor& self, Scalar other, Scalar alpha) {
  return at::fpga::sub_(self, wrapped_scalar_tensor(other), alpha);
}

template <typename Stub>
Tensor& comparison_op_out(Tensor& result, const Tensor& self, const Tensor& other, Stub& stub) {
  // Validate that is possible to convert zero-dim tensor's dtype to other dtype without overflow
  if (self.scalar_type() != other.scalar_type()) {
    if (self.dim() != 0 && other.dim() == 0) {
      check_convert(other.item(), self.scalar_type());
    } else if (self.dim() == 0 && other.dim() != 0) {
      check_convert(self.item(), other.scalar_type());
    }
  }
  auto iter = TensorIterator::comparison_op(result, self, other);
  stub(iter);
  return result;
}

template <typename OutImpl>
Tensor comparison_op(const Tensor& self, const Tensor& other, OutImpl& out_impl) {
  Tensor result = at::empty({0}, self.options().dtype(kBool));
  return out_impl(result, self, other);
}

// To avoid overflow during type promotion we will check that both dtypes of self and other are same
template <typename OutImpl>
Tensor& comparison_op_(Tensor& self, const Tensor& other, OutImpl& out_impl) {
  TORCH_CHECK(self.dtype() == other.dtype(),
              "Expected object of scalar type ", self.dtype(), " but got scalar type ",
              other.dtype(), " for argument 'other'");
  return out_impl(self, self, other);
}

// validates that is possible to convert Scalar other to self's dtype without overflow.
// This behavior is unique to comparison ops; arithmetic operations don't do this.
// In the future, we should reconsider this inconsistency and decide if we want to add the same check to arithmetic ops.
template <typename OutImpl>
Tensor& comparison_op_out(Tensor& result, const Tensor& self, Scalar other, OutImpl& out_impl) {
  return out_impl(result, self, wrapped_scalar_tensor_and_check_convert(other, self));
}

template <typename OutImpl>
Tensor comparison_op(const Tensor& self, Scalar other, OutImpl& out_impl) {
  return comparison_op(self, wrapped_scalar_tensor_and_check_convert(other, self), out_impl);
}

template <typename OutImpl>
Tensor& comparison_op_(Tensor& self, Scalar other, OutImpl& out_impl) {
  return out_impl(self, self, wrapped_scalar_tensor_and_check_convert(other, self));
}

// We need explicit cast to OutFunc because each *_out func is overloaded twice. Without An explicit cast, merely
// referring to *_out function is ambiguious.
using OutFunc = std::add_const<Tensor&(&)(Tensor&, const Tensor&, const Tensor&)>::type;

TORCH_API at::Tensor & lt_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other) { return comparison_op_out(out, self, other, lt_stub); }
TORCH_API at::Tensor lt(const at::Tensor & self, const at::Tensor & other) { return comparison_op(self, other, static_cast<OutFunc>(at::lt_out)); }
TORCH_API at::Tensor & lt_out(at::Tensor & out, const at::Tensor & self, const at::Scalar & other) { return comparison_op_out(out, self, other, static_cast<OutFunc>(at::lt_out)); }
TORCH_API at::Tensor lt(const at::Tensor & self, const at::Scalar & other) { return comparison_op(self, other, static_cast<OutFunc>(at::lt_out)); }

TORCH_API at::Tensor & le_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other) { return comparison_op_out(out, self, other, le_stub); }
TORCH_API at::Tensor le(const at::Tensor & self, const at::Tensor & other) { return comparison_op(self, other, static_cast<OutFunc>(at::le_out)); }
TORCH_API at::Tensor & le_out(at::Tensor & out, const at::Tensor & self, const at::Scalar & other) { return comparison_op_out(out, self, other, static_cast<OutFunc>(at::le_out)); }
TORCH_API at::Tensor le(const at::Tensor & self, const at::Scalar & other) { return comparison_op(self, other, static_cast<OutFunc>(at::le_out)); }

TORCH_API at::Tensor & gt_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other) { return comparison_op_out(out, self, other, gt_stub); }
TORCH_API at::Tensor gt(const at::Tensor & self, const at::Tensor & other) { return comparison_op(self, other, static_cast<OutFunc>(at::gt_out)); }
TORCH_API at::Tensor & gt_out(at::Tensor & out, const at::Tensor & self, const at::Scalar & other) { return comparison_op_out(out, self, other, static_cast<OutFunc>(at::gt_out)); }
TORCH_API at::Tensor gt(const at::Tensor & self, const at::Scalar & other) { return comparison_op(self, other, static_cast<OutFunc>(at::gt_out)); }

TORCH_API at::Tensor & ge_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other) { return comparison_op_out(out, self, other, ge_stub); }
TORCH_API at::Tensor ge(const at::Tensor & self, const at::Tensor & other) { return comparison_op(self, other, static_cast<OutFunc>(at::ge_out)); }
TORCH_API at::Tensor & ge_out(at::Tensor & out, const at::Tensor & self, const at::Scalar & other) { return comparison_op_out(out, self, other, static_cast<OutFunc>(at::ge_out)); }
TORCH_API at::Tensor ge(const at::Tensor & self, const at::Scalar & other) { return comparison_op(self, other, static_cast<OutFunc>(at::ge_out)); }

TORCH_API at::Tensor & eq_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other) { return comparison_op_out(out, self, other, eq_stub); }
TORCH_API at::Tensor eq(const at::Tensor & self, const at::Tensor & other) { return comparison_op(self, other, static_cast<OutFunc>(at::eq_out)); }
TORCH_API at::Tensor & eq_out(at::Tensor & out, const at::Tensor & self, const at::Scalar & other) { return comparison_op_out(out, self, other, static_cast<OutFunc>(at::eq_out)); }
TORCH_API at::Tensor eq(const at::Tensor & self, const at::Scalar & other) { return comparison_op(self, other, static_cast<OutFunc>(at::eq_out)); }

TORCH_API at::Tensor & ne_out(at::Tensor & out, const at::Tensor & self, const at::Tensor & other) { return comparison_op_out(out, self, other, ne_stub); }
TORCH_API at::Tensor ne(const at::Tensor & self, const at::Tensor & other)  { return comparison_op(self, other, static_cast<OutFunc>(at::ne_out)); }
TORCH_API at::Tensor & ne_out(at::Tensor & out, const at::Tensor & self, const at::Scalar & other) { return comparison_op_out(out, self, other, static_cast<OutFunc>(at::ne_out)); }
TORCH_API at::Tensor ne(const at::Tensor & self, const at::Scalar & other) { return comparison_op(self, other, static_cast<OutFunc>(at::ne_out)); }

}}  // at::fpga
