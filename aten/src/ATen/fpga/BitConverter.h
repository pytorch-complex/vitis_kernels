#pragma once

#include <ap_int.h>

//  Bit converter
template <typename T>
class BitConv {
   public:
    static const unsigned int t_SizeOf = sizeof(T);
    static const unsigned int t_NumBits = 8 * sizeof(T);
    typedef ap_uint<t_NumBits> BitsType;

   public:
    BitsType toBits(T p_Val) { return p_Val; }
    T toType(BitsType p_Val) { return p_Val; }
};

template <>
inline BitConv<float>::BitsType BitConv<float>::toBits(float p_Val) {
    union {
        float f;
        unsigned int i;
    } u;
    u.f = p_Val;
    return (u.i);
}

template <>
inline float BitConv<float>::toType(BitConv<float>::BitsType p_Val) {
    union {
        float f;
        unsigned int i;
    } u;
    u.i = p_Val;
    return (u.f);
}

template <>
inline BitConv<double>::BitsType BitConv<double>::toBits(double p_Val) {
    union {
        double f;
        int64_t i;
    } u;
    u.f = p_Val;
    return (u.i);
}

template <>
inline double BitConv<double>::toType(BitConv<double>::BitsType p_Val) {
    union {
        double f;
        int64_t i;
    } u;
    u.i = p_Val;
    return (u.f);
}
