#pragma once

#include <cassert>
#include <cstddef>
#include <iostream>

#include <ap_int.h>
#include <hls_stream.h>
#define __HLS_VEC__  // This flag enables kernel specific methods in at::vec::Vec
#include <ATen/fpga/vec/vec_complex.h>

namespace at { namespace vec {

template <typename T>
void read_dataflow(hls::stream<T>& a_stream,
                   const ap_uint<SIZE_BITS>* a,
                   size_t stride,
                   size_t offset) {
  ap_uint<SIZE_BITS> a_dat;
  BitConv<T> conv;
  T a_val;
  read: for (size_t j = 0; j < stride; j++) {
    #pragma HLS PIPELINE II=1
     a_dat = a[j + offset];
     a_val = conv.toType(a_dat(SIZE_BITS - 1, 0));
     a_stream << a_val;
  }
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void read_dataflow(hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> >& a_stream,
                   const ap_uint<PAR_SIZE_BITS>* a,
                   size_t stride,
                   size_t offset) {
  const size_t par_stride = (stride << Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::rn_log_size()) >> Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_log_size();
  ap_uint<PAR_SIZE_BITS> a_dat;
  BitConv<T> conv;
  Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> a_vec;
  read: for (size_t j = 0; j < par_stride; j++) {
    #pragma HLS PIPELINE II=1
     a_dat = a[j + offset];
    for (size_t k = 0; k < PAR_SIZE; k++) {
      a_vec[k] = conv.toType(a_dat(SIZE_BITS*k + SIZE_BITS - 1, SIZE_BITS*k));
    }
    a_stream << a_vec;
  }
}

template <typename T>
void write_dataflow(ap_uint<SIZE_BITS> *a,
                    hls::stream<T>& a_stream,
                    size_t stride,
                    size_t offset) {
    BitConv<T> conv;
    T a_val;
    ap_uint<SIZE_BITS> a_dat;
    write: for (size_t j = 0; j < stride; j++) {
      #pragma HLS PIPELINE II=1
      a_stream >> a_val;
      a_dat(SIZE_BITS - 1, 0) = conv.toBits(a_val);
      a[j + offset] = a_dat;
    }
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void write_dataflow(ap_uint<PAR_SIZE_BITS> *a,
                    hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> >& a_stream,
                    size_t stride,
                    size_t offset) {
    const size_t par_stride = (stride << Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::rn_log_size()) >> Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_log_size();
    Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> a_vec;
    BitConv<T> conv;
    ap_uint<PAR_SIZE_BITS> a_dat;
    write: for (size_t j = 0; j < par_stride; j++) {
      #pragma HLS PIPELINE II=1
      a_stream >> a_vec;
      for (size_t k = 0; k < PAR_SIZE; k++) {
        a_dat(SIZE_BITS*k + SIZE_BITS - 1, SIZE_BITS*k) = conv.toBits(a_vec[k]);
      }
      a[j + offset] = a_dat;
    }
}

template <typename DEST_T, typename SRC_T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void copy_dataflow(hls::stream<Vec<DEST_T, PAR_LOG_SIZE, RN_LOG_SIZE> >& result_stream,
                   hls::stream<Vec<SRC_T, PAR_LOG_SIZE, RN_LOG_SIZE> >& a_stream,
                   size_t stride) {
    const size_t par_stride = (stride << Vec<SRC_T, PAR_LOG_SIZE, RN_LOG_SIZE>::rn_log_size()) >> Vec<SRC_T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_log_size();
    Vec<SRC_T, PAR_LOG_SIZE, RN_LOG_SIZE> a_vec;
    Vec<DEST_T, PAR_LOG_SIZE, RN_LOG_SIZE> result_vec;
    inter_type_copy: for (size_t j = 0; j < par_stride; j++) {
      #pragma HLS PIPELINE II=1
      a_stream >> a_vec;
      for (size_t i = 0; i != PAR_SIZE; i++) {
        #pragma HLS UNROLL
        result_vec[i] = DEST_T(a_vec[i]);
      }
      result_stream << result_vec;
    }
}

} } // namespace at::vec
