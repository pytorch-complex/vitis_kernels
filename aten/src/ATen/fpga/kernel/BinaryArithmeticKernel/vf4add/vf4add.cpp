#include <ATen/fpga/kernel/BinaryArithmeticKernel/vadd.hpp>

extern "C" {

using buffer_t = ap_uint<512>;
void vf4add(buffer_t* result, const buffer_t* a, const buffer_t* b, const buffer_t* alpha,
           size_t stride, size_t offset) {
  #pragma HLS INTERFACE s_axilite port=result bundle=control
  #pragma HLS INTERFACE s_axilite port=a bundle=control
  #pragma HLS INTERFACE s_axilite port=b bundle=control
  #pragma HLS INTERFACE s_axilite port=alpha bundle=control
  #pragma HLS INTERFACE s_axilite port=stride bundle=control
  #pragma HLS INTERFACE s_axilite port=offset bundle=control
  #pragma HLS INTERFACE s_axilite port=return bundle=control
  #pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem1
  #pragma HLS INTERFACE m_axi port=a offset=slave bundle=gmem2
  #pragma HLS INTERFACE m_axi port=b offset=slave bundle=gmem3
  #pragma HLS INTERFACE m_axi port=alpha offset=slave bundle=gmem1
  at::vec::r1::vadd<float, 4, 0>(result, a, b, alpha, stride, offset);
}

}
