#include <ATen/fpga/kernel/${CATEGORY_NAME}/${KERNEL_BASENAME}.hpp>

extern "C" {

using dest_buffer_t = ap_uint<${${DTYPE_NAME}_BOOL_PAR_SIZE_BITS}>;
using src_buffer_t = ap_uint<${${DTYPE_NAME}_PAR_SIZE_BITS}>;
void ${KERNEL_NAME}(dest_buffer_t *result, const src_buffer_t *a, const src_buffer_t *b,
                    size_t stride, size_t offset) {
  #pragma HLS INTERFACE s_axilite port=result bundle=control
  #pragma HLS INTERFACE s_axilite port=a bundle=control
  #pragma HLS INTERFACE s_axilite port=b bundle=control
  #pragma HLS INTERFACE s_axilite port=stride bundle=control
  #pragma HLS INTERFACE s_axilite port=offset bundle=control
  #pragma HLS INTERFACE s_axilite port=return bundle=control
  #pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem1
  #pragma HLS INTERFACE m_axi port=a offset=slave bundle=gmem2
  #pragma HLS INTERFACE m_axi port=b offset=slave bundle=gmem3
  at::vec::r${${DTYPE_NAME}_RN}::${KERNEL_BASENAME}<${Bool_T}, ${${DTYPE_NAME}_T}, ${${DTYPE_NAME}_PAR_LOG_SIZE}, ${${DTYPE_NAME}_RN_LOG_SIZE}>(result, a, b, stride, offset);
}

}
