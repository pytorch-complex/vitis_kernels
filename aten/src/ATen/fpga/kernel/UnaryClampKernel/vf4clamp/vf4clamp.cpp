#include <ATen/fpga/kernel/UnaryClampKernel/vclamp.hpp>

extern "C" {

using buffer_t = ap_uint<512>;
void vf4clamp(buffer_t* result, const buffer_t* a, const buffer_t* min, const buffer_t* max,
           size_t stride, size_t offset) {
  #pragma HLS INTERFACE s_axilite port=result bundle=control
  #pragma HLS INTERFACE s_axilite port=a bundle=control
  #pragma HLS INTERFACE s_axilite port=min bundle=control
  #pragma HLS INTERFACE s_axilite port=max bundle=control
  #pragma HLS INTERFACE s_axilite port=stride bundle=control
  #pragma HLS INTERFACE s_axilite port=offset bundle=control
  #pragma HLS INTERFACE s_axilite port=return bundle=control
  #pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem1
  #pragma HLS INTERFACE m_axi port=a offset=slave bundle=gmem2
  #pragma HLS INTERFACE m_axi port=min offset=slave bundle=gmem1
  #pragma HLS INTERFACE m_axi port=max offset=slave bundle=gmem3
  at::vec::r1::vclamp<float, 4, 0>(result, a, min, max, stride, offset);
}

}
