cmake_minimum_required(VERSION 3.10.0)
project(vc4clamp NONE)

# Include
set(ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../../../../../..)
include(${ROOT_DIR}/root.cmake)

# Variables
set(CMAKE_VXX_COMPILER ${XILINX_VITIS}/bin/v++)
set(ATEN_DIR ${ROOT_DIR}/aten/src)
set(SRC_DIR ${CMAKE_CURRENT_SOURCE_DIR}/..)
set(OBJ_FILENAME ${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.xo)

# Build
add_custom_command(OUTPUT ${OBJ_FILENAME}
        COMMAND ${CMAKE_VXX_COMPILER} -c
             -t ${XCL_EMULATION_MODE}
             -k ${PROJECT_NAME}
             -I ${SRC_DIR}
             -I ${ROOT_DIR}
             -I ${ATEN_DIR}
             -I ${ROOT_DIR}/third_party/Vitis_Libraries/dsp/L1/include/hw/vitis_fft/float
             -I ${ROOT_DIR}/third_party/Vitis_Libraries/dsp/L2/include/hw/vitis_fft/float
             -I ${ROOT_DIR}/third_party/Vitis_Libraries/dsp/L1/include/hw
             -o ${OBJ_FILENAME}
             --platform ${XCL_PLATFORM}
             --advanced.prop kernel.${PROJECT_NAME}.kernel_flags=-std=c++0x
             --hls.clock 100000000:${PROJECT_NAME}
             ${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}.cpp
        COMMENT "Compiling target: ${OBJ_FILENAME}"
)

add_custom_target(${PROJECT_NAME} ALL
        DEPENDS ${OBJ_FILENAME}
)
