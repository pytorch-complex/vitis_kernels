#pragma once

#include <ATen/fpga/kernel/Dataflow.h>

namespace at { namespace vec {

template <typename ACC_T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
struct WelfordReduceData {
    Vec<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> mean;
    Vec<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> m2;
    Vec<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> nf;
    Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE> n;
};

template <typename acc_t,
          typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class WelfordReduceOps {
   public:
    using h_op_t = WelfordReduceOps<acc_t, T, PAR_LOG_SIZE - 1, ACC_T, RN_LOG_SIZE>;
    static void reduce(void (*func)(acc_t &, const acc_t &),
                       acc_t &a, const WelfordReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        using h_data_t = WelfordReduceData<T, PAR_LOG_SIZE - 1, RN_LOG_SIZE>;
        acc_t b;
        h_op_t::reduce(func, a, h_data_t{data.mean.beg_half(), data.m2.beg_half(), data.nf.beg_half(), data.n.beg_half()});
        h_op_t::reduce(func, b, h_data_t{data.mean.end_half(), data.m2.end_half(), data.nf.end_half(), data.n.end_half()});
        (*func)(a, b);
    }
    static void combine(void (*func)(acc_t &, const acc_t &),
                        acc_t &a, const WelfordReduceData<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        using h_data_t = WelfordReduceData<ACC_T, PAR_LOG_SIZE - 1, RN_LOG_SIZE>;
        acc_t b;
        h_op_t::combine(func, a, h_data_t{data.mean.beg_half(), data.m2.beg_half(), data.nf.beg_half(), data.n.beg_half()});
        h_op_t::combine(func, b, h_data_t{data.mean.end_half(), data.m2.end_half(), data.nf.end_half(), data.n.end_half()});
        (*func)(a, b);
    }
    static void project(void (*func)(acc_t &),
                        acc_t &a) {
        (*func)(a);
    }
};

template <typename acc_t,
          typename T, typename ACC_T, size_t RN_LOG_SIZE>
class WelfordReduceOps<acc_t, T, RN_LOG_SIZE, ACC_T, RN_LOG_SIZE> {
   public:
    static void reduce(void (*func)(acc_t &, const acc_t &),
                       acc_t &a, const WelfordReduceData<T, RN_LOG_SIZE, RN_LOG_SIZE> &data) {
      #pragma HLS INLINE
      for (size_t i = 0; i < RN_PAR_SIZE; i++) {
        #pragma HLS UNROLL
        a.mean[i] = ACC_T(data.mean[i]);
        a.m2[i] = ACC_T(data.m2[i]);
        a.nf[i] = ACC_T(data.nf[i]);
        a.n[i] = data.n[i];
      }
    }
    static void combine(void (*func)(acc_t &, const acc_t &),
                        acc_t &a, const WelfordReduceData<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE> &data) {
      #pragma HLS INLINE
      for (size_t i = 0; i < RN_PAR_SIZE; i++) {
        #pragma HLS UNROLL
        a.mean[i] = data.mean[i];
        a.m2[i] = data.m2[i];
        a.nf[i] = data.nf[i];
        a.n[i] = data.n[i];
      }
    }
    static void project(void (*func)(acc_t &),
                        acc_t &a) {
        (*func)(a);
    }
};

template <typename config_t, typename acc_t, typename in_t, typename init_t, typename ops_t,
          typename T, typename ACC_T>
void welford_reduce_dataflow(hls::stream<acc_t> &acc_stream,
                             hls::stream<in_t> &a_stream,
                             size_t stride,
                             size_t num_iters = 1) {
    using r_in_t = WelfordReduceData<T, config_t::par_log_size(), config_t::rn_log_size()>;
    const size_t par_stride = (stride << config_t::rn_log_size()) >> config_t::par_log_size();
    reduce_iter: for (size_t r = 0; r < num_iters; r++)
        reduce: for (size_t i = 0; i < par_stride; i++) {
            #pragma HLS PIPELINE
            in_t a_vec;
            acc_t acc_vec;
            a_stream >> a_vec;
            ops_t::reduce(acc_vec, r_in_t{a_vec, init_t::m2(), init_t::nf(), init_t::n()});
            acc_stream << acc_vec;
        }
}

template <typename config_t, typename buf_t, typename acc_t, typename init_t,
          typename T, typename ACC_T>
void welford_buffer_dataflow(hls::stream<buf_t>& acc_stream,
                             hls::stream<acc_t>& a_stream,
                             size_t stride,
                             size_t num_iters = 1) {
    const size_t par_stride = (stride << config_t::rn_log_size()) >> config_t::par_log_size();
    const size_t buf_log_size = config_t::buf_log_size() - config_t::rn_log_size();
    const size_t buf_size = config_t::buf_size() >> config_t::rn_log_size();
    const size_t padded_par_stride = (par_stride + buf_size - 1) >> buf_log_size;
    buffer_iter: for (size_t r = 0; r < num_iters; r++) {
        buffer: for (size_t i = 0; i < padded_par_stride; i++) {
            #pragma HLS PIPELINE II=buf_size
            acc_t a_vec;
            buf_t acc_vec;
            for (size_t j = 0; j < buf_size; j++) {
                #pragma HLS UNROLL
                if ((i << buf_log_size) < par_stride) {
                  a_stream >> a_vec;
                  acc_vec.mean.shift(a_vec.mean.ptr());
                  acc_vec.m2.shift(a_vec.m2.ptr());
                  acc_vec.nf.shift(a_vec.nf.ptr());
                  acc_vec.n.shift(a_vec.n.ptr());
                } else {
                  acc_vec.mean.shift(init_t::mean().ptr());
                  acc_vec.m2.shift(init_t::m2().ptr());
                  acc_vec.nf.shift(init_t::nf().ptr());
                  acc_vec.n.shift(init_t::n().ptr());
                }
            }
            acc_stream << acc_vec;
        }
    }
}

template <typename config_t, typename acc_t, typename buf_t, typename init_t, typename ops_t,
          typename T, typename ACC_T>
void welford_combine_dataflow(hls::stream<acc_t>& acc_stream,
                              hls::stream<buf_t>& a_stream,
                              size_t stride,
                              size_t num_iters = 1 ) {
    const size_t par_stride = (stride << config_t::rn_log_size()) >> config_t::par_log_size();
    const size_t buf_log_size = config_t::buf_log_size() - config_t::rn_log_size();
    const size_t buf_size = config_t::buf_size() >> config_t::rn_log_size();
    const size_t padded_par_stride = (par_stride + buf_size - 1) >> buf_log_size;
    combine_iter: for (size_t r = 0; r < num_iters; r++) {
        acc_t acc_final_vec = {init_t::mean(), init_t::m2(), init_t::nf(), init_t::n()};
        combine: for (size_t i = 0; i < padded_par_stride; i++) {
            #pragma HLS PIPELINE II=buf_size
            buf_t a_vec;
            acc_t acc_vec;
            a_stream >> a_vec;
            ops_t::combine(acc_final_vec, acc_vec, a_vec);
            if (i == padded_par_stride - 1) {
                acc_stream << acc_final_vec;
            }
        }
    }
}

template <typename config_t, typename out_t, typename acc_t, typename ops_t,
          typename T, typename ACC_T>
void welford_project_dataflow(hls::stream<out_t>& a_out_stream,
                              hls::stream<acc_t>& a_stream,
                              size_t stride) {
    acc_t a;
    const size_t par_stride = (stride << out_t::rn_log_size()) >> out_t::par_log_size();
    project: for (size_t j = 0; j < par_stride; j++) {
      #pragma HLS PIPELINE II=1
        a_stream >> a;
        ops_t::project(a);
        a_out_stream << a.m2;
    }
}

template <typename config_t, typename out_t, typename acc_t, typename ops_t,
          typename T, typename ACC_T>
void mean_project_dataflow(hls::stream<out_t>& a_out_stream,
                              hls::stream<acc_t>& a_stream,
                              size_t stride) {
    acc_t a;
    const size_t par_stride = (stride << out_t::rn_log_size()) >> out_t::par_log_size();
    project: for (size_t j = 0; j < par_stride; j++) {
      #pragma HLS PIPELINE II=1
        a_stream >> a;
        ops_t::project(a);
        a_out_stream << a.mean;
    }
}

} } // namespace at::vec
