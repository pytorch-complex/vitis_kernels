#pragma once

#include <ATen/fpga/kernel/Dataflow.h>

namespace at { namespace vec {

template <typename ACC_T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
struct ArgReduceData {
    Vec<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> value;
    Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE> index;
};

template <typename acc_t,
          typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class ArgReduceOps {
   public:
    using h_op_t = ArgReduceOps<acc_t, T, PAR_LOG_SIZE - 1, ACC_T, RN_LOG_SIZE>;
    static void reduce(void (*func)(acc_t &, const acc_t &),
                       acc_t &a, const ArgReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        using h_data_t = ArgReduceData<T, PAR_LOG_SIZE - 1, RN_LOG_SIZE>;
        acc_t b;
        h_op_t::reduce(func, a, h_data_t{data.value.beg_half(), data.index.beg_half()});
        h_op_t::reduce(func, b, h_data_t{data.value.end_half(), data.index.end_half()});
        (*func)(a, b);
    }
    static void combine(void (*func)(acc_t &, const acc_t &),
                        acc_t &a, const ArgReduceData<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        using h_data_t = ArgReduceData<ACC_T, PAR_LOG_SIZE - 1, RN_LOG_SIZE>;
        acc_t b;
        h_op_t::combine(func, a, h_data_t{data.value.beg_half(), data.index.beg_half()});
        h_op_t::combine(func, b, h_data_t{data.value.end_half(), data.index.end_half()});
        (*func)(a, b);
    }
    static void project(void (*func)(acc_t &),
                        acc_t &a) {
        (*func)(a);
    }
};

template <typename acc_t,
          typename T, typename ACC_T, size_t RN_LOG_SIZE>
class ArgReduceOps<acc_t, T, RN_LOG_SIZE, ACC_T, RN_LOG_SIZE> {
   public:
    static void reduce(void (*func)(acc_t &, const acc_t &),
                       acc_t &a, const ArgReduceData<T, RN_LOG_SIZE, RN_LOG_SIZE> &data) {
      #pragma HLS INLINE
      for (size_t i = 0; i < RN_PAR_SIZE; i++) {
        #pragma HLS UNROLL
        a.value[i] = ACC_T(data.value[i]);
        a.index[i] = data.index[i];
      }
    }
    static void combine(void (*func)(acc_t &, const acc_t &),
                        acc_t &a, const ArgReduceData<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE> &data) {
      #pragma HLS INLINE
      for (size_t i = 0; i < RN_PAR_SIZE; i++) {
        #pragma HLS UNROLL
        a.value[i] = data.value[i];
        a.index[i] = data.index[i];
      }
    }
    static void project(void (*func)(acc_t &),
                        acc_t &a) {
        (*func)(a);
    }
};

template <typename config_t, typename acc_t, typename in_t, typename init_t, typename ops_t,
          typename T, typename ACC_T>
void arg_reduce_dataflow(hls::stream<acc_t> &acc_stream,
                         hls::stream<in_t> &a_stream,
                         size_t stride,
                         size_t num_iters = 1) {
    using i_vect = Vec<size_t, config_t::par_log_size(), config_t::rn_log_size()>;
    using r_in_t = ArgReduceData<T, config_t::par_log_size(), config_t::rn_log_size()>;
    const size_t par_stride = (stride << config_t::rn_log_size()) >> config_t::par_log_size();
    const i_vect i_vec = i_vect::arange(0, 1);
    reduce_iter: for (size_t r = 0; r < num_iters; r++)
        reduce: for (size_t i = 0; i < par_stride; i++) {
            #pragma HLS PIPELINE
            in_t a_vec;
            acc_t acc_vec;
            a_stream >> a_vec;
            ops_t::reduce(acc_vec, r_in_t{a_vec, i_vec});
            acc_vec.index = acc_vec.index + (i << config_t::par_log_size());
            acc_stream << acc_vec;
        }
}

template <typename config_t, typename buf_t, typename acc_t, typename init_t,
          typename T, typename ACC_T>
void arg_buffer_dataflow(hls::stream<buf_t>& acc_stream,
                         hls::stream<acc_t>& a_stream,
                         size_t stride,
                         size_t num_iters = 1) {
    const size_t par_stride = (stride << config_t::rn_log_size()) >> config_t::par_log_size();
    const size_t buf_log_size = config_t::buf_log_size() - config_t::rn_log_size();
    const size_t buf_size = config_t::buf_size() >> config_t::rn_log_size();
    const size_t padded_par_stride = (par_stride + buf_size - 1) >> buf_log_size;
    buffer_iter: for (size_t r = 0; r < num_iters; r++) {
        buffer: for (size_t i = 0; i < padded_par_stride; i++) {
            #pragma HLS PIPELINE II=buf_size
            acc_t a_vec;
            buf_t acc_vec;
            for (size_t j = 0; j < buf_size; j++) {
                #pragma HLS UNROLL
                if ((i << buf_log_size) < par_stride) {
                  a_stream >> a_vec;
                  acc_vec.value.shift(a_vec.value.ptr());
                  acc_vec.index.shift(a_vec.index.ptr());
                } else {
                  acc_vec.value.shift(init_t::value().ptr());
                  acc_vec.index.shift(init_t::index().ptr());
                }
            }
            acc_stream << acc_vec;
        }
    }
}

template <typename config_t, typename acc_t, typename buf_t, typename init_t, typename ops_t,
          typename T, typename ACC_T>
void arg_combine_dataflow(hls::stream<acc_t>& acc_stream,
                          hls::stream<buf_t>& a_stream,
                          size_t stride,
                          size_t num_iters = 1 ) {
    const size_t par_stride = (stride << config_t::rn_log_size()) >> config_t::par_log_size();
    const size_t buf_log_size = config_t::buf_log_size() - config_t::rn_log_size();
    const size_t buf_size = config_t::buf_size() >> config_t::rn_log_size();
    const size_t padded_par_stride = (par_stride + buf_size - 1) >> buf_log_size;
    combine_iter: for (size_t r = 0; r < num_iters; r++) {
        acc_t acc_final_vec = {init_t::value(), init_t::index()};
        combine: for (size_t i = 0; i < padded_par_stride; i++) {
            #pragma HLS PIPELINE II=buf_size
            buf_t a_vec;
            acc_t acc_vec;
            a_stream >> a_vec;
            ops_t::combine(acc_final_vec, acc_vec, a_vec);
            if (i == padded_par_stride - 1) {
                acc_stream << acc_final_vec;
            }
        }
    }
}

template <typename config_t, typename out_t, typename i_out_t, typename acc_t, typename ops_t,
          typename T, typename ACC_T>
void arg_project_dataflow(hls::stream<out_t>& a_out_stream,
                          hls::stream<i_out_t>& i_out_stream,
                          hls::stream<acc_t>& a_stream,
                          size_t stride) {
    acc_t a;
    size_t par_stride = (stride << out_t::rn_log_size()) >> out_t::par_log_size();
    project: for (size_t j = 0; j < par_stride; j++) {
      #pragma HLS PIPELINE II=1
        a_stream >> a;
        ops_t::project(a);
        a_out_stream << a.value;
        i_out_stream << a.index;
    }
}

} } // namespace at::vec
