#include <ATen/fpga/kernel/${CATEGORY_NAME}/${KERNEL_BASENAME}.hpp>

extern "C" {

using buffer_t = ap_uint<${${DTYPE_NAME}_PAR_SIZE_BITS}>;
void ${KERNEL_NAME}(buffer_t *result, const buffer_t *a,
           size_t stride, size_t offset) {
  #pragma HLS INTERFACE s_axilite port=result bundle=control
  #pragma HLS INTERFACE s_axilite port=a bundle=control
  #pragma HLS INTERFACE s_axilite port=stride bundle=control
  #pragma HLS INTERFACE s_axilite port=offset bundle=control
  #pragma HLS INTERFACE s_axilite port=return bundle=control
  #pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem1
  #pragma HLS INTERFACE m_axi port=a offset=slave bundle=gmem2
  at::vec::r${${DTYPE_NAME}_RN}::${KERNEL_BASENAME}<${${DTYPE_NAME}_T}, ${${DTYPE_NAME}_PAR_LOG_SIZE}, ${${DTYPE_NAME}_RN_LOG_SIZE}>(result, a, stride, offset);
}

}
