#include <ATen/fpga/kernel/Dataflow.h>
#include <ATen/fpga/kernel/UnaryLogKernel/vlog.hpp>

namespace at { namespace vec {

namespace r1 {

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vlog1p_dataflow(hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> >& result_stream,
               hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> >& a_stream,
               size_t stride) {

  Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> a;
  const size_t par_stride = (stride << Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::rn_log_size()) >> Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_log_size();
  vlog1p: for (size_t j = 0; j < par_stride; j++){
    #pragma HLS PIPELINE II=1
    a_stream >> a;
    result_stream << a.log1p();
  }
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vlog1p(ap_uint<PAR_SIZE_BITS>* result,
          const ap_uint<PAR_SIZE_BITS>* a,
          size_t stride,
          size_t offset) {

#ifndef __SYNTHESIS__
  assert((stride % Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_size()) == 0);
#endif

  #pragma HLS DATAFLOW
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > result_stream("result_write");
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > a_stream("a_read");
  #pragma HLS data_pack variable = result_stream
  #pragma HLS data_pack variable = a_stream

  read_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(a_stream, a, stride, offset);
  vlog1p_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result_stream, a_stream, stride);
  write_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result, result_stream, stride, offset);
}

} // namespace r1

namespace r2 {

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vlog1p_p1_dataflow(hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> >& result_stream,
                            hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> >& a_stream,
                            size_t stride) {
  Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> a;
  static const Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> p1 = Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(1.0);
  const size_t par_stride = (stride << Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::rn_log_size()) >> Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_log_size();
  vlog1p_divlog1p: for (size_t j = 0; j < par_stride; j++){
    #pragma HLS PIPELINE II=1
    a_stream >> a;
    result_stream << a + p1;
  }
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vlog1p(ap_uint<PAR_SIZE_BITS>* result,
          const ap_uint<PAR_SIZE_BITS>* a,
          size_t stride,
          size_t offset) {

#ifndef __SYNTHESIS__
  assert((stride % Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_size()) == 0);
#endif

  #pragma HLS DATAFLOW
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > result_stream("result_write");
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > a_stream("a_read");
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > p1_stream("p1_stream");
  #pragma HLS data_pack variable = result_stream
  #pragma HLS data_pack variable = a_stream
  #pragma HLS data_pack variable = p1_stream

  read_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(a_stream, a, stride, offset);
  vlog1p_p1_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(p1_stream, a_stream, stride);
  vlog_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result_stream, p1_stream, stride);
  write_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result, result_stream, stride, offset);
}

} // namespace r2

} } // namespace at::vec
