#include <ATen/fpga/kernel/SharedWelfordReduceOps.h>

namespace at { namespace vec {

namespace r1 {

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class WelfordConfig {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 0; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class WelfordConfig<double, PAR_LOG_SIZE, double, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 3; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class WelfordConfig<float, PAR_LOG_SIZE, float, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 2; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
struct WelfordInit {
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> mean() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> m2() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> nf() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE> n() {return Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE>(size_t(0));}
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
struct ReduceWelfordInit {
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> mean() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> m2() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> nf() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(1));}
  static constexpr Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE> n() {return Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE>(size_t(1));}
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void welford_combine(WelfordReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a, const WelfordReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &b) {
  Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> delta, new_n, nb_over_n;
  delta = b.mean - a.mean;
  new_n = a.nf + b.nf;
  nb_over_n = b.nf / new_n;
  a.mean = a.mean + delta * nb_over_n;
  a.m2 = a.m2 + b.m2 + delta * delta * a.nf * nb_over_n;
  a.nf = new_n;
  a.n = a.n + b.n;
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void welford_project(WelfordReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a) {
  Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> _1 = Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(1));
  a.m2 = a.m2/(a.nf - _1);
}

template <typename acc_t,
          typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class StdVarOps : WelfordReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>{
   public:
    using super_t = WelfordReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    static void reduce(acc_t &a,
                       const WelfordReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::reduce(&welford_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
    }
    static void combine(acc_t &r, acc_t &a,
                        const WelfordReduceData<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::combine(&welford_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
        welford_combine(r, a);
    }
    static void project(acc_t &a) {
        super_t::project(&welford_project<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a);
    }
};

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
void vstd_var(ap_uint<SIZE_BITS * RN_PAR_SIZE>* result,
              const ap_uint<PAR_SIZE_BITS>* a,
              size_t stride,
              size_t offset,
              size_t num_iters) {
#ifndef __SYNTHESIS__
    assert(stride % PAR_SIZE == 0);
#endif
    using config_t = WelfordConfig<T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    using r_init_t = ReduceWelfordInit<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using b_init_t = WelfordInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using c_init_t = WelfordInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using in_t = Vec<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using buf_t = WelfordReduceData<ACC_T, config_t::buf_log_size(), config_t::rn_log_size()>;
    using acc_t = WelfordReduceData<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using out_t = Vec<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using r_t = StdVarOps<acc_t, T, config_t::par_log_size(), ACC_T, config_t::rn_log_size()>;
    using c_t = StdVarOps<acc_t, ACC_T, config_t::buf_log_size(), ACC_T, config_t::rn_log_size()>;
    using p_t = StdVarOps<acc_t, ACC_T, config_t::rn_log_size(), ACC_T, config_t::rn_log_size()>;

    #pragma HLS DATAFLOW
    hls::stream<in_t> a_stream;
    hls::stream<acc_t> reduce_stream;
    hls::stream<buf_t> buffer_stream;
    hls::stream<acc_t> combine_stream;
    hls::stream<out_t> result_stream;
    #pragma HLS data_pack variable = a_stream
    #pragma HLS stream variable = reduce_stream depth = 2
    #pragma HLS data_pack variable = reduce_stream
    #pragma HLS stream variable = buffer_stream depth = 2
    #pragma HLS data_pack variable = buffer_stream
    #pragma HLS data_pack variable = combine_stream
    #pragma HLS data_pack variable = split_stream

    read_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(a_stream, a, stride, offset);
    welford_reduce_dataflow<config_t, acc_t, in_t, r_init_t, r_t, T, ACC_T>(reduce_stream, a_stream, stride, num_iters);
    welford_buffer_dataflow<config_t, buf_t, acc_t, b_init_t, ACC_T, ACC_T>(buffer_stream, reduce_stream, stride, num_iters);
    welford_combine_dataflow<config_t, acc_t, buf_t, c_init_t, c_t, ACC_T, ACC_T>(combine_stream, buffer_stream, stride, num_iters);
    welford_project_dataflow<config_t, out_t, acc_t, p_t, ACC_T, ACC_T>(result_stream, combine_stream, 1);
    write_dataflow<ACC_T, RN_LOG_SIZE>(result, result_stream, 1, offset);
}

} // namespace r1

namespace r2 {

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class WelfordConfig {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 0; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class WelfordConfig<double, PAR_LOG_SIZE, double, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 3; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
class WelfordConfig<float, PAR_LOG_SIZE, float, RN_LOG_SIZE> {
   public:
    static constexpr size_t par_log_size() { return PAR_LOG_SIZE; }
    static constexpr size_t par_size() { return 1 << par_log_size(); }
    static constexpr size_t buf_log_size() { return 2; }
    static constexpr size_t buf_size() { return 1 << buf_log_size(); }
    static constexpr size_t rn_log_size() { return RN_LOG_SIZE; }
    static constexpr size_t rn_size() { return 1 << rn_log_size(); }
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
struct WelfordInit {
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> mean() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> m2() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> nf() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE> n() {return Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE>(size_t(0));}
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
struct ReduceWelfordInit {
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> mean() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> m2() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(0));}
  static constexpr Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> nf() {return Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(1), T(0));}
  static constexpr Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE> n() {return Vec<size_t, PAR_LOG_SIZE, RN_LOG_SIZE>(size_t(1));}
};

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void welford_combine(WelfordReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a, const WelfordReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &b) {
  Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> delta, new_n, nb_over_n;
  delta = b.mean - a.mean;
  new_n = a.nf + b.nf;
  nb_over_n = b.nf / new_n;
  a.mean = a.mean + delta * nb_over_n;
  a.m2 = a.m2 + b.m2 + delta * delta * a.nf * nb_over_n;
  a.nf = new_n;
  a.n = a.n + b.n;
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void welford_project(WelfordReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &a) {
  Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> _1 = Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>(T(1));
  a.m2 = a.m2/(a.nf - _1);
}

template <typename acc_t,
          typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
class StdVarOps : WelfordReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>{
   public:
    using super_t = WelfordReduceOps<acc_t, T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    static void reduce(acc_t &a,
                       const WelfordReduceData<T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::reduce(&welford_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
    }
    static void combine(acc_t &r, acc_t &a,
                        const WelfordReduceData<ACC_T, PAR_LOG_SIZE, RN_LOG_SIZE> &data) {
        super_t::combine(&welford_combine<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a, data);
        welford_combine(r, a);
    }
    static void project(acc_t &a) {
        super_t::project(&welford_project<ACC_T, RN_LOG_SIZE, RN_LOG_SIZE>, a);
    }
};

template <typename T, size_t PAR_LOG_SIZE, typename ACC_T, size_t RN_LOG_SIZE>
void vstd_var(ap_uint<SIZE_BITS * RN_PAR_SIZE>* result,
              const ap_uint<PAR_SIZE_BITS>* a,
              size_t stride,
              size_t offset,
              size_t num_iters) {
#ifndef __SYNTHESIS__
    assert(stride % PAR_SIZE == 0);
#endif
    using config_t = WelfordConfig<T, PAR_LOG_SIZE, ACC_T, RN_LOG_SIZE>;
    using r_init_t = ReduceWelfordInit<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using b_init_t = WelfordInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using c_init_t = WelfordInit<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using in_t = Vec<T, config_t::par_log_size(), config_t::rn_log_size()>;
    using buf_t = WelfordReduceData<ACC_T, config_t::buf_log_size(), config_t::rn_log_size()>;
    using acc_t = WelfordReduceData<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using out_t = Vec<ACC_T, config_t::rn_log_size(), config_t::rn_log_size()>;
    using r_t = StdVarOps<acc_t, T, config_t::par_log_size(), ACC_T, config_t::rn_log_size()>;
    using c_t = StdVarOps<acc_t, ACC_T, config_t::buf_log_size(), ACC_T, config_t::rn_log_size()>;
    using p_t = StdVarOps<acc_t, ACC_T, config_t::rn_log_size(), ACC_T, config_t::rn_log_size()>;

    #pragma HLS DATAFLOW
    hls::stream<in_t> a_stream;
    hls::stream<acc_t> reduce_stream;
    hls::stream<buf_t> buffer_stream;
    hls::stream<acc_t> combine_stream;
    hls::stream<out_t> result_stream;
    #pragma HLS data_pack variable = a_stream
    #pragma HLS stream variable = reduce_stream depth = 2
    #pragma HLS data_pack variable = reduce_stream
    #pragma HLS stream variable = buffer_stream depth = 2
    #pragma HLS data_pack variable = buffer_stream
    #pragma HLS data_pack variable = combine_stream
    #pragma HLS data_pack variable = split_stream

    read_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(a_stream, a, stride, offset);
    welford_reduce_dataflow<config_t, acc_t, in_t, r_init_t, r_t, T, ACC_T>(reduce_stream, a_stream, stride, num_iters);
    welford_buffer_dataflow<config_t, buf_t, acc_t, b_init_t, ACC_T, ACC_T>(buffer_stream, reduce_stream, stride, num_iters);
    welford_combine_dataflow<config_t, acc_t, buf_t, c_init_t, c_t, ACC_T, ACC_T>(combine_stream, buffer_stream, stride, num_iters);
    welford_project_dataflow<config_t, out_t, acc_t, p_t, ACC_T, ACC_T>(result_stream, combine_stream, 1);
    write_dataflow<ACC_T, RN_LOG_SIZE>(result, result_stream, 1, offset);
}

} // namespace r2

} } // namespace at::vec
