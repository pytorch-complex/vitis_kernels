#include <ATen/fpga/kernel/${CATEGORY_NAME}/${KERNEL_BASENAME}.hpp>

extern "C" {

using buffer_t = ap_uint<512>;

void vi4fft(buffer_t result[FFT_LEN * N_FFT / SSR],
                    buffer_t a[FFT_LEN * N_FFT / SSR]) {
#pragma HLS INTERFACE s_axilite port=result bundle=control
#pragma HLS INTERFACE s_axilite port=a bundle=control
#pragma HLS INTERFACE s_axilite port=return bundle=control
#pragma HLS INTERFACE m_axi port=result offset=slave bundle=gmem1
#pragma HLS INTERFACE m_axi port=a offset=slave bundle=gmem2
    xf::dsp::fft::fftKernel<fftParams, IID, complex_wrapper<float> >(a, result, 1);
}

}
