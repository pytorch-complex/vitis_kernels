#include <ATen/fpga/kernel/Dataflow.h>
#include <ATen/fpga/kernel/UnaryPowKernel/vsqrt.hpp>
#include <ATen/fpga/kernel/UnaryPowKernel/vreciprocal.hpp>

namespace at { namespace vec {

namespace r1 {

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vrsqrt_dataflow(hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> >& result_stream,
               hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> >& a_stream,
               size_t stride) {

  Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> a;
  const size_t par_stride = (stride << Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::rn_log_size()) >> Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_log_size();
  vrsqrt: for (size_t j = 0; j < par_stride; j++){
    #pragma HLS PIPELINE II=1
    a_stream >> a;
    result_stream << a.rsqrt();
  }
}

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vrsqrt(ap_uint<PAR_SIZE_BITS>* result,
          const ap_uint<PAR_SIZE_BITS>* a,
          size_t stride,
          size_t offset) {

#ifndef __SYNTHESIS__
  assert((stride % Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_size()) == 0);
#endif

  #pragma HLS DATAFLOW
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > result_stream("result_write");
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > a_stream("a_read");
  #pragma HLS data_pack variable = result_stream
  #pragma HLS data_pack variable = a_stream

  read_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(a_stream, a, stride, offset);
  vrsqrt_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result_stream, a_stream, stride);
  write_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result, result_stream, stride, offset);
}

} // namespace r1

namespace r2 {

template <typename T, size_t PAR_LOG_SIZE, size_t RN_LOG_SIZE>
void vrsqrt(ap_uint<PAR_SIZE_BITS>* result,
          const ap_uint<PAR_SIZE_BITS>* a,
          size_t stride,
          size_t offset) {

#ifndef __SYNTHESIS__
  assert((stride % Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE>::par_size()) == 0);
#endif

  #pragma HLS DATAFLOW
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > result_stream("result_write");
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > a_stream("a_read");
  hls::stream<Vec<T, PAR_LOG_SIZE, RN_LOG_SIZE> > sqrt_stream("sqrt_stream");
  #pragma HLS data_pack variable = result_stream
  #pragma HLS data_pack variable = a_stream
  #pragma HLS data_pack variable = sqrt_stream

  read_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(a_stream, a, stride, offset);
  vsqrt_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(sqrt_stream, a_stream, stride);
  vreciprocal_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result_stream, sqrt_stream, stride);
  write_dataflow<T, PAR_LOG_SIZE, RN_LOG_SIZE>(result, result_stream, stride, offset);
}

} // namespace r2

} } // namespace at::vec
