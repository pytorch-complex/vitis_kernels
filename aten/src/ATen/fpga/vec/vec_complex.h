#pragma once

#include <complex>

#include <ATen/fpga/vec/vec_float.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wpragmas"
#pragma GCC diagnostic ignored "-Wunknown-pragmas"

namespace at { namespace vec { namespace {

// NOTE: If you specialize on a type, you must define all operations!
template <typename T, size_t PAR_LOG>
struct Vec<T, PAR_LOG, 1> {

private:
  T values[1 << PAR_LOG];
public:
  using value_type = T;
  static constexpr size_t par_log_size() {
    return PAR_LOG;
  }
  static constexpr size_t rn_log_size() {
    return 1;
  }
  static constexpr size_t size_bits() {
    return sizeof(T) * 8;
  }
  static constexpr size_t par_size() {
    return 1 << PAR_LOG;
  }
  static constexpr size_t par_size_bytes() {
    return sizeof(T) * par_size();
  }
  static constexpr size_t par_size_bits() {
    return sizeof(T) * par_size() * 8;
  }
  static constexpr size_t rn_size() {
    return 1 << rn_log_size();
  }
  Vec() { }
  Vec(T val) {
    #pragma HLS INLINE
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = val;
    }
  }
  Vec(T re, T im) {
    #pragma HLS INLINE
    for (size_t i = 0; i < par_size(); i +=rn_size()) {
      #pragma HLS UNROLL
      values[i + 0] = re;
      values[i + 1] = im;
    }
  }
  Vec(std::complex<T> val) {
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      values[i + 0] = val.real();
      values[i + 1] = val.imag();
    }
  }
  Vec(const Vec &val) {
    #pragma HLS INLINE
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = val.values[i];
    }
  }
  Vec(const T* val_ptr) {
    #pragma HLS INLINE
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = val_ptr[i];
    }
  }
  static Vec zero() {
    #pragma HLS INLINE
    Vec ret;
    for (int i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = static_cast<T>(0);
    }
    return ret;
  }
  template <size_t mask_>
  static Vec blend(const Vec& a, const Vec& b) {
    #pragma HLS INLINE
    size_t mask = mask_;
    Vec ret;
    for (size_t i = 0; i < par_size(); i+=rn_size()) {
      for (size_t j = 0; j < rn_size(); j++) {
        #pragma HLS UNROLL
        if (mask & 0x01) {
          ret[i + j] = b[i + j];
        } else {
          ret[i + j] = a[i + j];
        }
        mask = mask >> 1;
      }
    }
    return ret;
  }
  static Vec arange(T base = static_cast<T>(0), T step = static_cast<T>(1)) {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i+=rn_size()) {
      for (size_t j = 0; j < rn_size(); j++) {
        #pragma HLS UNROLL
        ret.values[i + j] = base + static_cast<T>(i)/rn_size() * step;
      }
    }
    return ret;
  }
  static Vec set(const Vec& a, const Vec& b, size_t count = par_size()) {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      for (size_t j = 0; j < rn_size(); j++) {
        #pragma HLS UNROLL
        if (i < count) {
          ret[i + j] = b[i + j];
        } else {
          ret[i + j] = a[i + j];
        }
      }
    }
    return ret;
  }
  Vec<T, PAR_LOG - 1, 1> beg_half() const{
    #pragma HLS INLINE
    return Vec<T, PAR_LOG - 1, 1>(ptr());
  }
  Vec<T, PAR_LOG - 1, 1> end_half() const{
    #pragma HLS INLINE
    return Vec<T, PAR_LOG - 1, 1>(ptr() + size_t(1 << size_t(par_log_size() - 1)));
  }
  const T& operator[](size_t idx) const {
    #pragma HLS INLINE
    return values[idx];
  }
  T& operator[](size_t idx) {
    #pragma HLS INLINE
    return values[idx];
  }
  const T* ptr() const {
    #pragma HLS INLINE
    return (&values[0]);
  }
  void* data_ptr() {
    #pragma HLS INLINE
    return reinterpret_cast<void*>(values);
  }
  T* shift(const T* val_in) {
    #pragma HLS INLINE
    static T values_out[rn_size()];
    for (int i = par_size() - 1; i > par_size() - 1 - rn_size(); i--) {
      #pragma HLS UNROLL
      values_out[i] = values[i];
    }
    for (int i = par_size() - 1; i > rn_size() - 1; i--) {
      #pragma HLS UNROLL
      T val = values[i - rn_size()];
      values[i] = val;
    }
    for (int i = rn_size() - 1; i >= 0; i--) {
      #pragma HLS UNROLL
      values[i] = val_in[i];
    }
    return &values_out[0];
  }
  T shift(T val_in) {
    #pragma HLS INLINE
    T values_out = values[par_size() - 1];
    for (int i = par_size() - 1; i > 0; i--) {
      #pragma HLS UNROLL
      T val = values[i - 1];
      values[i] = val;
    }
    values[0] = val_in;
    return values_out;
  }
  T shift() {
    #pragma HLS INLINE
    T values_out = values[par_size() - 1];
    for (int i = par_size() - 1; i > 0; i--) {
      #pragma HLS UNROLL
      T val = values[i - 1];
      values[i] = val;
    }
    return values_out;
  }
  T unshift() {
    #pragma HLS INLINE
    T values_out = values[0];
    for (int i = 0; i < par_size() - 1; i--) {
        #pragma HLS UNROLL
        T val = values[i + 1];
        values[i] = val;
    }
    return values_out;
  }
  void print(std::ostream& os) {
    for (size_t i = 0; i < par_size(); i++) {
        os << values[i] << " ";
    }
  }
  friend std::ostream& operator<<(std::ostream& os, Vec val) {
    val.print(os);
    return (os);
  }
#ifdef __HLS_VEC__
  Vec(const ap_uint<par_size_bits()> *buffer) {
    #pragma HLS INLINE
    BitConv<T> conv;
    const ap_uint<par_size_bits()> buf = buffer[0];
    size_t size_bits_ = size_bits();
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = conv.toType(buf(size_bits_*i + size_bits_ - 1, size_bits_*i));
    }
  }
  template<typename GEN_T>
  Vec(GEN_T generator[PAR_LOG]) {
    #pragma HLS INLINE
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = static_cast<T>(generator[i].next());
    }
  }
  Vec abs() const {
    #pragma HLS INLINE
    Vec ret = abs2().hadd().sqrtr().real();
    return ret;
  }
  Vec angle() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      ret[i + 0] = hls::atan2(values[i + 1], values[i + 0]);
      ret[i + 1] = 0.0;
    }
    return ret;
  }
  Vec real() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = (i % 2) ? 0.0 : values[i];
    }
    return ret;
  }
  Vec imag() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = (i % 2) ? values[i] : 0.0;
    }
    return ret;
  }
  Vec conj() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = (i % 2) ? -values[i] : values[i];
    }
    return ret;
  }
  Vec log() const {
    // ln(a + j*b)= ln(r) + j*(θ)
    #pragma HLS INLINE
    Vec ret_re = abs().logr();
    Vec ret_im = angle();
    Vec ret;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      ret[i + 0] = ret_re[i];
      ret[i + 1] = ret_im[i];
    }
    return ret;
  }
  Vec asin() const {
    // asin(x) = -i*ln(iz + sqrt(1 -z^2))
    // = -i*ln((ai - b) + sqrt(1 - (a + bi)*(a + bi)))
    // = -i*ln((-b + ai) + sqrt(1 - (a**2 - b**2) - 2*abi))
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::asin(values[i]);
    }
    return ret;
  }
  Vec acos() const {
    // acos(x) = pi/2 - asin(x)
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::acos(values[i]);
    }
    return ret;
  }
  Vec atan() const {
    // atan(x) = i/2 * ln((i + z)/(i - z))
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::atan(values[i]);
    }
    return ret;
  }
  Vec atan2(const Vec &b) const {
    //    AT_ERROR("not supported for complex numbers");
    #pragma HLS INLINE
    return Vec(0.0);
  }
  Vec erf() const {
    #pragma HLS INLINE
    return Vec(0.0);
  }
  Vec erfc() const {
    #pragma HLS INLINE
    return Vec(0.0);
  }
  Vec exp() const {
    //exp(a + bi)
    // = exp(a)*(cos(b) + sin(b)i)
    #pragma HLS INLINE
    Vec ret;
    T sin_b, cos_b;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      hls::sincos(values[i + 1], &sin_b, &cos_b);
      ret[i + 0] = hls::exp(values[i + 0]) * cos_b;
      ret[i + 1] = hls::exp(values[i + 0]) * sin_b;
    }
    return ret;
  }
  Vec expm1() const {
    #pragma HLS INLINE
    static const Vec ones = Vec(1);
    Vec ret = exp() - ones;
    return ret;
  }
  Vec sin() const {
    //sin𝑎cosh𝑏+𝑖cos𝑎sinh𝑏
    #pragma HLS INLINE
    Vec ret;
    T sin_a, cos_a;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      hls::sincos(values[i + 0], &sin_a, &cos_a);
      ret[i + 0] = sin_a * hls::cosh(values[i + 1]);
      ret[i + 1] = cos_a * hls::sinh(values[i + 1]);
    }
    return ret;
  }
  Vec sinh() const {
    //sinh(𝑎+𝑏𝑖)=-i*sin(ix) =
    //= -i*sin(-b + ai)
    //= -i*(sin(-b)cosh(a)+𝑖cos(-b)sinh(a))
    //= cos(-b)sinh(a) - isin(-b)cosh(a)
    //= cos(b)sinh(a) + isin(b)cosh(a)
    #pragma HLS INLINE
    Vec ret;
    T sin_b, cos_b;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      hls::sincos(values[i + 1], &sin_b, &cos_b);
      ret[i + 0] = cos_b * hls::sinh(values[i + 0]);
      ret[i + 1] = sin_b * hls::cosh(values[i + 0]);
    }
    return ret;
  }
  Vec cos() const {
    //cos(𝑎+𝑏𝑖)=cos𝑎cosh𝑏−𝑖sin𝑎sinh𝑏
    #pragma HLS INLINE
    Vec ret;
    T sin_a, cos_a;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      hls::sincos(values[i + 0], &sin_a, &cos_a);
      ret[i + 0] = cos_a * hls::cosh(values[i + 1]);
      ret[i + 1] = -sin_a * hls::sinh(values[i + 1]);
    }
    return ret;
  }
  Vec cosh() const {
    //cosh(𝑎+𝑏𝑖)=cos(ix) = cos(-b + ia)
    //= cos(-b)cosh(a) − 𝑖sin(-b)sinh(a)
    //= cos(b)cosh(a) + jsin(b)sinh(a)
    #pragma HLS INLINE
    Vec ret;
    T sin_b, cos_b;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      hls::sincos(values[i + 1], &sin_b, &cos_b);
      ret[i + 0] = cos_b * hls::cosh(values[i + 0]);
      ret[i + 1] = sin_b * hls::sinh(values[i + 0]);
    }
    return ret;
  }
  Vec ceil() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::ceil(values[i]);
    }
    return ret;
  }
  Vec floor() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::floor(values[i]);
    }
    return ret;
  }
  Vec neg() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = -values[i];
    }
    return ret;
  }
  Vec round() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::round(values[i]);
    }
    return ret;
  }
  Vec tan() const {
    #pragma HLS INLINE
    Vec ret;
    ret = sin()/cos();
    return ret;
  }
  Vec tanh() const {
    #pragma HLS INLINE
    Vec ret;
    ret = sinh()/cosh();
    return ret;
  }
  Vec trunc() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::trunc(values[i]);
    }
    return ret;
  }
  Vec sqrt() const {
    //   sqrt(a + bi)
    // = sqrt(2)/2 * [sqrt(sqrt(a**2 + b**2) + a) + sgn(b)*sqrt(sqrt(a**2 + b**2) - a)i]
    // = sqrt(2)/2 * [sqrt(abs() + a) + sgn(b)*sqrt(abs() - a)i]
    #pragma HLS INLINE
    Vec abs_ = abs2().hadd().sqrtr();
    Vec ret;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      ret[i + 0] = M_SQRT1_2*hls::sqrt(abs_[i + 0] + values[i + 0]);
      ret[i + 1] = hls::copysign(T(M_SQRT1_2*hls::sqrt(abs_[i + 0] - values[i + 0])), values[i + 1]);
    }
    return ret;
  }
  Vec reciprocal() const {
    //re + im*i = (a + bi)  / (c + di)
    //re = (ac + bd)/abs_2() = c/abs_2()
    //im = (bc - ad)/abs_2() = -d/abs_2()
    #pragma HLS INLINE
    Vec abs_2 = abs2().hadd();
    Vec ret;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      ret[i + 0] = values[i]/abs_2[i];
      ret[i + 1] = -values[i + 1]/abs_2[i + 1];
    }
    return ret;
  }
  Vec pow(const Vec &exp) const {
    //de Moivre’s formula: (cosθ+isinθ)^n =cosnθ+isinnθ
    #pragma HLS INLINE
    return Vec(0.0);
  }
  Vec<bool, PAR_LOG, 1> operator==(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, 1> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isequal(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, 1> operator!=(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, 1> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isnotequal(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, 1> operator<(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, 1> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isless(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, 1> operator<=(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, 1> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::islessequal(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, 1> operator>(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, 1> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isgreater(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, 1> operator>=(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, 1> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isgreaterequal(values[i], other[i]);
    }
    return ret;
  }
  Vec operator+(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] + other[i];
    }
    return ret;
  }
  Vec operator-(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] - other[i];
    }
    return ret;
  }
  Vec operator*(const Vec& other) const {
    //(a + bi)  * (c + di) = (ac - bd) + (ad + bc)i
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      ret[i + 0] = values[i + 0] * other[i + 0] - values[i + 1] * other[i + 1];
      ret[i + 1] = values[i + 0] * other[i + 1] + values[i + 1] * other[i + 0];
    }
    return ret;
  }
  Vec operator/(const Vec& other) const {
    //re + im*i = (a + bi)  / (c + di)
    //re = (ac + bd)/abs_2()
    //im = (bc - ad)/abs_2()
    #pragma HLS INLINE
    Vec ret;
    T abs_2;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
      abs_2 = other[i + 0]*other[i + 0] + other[i + 1]*other[i + 1];
      ret[i + 0] = (values[i + 0] * other[i + 0] + values[i + 1] * other[i + 1])/abs_2;
      ret[i + 1] = (values[i + 1] * other[i + 0] - values[i + 0] * other[i + 1])/abs_2;
    }
    return ret;
  }
  Vec maximum(const Vec &other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = (values[i] > other[i]) ? values[i] : other[i];
    }
    return ret;
  }
  Vec minimum(const Vec &other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = (values[i] < other[i]) ? values[i] : other[i];
    }
    return ret;
  }
  Vec clamp(const Vec& min, const Vec& max) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::min(max[i], hls::max(min[i], values[i]));
    }
    return ret;
  }
  Vec clamp_min(const Vec& min) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::max(min[i], values[i]);
    }
    return ret;
  }
  Vec clamp_max(const Vec& max) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::min(max[i], values[i]);
    }
    return ret;
  }
  Vec operator&(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] & other[i];
    }
    return ret;
  }
  Vec operator|(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] | other[i];
    }
    return ret;
  }
  Vec operator^(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] ^ other[i];
    }
    return ret;
  }
  Vec abs2() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i]*values[i];
    }
    return ret;
  }
  Vec hadd() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i += rn_size()) {
      #pragma HLS UNROLL
        ret[i + 0] = values[i + 0] + values[i + 1];
        ret[i + 1] = values[i + 0] + values[i + 1];
    }
    return ret;
  }
  Vec logr() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
        ret[i] = hls::log(values[i]);
    }
    return ret;
  }
  Vec sqrtr() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
        ret[i] = hls::sqrt(values[i]);
    }
    return ret;
  }
#endif

};

} } } //at::vec::anonymous

