#pragma once

#include <type_traits>
#include <cstring>
#include <cmath>
#include <stdint.h>
#include <iostream>

#include <c10/fpga/FPGAMacros.h>

#ifdef __HLS_VEC__
#include <hls_math.h>
#include <ATen/fpga/BitConverter.h>
#endif

namespace at { namespace vec { namespace {

// NOTE: If you specialize on a type, you must define all operations!
template <typename T, size_t PAR_LOG, size_t RN_LOG>
struct Vec {

public:
  T values[1 << PAR_LOG];
public:
  using value_type = T;
  static constexpr size_t par_log_size() {
    return PAR_LOG;
  }
  static constexpr size_t rn_log_size() {
    return RN_LOG;
  }
  static constexpr size_t size_bits() {
    return sizeof(T) * 8;
  }
  static constexpr size_t par_size() {
    return 1 << PAR_LOG;
  }
  static constexpr size_t par_size_bytes() {
    return sizeof(T) * par_size();
  }
  static constexpr size_t par_size_bits() {
    return sizeof(T) * par_size() * 8;
  }
  static constexpr size_t rn_size() {
    return 1 << rn_log_size();
  }
  Vec() { }
  Vec(T val) {
    #pragma HLS INLINE
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = val;
    }
  }
  Vec(const Vec &val) {
    #pragma HLS INLINE
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = val.values[i];
    }
  }
  Vec(const T* val_ptr) {
    #pragma HLS INLINE
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = val_ptr[i];
    }
  }
  static Vec zero() {
    #pragma HLS INLINE
    Vec ret;
    for (int i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = static_cast<T>(0);
    }
    return ret;
  }
  template <size_t mask_>
  static Vec blend(const Vec& a, const Vec& b) {
    #pragma HLS INLINE
    size_t mask = mask_;
    Vec ret;
    for (size_t i = 0; i < par_size(); i+=rn_size()) {
      for (size_t j = 0; j < rn_size(); j++) {
        #pragma HLS UNROLL
        if (mask & 0x01) {
          ret[i + j] = b[i + j];
        } else {
          ret[i + j] = a[i + j];
        }
        mask = mask >> 1;
      }
    }
    return ret;
  }
  static Vec arange(T base = static_cast<T>(0), T step = static_cast<T>(1)) {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i+=rn_size()) {
      for (size_t j = 0; j < rn_size(); j++) {
        #pragma HLS UNROLL
        ret.values[i + j] = base + static_cast<T>(i)/rn_size() * step;
      }
    }
    return ret;
  }
  static Vec set(const Vec& a, const Vec& b, size_t count = par_size()) {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      for (size_t j = 0; j < rn_size(); j++) {
        #pragma HLS UNROLL
        if (i < count) {
          ret[i + j] = b[i + j];
        } else {
          ret[i + j] = a[i + j];
        }
      }
    }
    return ret;
  }
  Vec<T, PAR_LOG - 1, RN_LOG> beg_half() const{
    #pragma HLS INLINE
    return Vec<T, PAR_LOG - 1, RN_LOG>(ptr());
  }
  Vec<T, PAR_LOG - 1, RN_LOG> end_half() const{
    #pragma HLS INLINE
    return Vec<T, PAR_LOG - 1, RN_LOG>(ptr() + size_t(1 << size_t(par_log_size() - 1)));
  }
  const T& operator[](size_t idx) const {
    #pragma HLS INLINE
    return values[idx];
  }
  T& operator[](size_t idx) {
    #pragma HLS INLINE
    return values[idx];
  }
  const T* ptr() const {
    #pragma HLS INLINE
    return (&values[0]);
  }
  void* data_ptr() {
    #pragma HLS INLINE
    return reinterpret_cast<void*>(values);
  }
  T* shift(const T* val_in) {
    #pragma HLS INLINE
    static T values_out[rn_size()];
    for (int i = par_size() - 1; i > par_size() - 1 - rn_size(); i--) {
      #pragma HLS UNROLL
      values_out[i] = values[i];
    }
    for (int i = par_size() - 1; i > rn_size() - 1; i--) {
      #pragma HLS UNROLL
      T val = values[i - rn_size()];
      values[i] = val;
    }
    for (int i = rn_size() - 1; i >= 0; i--) {
      #pragma HLS UNROLL
      values[i] = val_in[i];
    }
    return &values_out[0];
  }
  T shift(T val_in) {
    #pragma HLS INLINE
    T values_out = values[par_size() - 1];
    for (int i = par_size() - 1; i > 0; i--) {
      #pragma HLS UNROLL
      T val = values[i - 1];
      values[i] = val;
    }
    values[0] = val_in;
    return values_out;
  }
  T shift() {
    #pragma HLS INLINE
    T values_out = values[par_size() - 1];
    for (int i = par_size() - 1; i > 0; i--) {
      #pragma HLS UNROLL
      T val = values[i - 1];
      values[i] = val;
    }
    return values_out;
  }
  T unshift() {
    #pragma HLS INLINE
    T values_out = values[0];
    for (int i = 0; i < par_size() - 1; i--) {
        #pragma HLS UNROLL
        T val = values[i + 1];
        values[i] = val;
    }
    return values_out;
  }
  void print(std::ostream& os) {
    for (size_t i = 0; i < par_size(); i++) {
        os << values[i] << " ";
    }
  }
  friend std::ostream& operator<<(std::ostream& os, Vec val) {
    val.print(os);
    return (os);
  }
#ifdef __HLS_VEC__
  Vec(const ap_uint<par_size_bits()> *buffer) {
    #pragma HLS INLINE
    BitConv<T> conv;
    const ap_uint<par_size_bits()> buf = buffer[0];
    size_t size_bits_ = size_bits();
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = conv.toType(buf(size_bits_*i + size_bits_ - 1, size_bits_*i));
    }
  }
  template<typename GEN_T>
  Vec(GEN_T generator[PAR_LOG]) {
    #pragma HLS INLINE
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      values[i] = static_cast<T>(generator[i].next());
    }
  }
  Vec abs() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::abs(values[i]);
    }
    return ret;
  }
  Vec angle() const {
    #pragma HLS INLINE
    return Vec(0.0);
  }
  Vec real() const {
    #pragma HLS INLINE
    return *this;
  }
  Vec imag() const {
    #pragma HLS INLINE
    return Vec(0.0);
  }
  Vec conj() const {
    #pragma HLS INLINE
    return *this;
  }
  Vec log() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::log(values[i]);
    }
    return ret;
  }
  Vec log2() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::log2(values[i]);
    }
    return ret;
  }
  Vec log10() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::log10(values[i]);
    }
    return ret;
  }
  Vec log1p() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::log1p(values[i]);
    }
    return ret;
  }
  Vec asin() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::asin(values[i]);
    }
    return ret;
  }
  Vec acos() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::acos(values[i]);
    }
    return ret;
  }
  Vec atan() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::atan(values[i]);
    }
    return ret;
  }
  Vec atan2(const Vec &b) const {
    #pragma HLS INLINE
    return *this;
  }
  Vec erf() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::erf(values[i]);
    }
    return ret;
  }
  Vec erfc() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::erfc(values[i]);
    }
    return ret;
  }
  Vec exp() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::exp(values[i]);
    }
    return ret;
  }
  Vec expm1() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::expm1(values[i]);
    }
    return ret;
  }
  Vec sin() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::sin(values[i]);
    }
    return ret;
  }
  Vec sinh() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::sinh(values[i]);
    }
    return ret;
  }
  Vec cos() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::cos(values[i]);
    }
    return ret;
  }
  Vec cosh() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::cosh(values[i]);
    }
    return ret;
  }
  Vec ceil() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::ceil(values[i]);
    }
    return ret;
  }
  Vec floor() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::floor(values[i]);
    }
    return ret;
  }
  Vec neg() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = -values[i];
    }
    return ret;
  }
  Vec round() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::round(values[i]);
    }
    return ret;
  }
  Vec tan() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::tan(values[i]);
    }
    return ret;
  }
  Vec tanh() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::tanh(values[i]);
    }
    return ret;
  }
  Vec trunc() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::trunc(values[i]);
    }
    return ret;
  }
  Vec sqrt() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::sqrt(values[i]);
    }
    return ret;
  }
  Vec reciprocal() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::recip(values[i]);
    }
    return ret;
  }
  Vec rsqrt() const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::rsqrt(values[i]);
    }
    return ret;
  }
  Vec pow(const Vec &exp) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::pow(values[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, RN_LOG> operator==(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, RN_LOG> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isequal(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, RN_LOG> operator!=(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, RN_LOG> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isnotequal(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, RN_LOG> operator<(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, RN_LOG> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isless(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, RN_LOG> operator<=(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, RN_LOG> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::islessequal(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, RN_LOG> operator>(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, RN_LOG> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isgreater(values[i], other[i]);
    }
    return ret;
  }
  Vec<bool, PAR_LOG, RN_LOG> operator>=(const Vec& other) const {
    #pragma HLS INLINE
    Vec<bool, PAR_LOG, RN_LOG> ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::isgreaterequal(values[i], other[i]);
    }
    return ret;
  }
  Vec operator+(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] + other[i];
    }
    return ret;
  }
  Vec operator-(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] - other[i];
    }
    return ret;
  }
  Vec operator*(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] * other[i];
    }
    return ret;
  }
  Vec operator/(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] / other[i];
    }
    return ret;
  }
  Vec maximum(const Vec &other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = (values[i] > other[i]) ? values[i] : other[i];
    }
    return ret;
  }
  Vec minimum(const Vec &other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = (values[i] < other[i]) ? values[i] : other[i];
    }
    return ret;
  }
  Vec clamp(const Vec& min, const Vec& max) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::min(max[i], hls::max(min[i], values[i]));
    }
    return ret;
  }
  Vec clamp_min(const Vec& min) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::max(min[i], values[i]);
    }
    return ret;
  }
  Vec clamp_max(const Vec& max) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = hls::min(max[i], values[i]);
    }
    return ret;
  }
  Vec operator&(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] & other[i];
    }
    return ret;
  }
  Vec operator|(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] | other[i];
    }
    return ret;
  }
  Vec operator^(const Vec& other) const {
    #pragma HLS INLINE
    Vec ret;
    for (size_t i = 0; i < par_size(); i++) {
      #pragma HLS UNROLL
      ret[i] = values[i] ^ other[i];
    }
    return ret;
  }
#endif
};

} } } //at::vec::anonymous

