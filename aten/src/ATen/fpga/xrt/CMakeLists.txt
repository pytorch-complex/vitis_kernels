cmake_minimum_required(VERSION 3.10.0)
project(xrt NONE)

# Include
set(ROOT_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../../../..)
include(${ROOT_DIR}/root.cmake)

# Variables
set(XRT_SRCNAME ${CMAKE_CURRENT_SOURCE_DIR}/xrt.ini.in)
set(XRT_SRCNAME_OUT ${CMAKE_CURRENT_SOURCE_DIR}/xrt.ini)
set(XRT_DESTNAME ${CMAKE_CURRENT_BINARY_DIR}/xrt.ini)
configure_file(${XRT_SRCNAME} ${XRT_SRCNAME_OUT})

# Compile
add_custom_command(OUTPUT ${XRT_DESTNAME}
	COMMAND cp ${XRT_SRCNAME_OUT} ${XRT_DESTNAME}
	COMMENT "Configuring Runtime:"
)
add_custom_target(xrt ALL
	DEPENDS ${XRT_DESTNAME}
)
install(FILES ${XRT_DESTNAME} DESTINATION torch_fpga_strided_complex)
